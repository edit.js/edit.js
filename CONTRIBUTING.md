# Contributing to edit.js

We are happy to accept community contributions to help edit.js become more intuitive, 
efficient, and polyglot.

The objective of this document is to provide newcomers a meaningful way to get engaged and contribute.


## Table of Contents

* [Coding Style](#coding-style)
  * [File Structure](#file-structure)
  * [Code](#Code)
  * [Documentation](#documentation)
* [Testing](#testing)
  * [Running tests](#running-tests)
  * [Writing tests](#writing-tests)
* [Getting your merge request accepted](#getting-your-merge-request-accepted)


## Coding Style

This section specifies the conventions used in the source code.
If you see code which does not conform to them, feel free to fix it.

### File Structure

Each code file should contain, in this order:

- The Apache 2.0 copyright license header, followed by a blank line
- A JSDoc comment describing the purpose of the class
- The class definition

### Code

By default, all variables are declared as `const`.
If a variable needs to be reassigned, use `let` instead.

Private members of a class are declared using either `const` or `let`.
`this` is reserved for public members, such as methods and static constants.

Use identity operators `===` and `!==` except when type coercion is desirable.
A typical example would be checking for `null` and `undefined` using `==`.

When iterating over a matrix or a two-dimensional array, the counter variable 
denoting the row index is named `y` and the counter denoting the column index is named `x`.

Use as little inheritance as possible to keep the code modular.

#### Indentation

Code is indented using four spaces. Tab characters are not allowed for indenting code.

#### Naming conventions

Identifier names (variables and functions) use camelCase. 
Class names use UpperCamelCase. 
Static constants use UPPER_SNAKE_CASE.

#### Filename conventions

JavaScript filenames use UpperCamelCase. 
HTML, CSS, and assets filenames use lower-kebab-case.

### Documentation

Comments are written using the JSDoc convention.

Although "good code is self-documenting", it is incredibly easy to forget what your code 
(or someone else's) intended. 

Public methods should be commented when their purpose, potential side effects, or parameters
would benefit from being clarified.

Writing a few words can also go a long way when:
- a method from a supertype should not be overridden by a subtype (ES6 has no `final` methods)
- a method should not be invoked by classes that are not in the same package, or a subclass 
  (ES6 has no `protected` methods)
- (any other situation you deem relevant)

New classes should have one or two lines describing their purpose.


## Testing

Before merging a pull request, the unit tests should pass.

### Running tests

To run the unit tests, open the following file in your web-browser:

`edit.js\test\index.html`

![Editor](images/dashboard.png)

### Writing tests

Unit tests are written using a lightweight, custom framework inspired by the xUnit suite.
These tests can be executed and displayed using any modern browser.

Creating a set of unit tests is straightforward:

```
// Where 'ClassName' is the name of the class under test
Unit.test('packageName.ClassName')

    // Write unit tests in "run" method:
    .run({
    
        methodToTest_condition_expectation: (context) => {
          // Given
          // When
          // Then
        }
          
        // ...
   
    });
```

Optional `beforeAll` and `beforeEach` methods can also be defined to initialize the testing context preconditions.

The testing context is automatically passed to all fixture methods and unit tests.

```
Unit.test('packageName.ClassName')

    .beforeAll(context => {
        // The test runner will run this method once before any unit test in this set
        // is executed
        // Set up code...
    })

    .beforeEach(context => {
        // The test runner will run this method prior to each test
        // Fixture code...
    })

    .run({
        // Unit tests
    });
```

If clean-up steps are required at the end of a set of unit tests, a tear-down method can be defined using
`afterAll`.

Note that `afterAll` methods don't need to access the testing context, since a new context will automatically be provided
by the test runner to the next set of unit tests.

```
Unit.test('packageName.ClassName')

    .afterAll(() => {
        // The test runner will execute this method once every unit test in this set
        // has been executed
        // Clean up code...
    })

    .run({
        // Unit tests
    });
```

All tests created by `Unit.test` method will automatically be handled by the test runner.


## Getting your merge request accepted

edit.js uses [GitFlow](https://git-flow.readthedocs.io/en/latest/presentation.html) as a branching model.

As a result, `master` is the production branch. 
`develop` is the development / integration branch.

All forks / branches are created from `develop`. 

To contribute your code: 

1. Fork the project from the `develop` branch
2. Create a new branch named `feature/branch-name` for a new feature, or `bugfix/branch-name` for fixing a bug
3. Create one or more commits on your branch
4. Submit your merge request back into `develop`
