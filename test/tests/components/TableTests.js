/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('components.Table')

    .beforeEach((context) => {
        const alignments = [
            Alignment.LEFT, Alignment.CENTER, Alignment.RIGHT
        ];
        const data = [
            ['0','1','2'],
            ['3','4','5'],
            ['6','7','8']
        ];
        context.table = new EditorTable(3, 2);
        context.table.importModel(new TableModel(alignments, data));
    })

    .run({
        copyRow_shouldCopySourceRowToTargetRow: (ctx) => {
            // When
            const expected = [
                ['0','1','2'],
                ['3','4','5'],
                ['0','1','2']
            ];
            ctx.table.copyRow(0, 2);
            // Then
            assertArrayEquals(expected, ctx.table.getModel().getData());
        },

        copyColumn_shouldCopySourceColumnToTargetColumn: (ctx) => {
            // When
            const expected = [
                ['2','1','2'],
                ['5','4','5'],
                ['8','7','8']
            ];
            ctx.table.copyColumn(2, 0);
            // Then
            assertArrayEquals(expected, ctx.table.getModel().getData());
        },

        insertRow_shouldInsertEmptyRowAtSpecifiedIndex: (ctx) => {
            // When
            const expected = [
                ['0',  '1',  '2' ],
                [null, null, null],
                ['3',  '4',  '5' ],
                ['6',  '7',  '8' ]
            ];
            ctx.table.insertRow(1);
            // Then
            assertArrayEquals(expected, ctx.table.getModel().getData());
        },

        duplicateRow_shouldDuplicateTargetRow: (ctx) => {
            // When
            const expected = [
                ['0','1','2'],
                ['3','4','5'],
                ['3','4','5'],
                ['6','7','8']
            ];
            ctx.table.duplicateRow(1);
            // Then
            assertArrayEquals(expected, ctx.table.getModel().getData());
        },

        duplicateColumn_shouldDuplicateTargetColumn: (ctx) => {
            // When
            const expected = [
                ['0','1','2','2'],
                ['3','4','5','5'],
                ['6','7','8','8']
            ];
            ctx.table.duplicateColumn(2);
            // Then
            assertArrayEquals(expected, ctx.table.getModel().getData());
        },

        swap_shouldAlsoSwapAlignments: () => {
            // Given
            const table = new EditorTable(3, 2);
            table.alignColumn(0, Alignment.LEFT);
            table.alignColumn(1, Alignment.CENTER);
            table.alignColumn(2, Alignment.RIGHT);
            // When
            table.swapColumns(0, 2);
            // Then
            assertEquals(Alignment.RIGHT, table.getModel().getAlignment(0));
            assertEquals(Alignment.LEFT, table.getModel().getAlignment(2));
        },

        getWidth_shouldReturnTableWidth: () => {
            // Given
            const width = 3;
            const table = new EditorTable(width, 2);
            // Then
            assertEquals(width, table.getWidth());
        },

        getHeight_shouldReturnTableHeight: () => {
            // Given
            const height = 3;
            const table = new EditorTable(2, height);
            // Then
            assertEquals(height, table.getHeight());
        },

        forEachCell_whenFilledWithValue_shouldReturnValue: () => {
            // Given
            const expected = 1;
            const table = new EditorTable(2, 2);
            // When
            table.fill(expected);
            // Then
            table.forEachCell(cell => assertEquals(expected, cell));
        }
    });




