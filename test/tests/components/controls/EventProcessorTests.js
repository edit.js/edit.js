/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('controls.EventProcessor')

    .beforeAll((context) => {
        context.actions = [
            new Action('Action 1', Context.GLOBAL, new KeyStroke(Key.UP,   Modifier.CTRL), () => { } ),
            new Action('Action 2', Context.GLOBAL, new KeyStroke(Key.LEFT, Modifier.ALT), () => { } ),
            new Action('Action 3', Context.GLOBAL, new KeyStroke(Key.DOWN, Modifier.ALT, Modifier.SHIFT), () => { } ),
        ];
        context.eventProcessor = new EventProcessor();
        context.eventProcessor.register(...context.actions);
    })

    .run({
        getAction_whenMatchNotExists_shouldReturnNull: (ctx) => {
            // Given
            const evt = {key: Key.UP}
            // When
            const action = ctx.eventProcessor.getAction(evt);
            // Then
            assertNull(action);
        },

        getAction_whenMatchExistsWithOneModifier_shouldReturnMatchedAction: (ctx) => {
            // Given
            const evt = {key: Key.UP, ctrlKey: true}
            // When
            const action = ctx.eventProcessor.getAction(evt);
            // Then
            assertEquals(ctx.actions[0], action);
        },

        getAction_whenMatchExistsWithMultipleModifier_shouldReturnMatchedAction: (ctx) => {
            // Given
            const evt = {key: Key.DOWN, altKey: true, shiftKey: true}
            // When
            const action = ctx.eventProcessor.getAction(evt);
            // Then
            assertEquals(ctx.actions[2], action);
        }
    });
