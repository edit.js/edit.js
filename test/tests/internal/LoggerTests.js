/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('internal.Logger')

    .beforeAll(context => {
        Logger.setProduction(false);
        context.mockConsole = () => {
            const logs = {
                error: 0,
                info: 0,
                log: 0,
                trace: 0
            };
            return {
                error: (...data) => { logs.error++; },
                info: (...data) => { logs.info++; },
                log: (...data) => { logs.log++; },
                trace: (...data) => { logs.trace++; },
                getLogs: () => { return logs; }
            }
        }
    })

    .beforeEach(context => {
        context.consoleMock = context.mockConsole();
        Logger.setTarget(context.consoleMock);
    })

    .afterAll(() => {
        Logger.setLevel(TestsSettings.logLevel);
        Logger.setProduction(false);
        Logger.setTarget(console);
    })

    .run({
        error_whenLogLevelError_shouldLogMessage: (ctx) => {
            // Given
            Logger.setLevel(LogLevel.ERROR);
            // When
            Logger.log(LogLevel.ERROR, 'LoggerTest', 'log mock');
            // Then
            assertEquals(1, ctx.consoleMock.getLogs().error);
        },

        info_whenLogLevelError_shouldNotLogMessage: (ctx) => {
            // Given
            Logger.setLevel(LogLevel.ERROR);
            // When
            Logger.log(LogLevel.INFO, 'LoggerTest', 'log mock');
            // Then
            assertEquals(0, ctx.consoleMock.getLogs().info);
        },

        info_whenLogLevelDebug_shouldLogMessage: (ctx) => {
            // Given
            Logger.setLevel(LogLevel.DEBUG);
            // When
            Logger.log(LogLevel.INFO, 'LoggerTest', 'log mock');
            // Then
            assertEquals(1, ctx.consoleMock.getLogs().info);
        }
    });
