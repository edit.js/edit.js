/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('utils.TypeFunctions')

    .run({
        /* isArray */

        isArray_whenEmptyArray_shouldReturnTrue: () => {
            assertTrue(isArray([]));
        },

        isArray_whenArrayOfIntegers_shouldReturnTrue: () => {
            assertTrue(isArray([0, 1, 2]));
        },

        isArray_whenObject_shouldReturnFalse: () => {
            assertFalse(isArray({}));
        },

        isArray_whenString_shouldReturnFalse: () => {
            assertFalse(isArray("Although strings are iterable, they are not treated as arrays by this function"));
        },

        /* isObject */

        isObject_whenEmptyObject_shouldReturnTrue: () => {
            assertTrue(isObject({}));
        },

        isObject_whenNonEmptyObject_shouldReturnTrue: () => {
            assertTrue(isObject({"a": 1, "b": true, "c": /./g, "d": [], "e": {}, "f": () => {}, g: null}));
        },

        isObject_whenString_shouldReturnFalse: () => {
            assertFalse(isObject("Although everything in JS is technically an object, strings are not treated as objects by this function"));
        },

        isObject_whenArray_shouldReturnFalse: () => {
            assertFalse(isObject(["Although everything in JS is technically an object, arrays are not treated as objects by this function"]));
        },

        /* isPrimitive */

        isFunction_whenFunction_shouldReturnTrue: () => {
            // Given
            const f = function(a, b) { return a + b };
            const g = (a, b) => { return a + b };
            // When
            assertTrue(isFunction(f));
            assertTrue(isFunction(g));
        },

        isFunction_whenObject_shouldReturnFalse: () => {
            assertFalse(isFunction({}));
            assertFalse(isFunction(new Component()));
        },

        isFunction_whenNull_shouldReturnFalse: () => {
            assertFalse(isFunction(null));
        },

        /* isPrimitive */

        isPrimitive_whenNullOrUndefined_shouldReturnTrue: () => {
            assertTrue(isPrimitive(null));
            assertTrue(isPrimitive(undefined));
        },

        isPrimitive_whenNumber_shouldReturnTrue: () => {
            assertTrue(isPrimitive(12));
            assertTrue(isPrimitive(Number(12)));
        },

        isPrimitive_whenString_shouldReturnTrue: () => {
            assertTrue(isPrimitive("Hello world"));
        },

        isPrimitive_whenBoolean_shouldReturnTrue: () => {
            assertTrue(isPrimitive(true));
        },

        isPrimitive_whenRegex_shouldReturnFalse: () => {
            // Note: although it is possible to declare regex literals in JS, they are treated as objects at runtime.
            // CF the ECMAScript spec:
            // "A regular expression literal is an input element that is converted to a RegExp object when it is scanned"
            assertFalse(isPrimitive(/./g));
            assertFalse(isPrimitive(new RegExp(".", "g")));
        },

        isPrimitive_whenEmptyArray_shouldReturnFalse: () => {
            assertFalse(isPrimitive([]));
        },

        isPrimitive_whenObject_shouldReturnFalse: () => {
            assertFalse(isPrimitive({}));
        }
    });
