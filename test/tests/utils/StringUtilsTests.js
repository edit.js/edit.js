/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('utils.StringUtils')

    .run({
        isBlank_whenBlank_shouldReturnTrue: () => {
            // Given
            const expected = [true, true, true];
            const lines = ['', '    ', null];
            // When
            const actual = lines.map(line => StringUtils.isBlank(line));
            // Then
            assertArrayEquals(expected, actual);
        },

        countSpacesBefore_shouldReturnNumberOfSpacesBeforeFirstCharacter: () => {
            // Given
            const expected = [-1, 0, 1, 2];
            const lines = [' ', 'a', ' b', '  c'];
            // When
            const actual = lines.map(line => StringUtils.countSpacesBefore(line));
            // Then
            assertArrayEquals(expected, actual);
        },

        countSpacesAfter_shouldReturnNumberOfSpacesAfterLastCharacter: () => {
            // Given
            const expected = [0, 0, 1, 2];
            const lines = ['', 'a', 'b ', 'c  '];
            // When
            const actual = lines.map(line => StringUtils.countSpacesAfter(line));
            // Then
            assertArrayEquals(expected, actual);
        },

        countOccurrences_whenThree_shouldReturnThree: () => {
            // Given
            const expected = 3;
            const input = 'hello world!'
            const pattern = 'l';
            // Then
            assertEquals(expected, StringUtils.countOccurrences(input, pattern));
        },

        countOccurrences_whenZero_shouldReturnZero: () => {
            // Given
            const expected = 0;
            const input = 'hello world!'
            const pattern = 'z';
            // Then
            assertEquals(expected, StringUtils.countOccurrences(input, pattern));
        },

        generateSequence_shouldConcatenateNTimesSpecifiedSequence: () => {
            // Given
            const expected = '+|+|+|';
            const sequence = '+|';
            // When
            const actual = StringUtils.repeat(sequence, 3);
            // Then
            assertEquals(expected, actual);
        },

        removeFirstAndLastCharacter_whenNotEmpty_shouldReturnAllCharactersBetweenFirstAndLast: () => {
            // Given
            const expected = 'Hello world!';
            const input = '"' + expected + '"';
            // When
            const actual = StringUtils.removeFirstAndLastCharacter(input);
            // Then
            assertEquals(expected, actual);

        },

        removeFirstAndLastCharacter_whenEmpty_shouldReturnEmptyString: () => {
            // Given
            const expected = '';
            const input = '';
            // When
            const actual = StringUtils.removeFirstAndLastCharacter(input);
            // Then
            assertEquals(expected, actual);

        },

        escapeHTMLChars_whenHTMLCharacter_shouldReturnHTMLEntity: () => {
            // Given
            const entity = HtmlEntities.AMPERSAND;
            const input = entity.unescaped;
            const expected = entity.escaped;
            // When
            const actual = StringUtils.escapeHTMLChars(input);
            // Then
            assertEquals(expected, actual);
        },

        unescapeHTMLChars_whenHTMLEntity_shouldReturnHTMLCharacter: () => {
            // Given
            const entity = HtmlEntities.AMPERSAND;
            const input = entity.escaped;
            const expected = entity.unescaped;
            // When
            const actual = StringUtils.unescapeHTMLChars(input);
            // Then
            assertEquals(expected, actual);
        },

        escapeHTMLChars_whenArrayOfHTMLCharacters_shouldReturnArrayOfEscapedHTMLEntities: () => {
            // Given
            const input = ['<a>link</a>', '<link href="app.css">']
            const expected = ['&lt;a&gt;link&lt;/a&gt;', '&lt;link href=&quot;app.css&quot;&gt;'];
            // When
            const actual = StringUtils.escapeHTMLChars(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        capitalize_whenSingleWord_shouldReturnCapitalizedWord: () => {
            // Given
            const expected = 'Hello!';
            const input = 'helLO!';
            // When
            const actual = StringUtils.capitalize(input);
            // Then
            assertEquals(expected, actual);
        },

        formatCase_whenCapitalize_shouldReturnCapitalizedSentence: () => {
            // Given
            const expected = 'Hello World!';
            const input = 'helLO woRLd!';
            // When
            const actual = StringUtils.formatCase(input, TextCase.CAPITALIZE);
            // Then
            assertEquals(expected, actual);
        },

        formatCase_whenLower_shouldReturnCapitalizedSentence: () => {
            // Given
            const expected = 'hello world!';
            const input = 'helLO woRLd!';
            // When
            const actual = StringUtils.formatCase(input, TextCase.LOWER);
            // Then
            assertEquals(expected, actual);
        },

        formatCase_whenUpper_shouldReturnCapitalizedSentence: () => {
            // Given
            const expected = 'HELLO WORLD!';
            const input = 'helLO woRLd!';
            // When
            const actual = StringUtils.formatCase(input, TextCase.UPPER);
            // Then
            assertEquals(expected, actual);
        },

        formatCase_whenSentence_shouldReturnTrue: () => {
            // Given
            const expected = 'Hello world!';
            const input = 'helLO woRLd!';
            // When
            const actual = StringUtils.formatCase(input, TextCase.SENTENCE);
            // Then
            assertEquals(expected, actual);
        },

        isCase_whenLowerCase_shouldReturnTrue: () => {
            // Given
            const input = 'hello world!';
            // Then
            assertTrue(StringUtils.isCase(input, TextCase.LOWER));
        },

        isCase_whenUpperCase_shouldReturnTrue: () => {
            // Given
            const input = 'HELLO WORLD!';
            // Then
            assertTrue(StringUtils.isCase(input, TextCase.UPPER));
        },

        isCase_whenCapitalize_shouldReturnTrue: () => {
            // Given
            const input = 'Hello World!';
            // Then
            assertTrue(StringUtils.isCase(input, TextCase.CAPITALIZE));
        },

        isCase_whenSentence_shouldReturnTrue: () => {
            // Given
            const input = 'Hello world!';
            // Then
            assertTrue(StringUtils.isCase(input, TextCase.SENTENCE));
        },

        getCase_whenSentence_shouldReturnSentence: () => {
            // Given
            const input = 'Hello world!';
            // Then
            assertTrue(StringUtils.isCase(input, TextCase.SENTENCE));
        },

        getCase_whenCapitalize_shouldReturnCapitalize: () => {
            // Given
            const input = 'Hello World!';
            // Then
            assertTrue(StringUtils.isCase(input, TextCase.CAPITALIZE));
        },

        cut_whenZero_shouldReturnEmptyStringAndInput: () => {
            // Given
            const input = 'Hello World!';
            const expected = ['', input];
            // When
            const actual = StringUtils.cut(input, [0]);
            // Then
            assertArrayEquals(expected, actual);
        },

        cut_whenLastIndex_shouldReturnInputAndEmptyString: () => {
            // Given
            const input = 'Hello World!';
            const expected = [input, ''];
            // When
            const actual = StringUtils.cut(input, [input.length]);
            // Then
            assertArrayEquals(expected, actual);
        },

        cut_whenOutOfBounds_shouldThrowError: () => {
            assertThrows(() => StringUtils.cut('', [2]));
        },

        cut_whenEmptyString_shouldReturnTwoEmptyStrings: () => {
            // Given
            const input = '';
            const expected = ['', input];
            // When
            const actual = StringUtils.cut(input, [0]);
            // Then
            assertArrayEquals(expected, actual);
        },

        cut_whenMiddle_shouldCutStringInHalf: () => {
            // Given
            const input = 'Hello World!';
            const expected = ['Hello', ' World!'];
            // When
            const actual = StringUtils.cut(input, [5]);
            // Then
            assertArrayEquals(expected, actual);
        },

        cut_whenTwoIndices_shouldCutStringInThreeParts: () => {
            // Given
            const input = 'Hello World! Foo Bar Baz';
            const expected = ['Hello', 'World!', 'Foo', 'Bar', 'Baz'];
            // When
            const actual = StringUtils.cut(input, [5, 12, 16, 20], 1);
            // Then
            assertArrayEquals(expected, actual);
        },

        indicesOf_whenTwoMatches_shouldReturnIndexOfEveryMatch: () => {
            // Given
            const input = '<table><tr></tr><tr></tr></table>';
            const pattern = /><\//;
            const expected = [10, 19, 24];
            // When
            const actual = StringUtils.indicesOf(input, pattern);
            // Then
            assertArrayEquals(expected, actual);
        },

        indexOf_whenMatches_shouldReturnIndexOfFirstMatch: () => {
            // Given
            const input = 'Foo bar baz';
            const pattern = /bar/;
            const expected = 4;
            // When
            const actual = StringUtils.indexOf(input, pattern);
            // Then
            assertEquals(expected, actual);
        },

        indexOf_whenMatchesAndStartPos_shouldReturnIndexOfSecondMatch: () => {
            // Given
            const input = 'Foo bar baz';
            const pattern = /a/;
            const startPos = 8;
            const expected = 9;
            // When
            const actual = StringUtils.indexOf(input, pattern, startPos);
            // Then
            assertEquals(expected, actual);
        },

        indexOf_whenNoMatch_shouldReturnMinusOne: () => {
            // Given
            const input = 'Foo bar baz';
            const pattern = /world/;
            const expected = -1;
            // When
            const actual = StringUtils.indexOf(input, pattern);
            // Then
            assertEquals(expected, actual);
        },

        indexOf_whenNoMatchAndStartPos_shouldReturnMinusOne: () => {
            // Given
            const input = 'Foo bar baz';
            const pattern = /world/;
            const startPos = 6;
            const expected = -1;
            // When
            const actual = StringUtils.indexOf(input, pattern, startPos);
            // Then
            assertEquals(expected, actual);
        },

        match_whenOffsetZero_shouldReturnFirstOccurrence: () => {
            // Given
            const input = 'Hello world!';
            // When
            const actual = StringUtils.match(input, /o/);
            // Then
            assertEquals(4, actual.getOffset());
        },

        match_whenOffsetAfterFirstOccurrence_shouldReturnSecondOccurrence: () => {
            // Given
            const input = 'Hello world!';
            // When
            const actual = StringUtils.match(input, /o/, 5);
            // Then
            assertEquals(7, actual.getOffset());
        },

        match_whenOffsetAfterLastOccurrence_shouldReturnNull: () => {
            // Given
            const input = 'Hello world!';
            // When
            const actual = StringUtils.match(input, /o/, 50);
            // Then
            assertNull(actual);
        },

        align_whenLeft_shouldAlignTextLeft: () => {
            // Given
            const input = 'Foo';
            const n = 10;
            const expected = 'Foo          ';
            // When
            const actual = StringUtils.alignText(input, n, Alignment.LEFT);
            // Then
            assertEquals(expected, actual);
        },

        align_whenRight_shouldAlignTextRight: () => {
            // Given
            const input = 'Foo';
            const n = 10;
            const expected = '          Foo';
            // When
            const actual = StringUtils.alignText(input, n, Alignment.RIGHT);
            // Then
            assertEquals(expected, actual);
        },

        align_whenCenterAndEventAvailableSpace_shouldAlignTextCenter: () => {
            // Given
            const input = 'Foo';
            const n = 10;
            const expected = '     Foo     ';
            // When
            const actual = StringUtils.alignText(input, n, Alignment.CENTER);
            // Then
            assertEquals(expected, actual);
        },

        align_whenCenterAndOddAvailableSpace_shouldAlignTextCenter: () => {
            // Given
            const input = 'Foo';
            const n = 11;
            const expected = '      Foo     ';
            // When
            const actual = StringUtils.alignText(input, n, Alignment.CENTER);
            // Then
            assertEquals(expected, actual);
        },

        isLineFeed_whenLineFeed_shouldReturnTrue: () => {
            assertTrue(StringUtils.isLinefeed('\n'));
        },

        isLineFeed_whenCarriageReturn_shouldReturnTrue: () => {
            assertTrue(StringUtils.isLinefeed('\r'));
        },

        isLineFeed_whenLineFeedAndCarriageReturn_shouldReturnTrue: () => {
            assertTrue(StringUtils.isLinefeed('\r\n'));
        },

        isLineFeed_whenNotLineFeedAndNotCarriageReturn_shouldReturnFalse: () => {
            assertFalse(StringUtils.isLinefeed(''));
        },

        indexOfFirstNonWhitespaceCharacter_whenThree_shouldReturnThree: () => {
            assertEquals(3, StringUtils.indexOfFirstNonWhitespaceCharacter('   Foo'));
        },

        indexOfFirstNonWhitespaceCharacter_whenNone_shouldReturnMinusOne: () => {
            assertEquals(-1, StringUtils.indexOfFirstNonWhitespaceCharacter('     '));
        },

        indexOfFirstNonWhitespaceCharacter_whenEmptyString_shouldReturnMinusone: () => {
            assertEquals(-1, StringUtils.indexOfFirstNonWhitespaceCharacter(''));
        },

        isNumeric_whenBoolean_shouldReturnFalse: () => {
            assertFalse(StringUtils.isNumeric(true));
            assertFalse(StringUtils.isNumeric(false));
        },

        isNumeric_whenNull_shouldReturnFalse: () => {
            assertFalse(StringUtils.isNumeric(null));
            assertFalse(StringUtils.isNumeric(undefined));
        },

        isNumeric_whenNumber_shouldReturnFalse: () => {
            assertTrue(StringUtils.isNumeric(1));
        },

        isNumeric_whenNumeric_shouldReturnTrue: () => {
            assertTrue(StringUtils.isNumeric('-135.4'));
        },

        isNumeric_whenLetters_shouldReturnFalse: () => {
            assertFalse(StringUtils.isNumeric('abc'));
        },
    });
