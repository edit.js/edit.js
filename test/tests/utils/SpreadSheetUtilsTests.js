/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('utils.SpreadSheetUtils')

    .run({
        columnName_shouldReturnCorrectColumnName: () => {
            // Given
            const first = 1;
            const last = 26;
            // When
            const a = SpreadSheetUtils.columnName(first);
            const z = SpreadSheetUtils.columnName(last);
            const aa = SpreadSheetUtils.columnName(last + 1);
            // Then
            assertEquals('A', a);
            assertEquals('Z', z);
            assertEquals('AA', aa);
        },

        columnNames_when26_shouldReturnAtoZ: () => {
            // Given
            const expected = Settings.ALPHABET;
            // When
            const actual = SpreadSheetUtils.columnNames(26);
            // Then
            assertEquals(expected, actual.join(''));
        },

        columnNames_when52_shouldReturnAtoAZ: () => {
            // Given
            const expected = [
                "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
                "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ"
            ];
            // When
            const actual = SpreadSheetUtils.columnNames(52);
            // Then
            assertArrayEquals(expected, actual);
        },

        rowNames_when26_shouldReturn1o26: () => {
            // Given
            const expected = [
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26
            ];
            // When
            const actual = SpreadSheetUtils.rowNames(26);
            // Then
            assertArrayEquals(expected, actual);
        }
    });
