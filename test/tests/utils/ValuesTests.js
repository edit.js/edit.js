/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('utils.Values')

    .run({
        between_whenBetween_shouldReturnTrue: () => {
            assertTrue(Values.between(1, 0, 2));
        },

        between_whenGreaterThanMax_shouldReturnFalse: () => {
            assertFalse(Values.between(-1, 0, 2));
        },

        between_whenLessThanMin_shouldReturnFalse: () => {
            assertFalse(Values.between(3, 0, 2));
        },

        restrict_whenGreaterThanMax_shouldReturnMin: () => {
            assertEquals(1, Values.restrict(4, 1, 3))
        },

        restrict_whenLessThanMin_shouldReturnMax: () => {
            assertEquals(3, Values.restrict(0, 1, 3))
        },

        restrict_whenBetweenMinAndMax_shouldReturnValue: () => {
            assertEquals(2, Values.restrict(2, 1, 3))
        },

        either_whenValuePresent_shouldReturnTrue: () => {
            assertTrue(Values.either(1, 0, 1, 2));
        },

        either_whenValueNotPresent_shouldReturnFalse: () => {
            assertFalse(Values.either(1, 2, 3));
        },

        either_whenNoElementsSpecified_shouldReturnFalse: () => {
            assertFalse(Values.either(1));
        },

        randInt_whenPositive_shouldReturnRandomIntegerBetweenMinAndMax: () => {
            const r = Values.randInt(0, 1);
            assertTrue(Values.either(r, 0, 1));
        },

        randInt_whenNegativeAndPositive_shouldReturnRandomIntegerBetweenMinAndMax: () => {
            const r = Values.randInt(-1, 1);
            assertTrue(Values.either(r, -1, 0, 1));
        },

        randBool_whenNoBiasSpecified_shouldReturnTrueOrFalse: () => {
            const r = Values.randBool();
            assertTrue(Values.either(r, true, false));
        },

        randBool_whenBias100_shouldReturnTrue: () => {
            const n = 9999;
            const actual = ArrayUtils.range(0, n)
                .map(e => Values.randBool(100));
            assertTrue(actual.filter(isTrue => isTrue).length === n);
        },

        randBool_whenBias0_shouldReturnFalse: () => {
            const n = 9999;
            const actual = ArrayUtils.range(0, n)
                .map(e => Values.randBool(0));
            assertTrue(actual.filter(isTrue => !isTrue).length === n);
        }
    });


