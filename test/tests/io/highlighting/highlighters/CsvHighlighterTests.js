/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('highlighters.CsvHighlighter')

    .beforeAll(ctx => {
        ctx.mockEncoderOutput = (subject) => {
            return StringUtils.escapeHTMLChars(subject).split('\n');
        }
    })

    .run({
        highlight_whenSeparatorComma_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const s = ',';
            const input = ctx.mockEncoderOutput(`
                ColumnA${s}ColumnB${s}ColumnC
                A1${s}B1${s}C1${s}
                A2${s}B2${s}${s}
                A3${s}B3${s}${s}
            `);
            const expected = [
                "",
                "                <span class=\"token string\">ColumnA</span><span class=\"token delimiter\">,</span><span class=\"token string\">ColumnB</span><span class=\"token delimiter\">,</span><span class=\"token string\">ColumnC</span>",
                "                <span class=\"token string\">A1</span><span class=\"token delimiter\">,</span><span class=\"token string\">B1</span><span class=\"token delimiter\">,</span><span class=\"token string\">C1</span><span class=\"token delimiter\">,</span>",
                "                <span class=\"token string\">A2</span><span class=\"token delimiter\">,</span><span class=\"token string\">B2</span><span class=\"token delimiter\">,</span><span class=\"token delimiter\">,</span>",
                "                <span class=\"token string\">A3</span><span class=\"token delimiter\">,</span><span class=\"token string\">B3</span><span class=\"token delimiter\">,</span><span class=\"token delimiter\">,</span>",
                "            "
            ];
            const highlighter = new CsvHighlighter(s);
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        }
    })
