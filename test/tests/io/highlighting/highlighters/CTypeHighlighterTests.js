/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('highlighters.CTypeHighlighter')

    .beforeAll(ctx => {
        ctx.getQuote = (format) => {
            return Languages.getDescriptor(format).quotes.htmlEntity;
        }
        ctx.mockEncoderOutput = (subject) => {
            return StringUtils.escapeHTMLChars(subject).split('\n');
        }
    })

    .run({
        highlight_whenJsonArray_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                [
                    ["A", "", "C", "D"],
                    ["", "F", "G", ""],
                    ["H", "I", "J", ""]
                ]
            `);
            const expected = [
                "",
                "                <span class=\"token delimiter\">[</span>",
                "                    <span class=\"token delimiter\">[</span><span class=\"token string\">&quot;A&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;C&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;D&quot;</span><span class=\"token delimiter\">]</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token delimiter\">[</span><span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;F&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;G&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">]</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token delimiter\">[</span><span class=\"token string\">&quot;H&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;I&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;J&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">]</span>",
                "                <span class=\"token delimiter\">]</span>",
                "            "
            ];
            const highlighter = new CTypeHighlighter(Format.JSON);
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        highlight_whenJsonArrayOfObjects_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                [
                    {
                        "A": "",
                        "": "F",
                        "C": "G",
                        "D": ""
                    },
                    {
                        "A": "H",
                        "": "I",
                        "C": "J",
                        "D": ""
                    }
                ]
            `);
            const expected = [
                "",
                "                <span class=\"token delimiter\">[</span>",
                "                    <span class=\"token delimiter\">{</span>",
                "                        <span class=\"token string\">&quot;A&quot;</span><span class=\"token delimiter\">:</span> <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">,</span>",
                "                        <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">:</span> <span class=\"token string\">&quot;F&quot;</span><span class=\"token delimiter\">,</span>",
                "                        <span class=\"token string\">&quot;C&quot;</span><span class=\"token delimiter\">:</span> <span class=\"token string\">&quot;G&quot;</span><span class=\"token delimiter\">,</span>",
                "                        <span class=\"token string\">&quot;D&quot;</span><span class=\"token delimiter\">:</span> <span class=\"token string\">&quot;&quot;</span>",
                "                    <span class=\"token delimiter\">}</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token delimiter\">{</span>",
                "                        <span class=\"token string\">&quot;A&quot;</span><span class=\"token delimiter\">:</span> <span class=\"token string\">&quot;H&quot;</span><span class=\"token delimiter\">,</span>",
                "                        <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">:</span> <span class=\"token string\">&quot;I&quot;</span><span class=\"token delimiter\">,</span>",
                "                        <span class=\"token string\">&quot;C&quot;</span><span class=\"token delimiter\">:</span> <span class=\"token string\">&quot;J&quot;</span><span class=\"token delimiter\">,</span>",
                "                        <span class=\"token string\">&quot;D&quot;</span><span class=\"token delimiter\">:</span> <span class=\"token string\">&quot;&quot;</span>",
                "                    <span class=\"token delimiter\">}</span>",
                "                <span class=\"token delimiter\">]</span>",
                "            "
            ];
            const highlighter = new CTypeHighlighter(Format.JSON);
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        highlight_whenJavaScriptArray_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                const arr = [
                    ['A', '', 'C', 'D'],
                    ['', 'F', 'G', ''],
                    ['H', 'I', 'J', '']
                ];
            `);
            const expected = [
                "",
                "                <span class=\"token keyword\">const</span> arr <span class=\"token delimiter\">=</span> <span class=\"token delimiter\">[</span>",
                "                    <span class=\"token delimiter\">[</span><span class=\"token string\">&#039;A&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;C&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;D&#039;</span><span class=\"token delimiter\">]</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token delimiter\">[</span><span class=\"token string\">&#039;&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;F&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;G&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;&#039;</span><span class=\"token delimiter\">]</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token delimiter\">[</span><span class=\"token string\">&#039;H&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;I&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;J&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;&#039;</span><span class=\"token delimiter\">]</span>",
                "                <span class=\"token delimiter\">]</span><span class=\"token delimiter\">;</span>",
                "            "
            ];
            const highlighter = new CTypeHighlighter(Format.JAVASCRIPT);
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        highlight_whenJavaScriptArrayOfObjects_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                const arr = [
                    {
                        A: '',
                        : 'F',
                        C: 'G',
                        D: ''
                    },
                    {
                        A: 'H',
                        : 'I',
                        C: 'J',
                        D: ''
                    }
                ];
            `);
            const expected = [
                "",
                "                <span class=\"token keyword\">const</span> arr <span class=\"token delimiter\">=</span> <span class=\"token delimiter\">[</span>",
                "                    <span class=\"token delimiter\">{</span>",
                "                        A<span class=\"token delimiter\">:</span> <span class=\"token string\">&#039;&#039;</span><span class=\"token delimiter\">,</span>",
                "                        <span class=\"token delimiter\">:</span> <span class=\"token string\">&#039;F&#039;</span><span class=\"token delimiter\">,</span>",
                "                        C<span class=\"token delimiter\">:</span> <span class=\"token string\">&#039;G&#039;</span><span class=\"token delimiter\">,</span>",
                "                        D<span class=\"token delimiter\">:</span> <span class=\"token string\">&#039;&#039;</span>",
                "                    <span class=\"token delimiter\">}</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token delimiter\">{</span>",
                "                        A<span class=\"token delimiter\">:</span> <span class=\"token string\">&#039;H&#039;</span><span class=\"token delimiter\">,</span>",
                "                        <span class=\"token delimiter\">:</span> <span class=\"token string\">&#039;I&#039;</span><span class=\"token delimiter\">,</span>",
                "                        C<span class=\"token delimiter\">:</span> <span class=\"token string\">&#039;J&#039;</span><span class=\"token delimiter\">,</span>",
                "                        D<span class=\"token delimiter\">:</span> <span class=\"token string\">&#039;&#039;</span>",
                "                    <span class=\"token delimiter\">}</span>",
                "                <span class=\"token delimiter\">]</span><span class=\"token delimiter\">;</span>",
                "            "
            ];
            const highlighter = new CTypeHighlighter(Format.JAVASCRIPT);
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        highlight_whenPythonArray_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                arr = [
                    ['A', '', 'C', 'D'],
                    ['', 'F', 'G', ''],
                    ['H', 'I', 'J', '']
                ];
            `);
            const expected = [
                "",
                "                arr <span class=\"token delimiter\">=</span> <span class=\"token delimiter\">[</span>",
                "                    <span class=\"token delimiter\">[</span><span class=\"token string\">&#039;A&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;C&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;D&#039;</span><span class=\"token delimiter\">]</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token delimiter\">[</span><span class=\"token string\">&#039;&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;F&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;G&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;&#039;</span><span class=\"token delimiter\">]</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token delimiter\">[</span><span class=\"token string\">&#039;H&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;I&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;J&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;&#039;</span><span class=\"token delimiter\">]</span>",
                "                <span class=\"token delimiter\">]</span><span class=\"token delimiter\">;</span>",
                "            "
            ];
            const highlighter = new CTypeHighlighter(Format.PYTHON);
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        highlight_whenPythonArrayOfDictionaries_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                arr = [
                    {
                        'A': '',
                        '': 'F',
                        'C': 'G',
                        'D': ''
                    },
                    {
                        'A': 'H',
                        '': 'I',
                        'C': 'J',
                        'D': ''
                    }
                ]
            `);
            const expected = [
                "",
                "                arr <span class=\"token delimiter\">=</span> <span class=\"token delimiter\">[</span>",
                "                    <span class=\"token delimiter\">{</span>",
                "                        <span class=\"token string\">&#039;A&#039;</span><span class=\"token delimiter\">:</span> <span class=\"token string\">&#039;&#039;</span><span class=\"token delimiter\">,</span>",
                "                        <span class=\"token string\">&#039;&#039;</span><span class=\"token delimiter\">:</span> <span class=\"token string\">&#039;F&#039;</span><span class=\"token delimiter\">,</span>",
                "                        <span class=\"token string\">&#039;C&#039;</span><span class=\"token delimiter\">:</span> <span class=\"token string\">&#039;G&#039;</span><span class=\"token delimiter\">,</span>",
                "                        <span class=\"token string\">&#039;D&#039;</span><span class=\"token delimiter\">:</span> <span class=\"token string\">&#039;&#039;</span>",
                "                    <span class=\"token delimiter\">}</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token delimiter\">{</span>",
                "                        <span class=\"token string\">&#039;A&#039;</span><span class=\"token delimiter\">:</span> <span class=\"token string\">&#039;H&#039;</span><span class=\"token delimiter\">,</span>",
                "                        <span class=\"token string\">&#039;&#039;</span><span class=\"token delimiter\">:</span> <span class=\"token string\">&#039;I&#039;</span><span class=\"token delimiter\">,</span>",
                "                        <span class=\"token string\">&#039;C&#039;</span><span class=\"token delimiter\">:</span> <span class=\"token string\">&#039;J&#039;</span><span class=\"token delimiter\">,</span>",
                "                        <span class=\"token string\">&#039;D&#039;</span><span class=\"token delimiter\">:</span> <span class=\"token string\">&#039;&#039;</span>",
                "                    <span class=\"token delimiter\">}</span>",
                "                <span class=\"token delimiter\">]</span>",
                "            "
            ];
            const highlighter = new CTypeHighlighter(Format.PYTHON);
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        highlight_whenCSharpJaggedArray_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                string[][] arr = {
                    new string[] {"A", "", "C", "D"},
                    new string[] {"", "F", "G", ""},
                    new string[] {"H", "I", "J", ""}
                };
            `);
            const expected = [
                "",
                "                <span class=\"token keyword\">string</span><span class=\"token delimiter\">[</span><span class=\"token delimiter\">]</span><span class=\"token delimiter\">[</span><span class=\"token delimiter\">]</span> arr <span class=\"token delimiter\">=</span> <span class=\"token delimiter\">{</span>",
                "                    <span class=\"token keyword\">new</span> <span class=\"token keyword\">string</span><span class=\"token delimiter\">[</span><span class=\"token delimiter\">]</span> <span class=\"token delimiter\">{</span><span class=\"token string\">&quot;A&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;C&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;D&quot;</span><span class=\"token delimiter\">}</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token keyword\">new</span> <span class=\"token keyword\">string</span><span class=\"token delimiter\">[</span><span class=\"token delimiter\">]</span> <span class=\"token delimiter\">{</span><span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;F&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;G&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">}</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token keyword\">new</span> <span class=\"token keyword\">string</span><span class=\"token delimiter\">[</span><span class=\"token delimiter\">]</span> <span class=\"token delimiter\">{</span><span class=\"token string\">&quot;H&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;I&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;J&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">}</span>",
                "                <span class=\"token delimiter\">}</span><span class=\"token delimiter\">;</span>",
                "            "
            ];
            const highlighter = new CTypeHighlighter(Format.C_SHARP);
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        highlight_whenCSharpMultiDimensionalArray_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                string[,] arr = {
                    {"A", "", "C", "D"},
                    {"", "F", "G", ""},
                    {"H", "I", "J", ""}
                };
            `);
            const expected = [
                "",
                "                <span class=\"token keyword\">string</span><span class=\"token delimiter\">[</span><span class=\"token delimiter\">,</span><span class=\"token delimiter\">]</span> arr <span class=\"token delimiter\">=</span> <span class=\"token delimiter\">{</span>",
                "                    <span class=\"token delimiter\">{</span><span class=\"token string\">&quot;A&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;C&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;D&quot;</span><span class=\"token delimiter\">}</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token delimiter\">{</span><span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;F&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;G&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">}</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token delimiter\">{</span><span class=\"token string\">&quot;H&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;I&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;J&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">}</span>",
                "                <span class=\"token delimiter\">}</span><span class=\"token delimiter\">;</span>",
                "            "
            ];
            const highlighter = new CTypeHighlighter(Format.C_SHARP);
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        highlight_whenPhpArray_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                $arr = array(
                    array("A", "", "C", "D"),
                    array("", "F", "G", ""),
                    array("H", "I", "J", "")
                );
            `);
            const expected = [
                "",
                "                <span class=\"token keyword\">$arr</span> <span class=\"token delimiter\">=</span> <span class=\"token keyword\">array</span><span class=\"token delimiter\">(</span>",
                "                    <span class=\"token keyword\">array</span><span class=\"token delimiter\">(</span><span class=\"token string\">&quot;A&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;C&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;D&quot;</span><span class=\"token delimiter\">)</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token keyword\">array</span><span class=\"token delimiter\">(</span><span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;F&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;G&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">)</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token keyword\">array</span><span class=\"token delimiter\">(</span><span class=\"token string\">&quot;H&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;I&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;J&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">)</span>",
                "                <span class=\"token delimiter\">)</span><span class=\"token delimiter\">;</span>",
                "            "
            ];
            const highlighter = new CTypeHighlighter(Format.PHP);
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        highlight_whenPhpAndAssociativeArray_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                $arr = array(
                    array(
                        "A" => "",
                        "" => "F",
                        "C" => "G",
                        "D" => ""
                    ),
                    array(
                        "A" => "H",
                        "" => "I",
                        "C" => "J",
                        "D" => ""
                    )
                );
            `);
            const expected = [
                "",
                "                <span class=\"token keyword\">$arr</span> <span class=\"token delimiter\">=</span> <span class=\"token keyword\">array</span><span class=\"token delimiter\">(</span>",
                "                    <span class=\"token keyword\">array</span><span class=\"token delimiter\">(</span>",
                "                        <span class=\"token string\">&quot;A&quot;</span> <span class=\"token delimiter\">=</span><span class=\"token delimiter\">&gt;</span> <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">,</span>",
                "                        <span class=\"token string\">&quot;&quot;</span> <span class=\"token delimiter\">=</span><span class=\"token delimiter\">&gt;</span> <span class=\"token string\">&quot;F&quot;</span><span class=\"token delimiter\">,</span>",
                "                        <span class=\"token string\">&quot;C&quot;</span> <span class=\"token delimiter\">=</span><span class=\"token delimiter\">&gt;</span> <span class=\"token string\">&quot;G&quot;</span><span class=\"token delimiter\">,</span>",
                "                        <span class=\"token string\">&quot;D&quot;</span> <span class=\"token delimiter\">=</span><span class=\"token delimiter\">&gt;</span> <span class=\"token string\">&quot;&quot;</span>",
                "                    <span class=\"token delimiter\">)</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token keyword\">array</span><span class=\"token delimiter\">(</span>",
                "                        <span class=\"token string\">&quot;A&quot;</span> <span class=\"token delimiter\">=</span><span class=\"token delimiter\">&gt;</span> <span class=\"token string\">&quot;H&quot;</span><span class=\"token delimiter\">,</span>",
                "                        <span class=\"token string\">&quot;&quot;</span> <span class=\"token delimiter\">=</span><span class=\"token delimiter\">&gt;</span> <span class=\"token string\">&quot;I&quot;</span><span class=\"token delimiter\">,</span>",
                "                        <span class=\"token string\">&quot;C&quot;</span> <span class=\"token delimiter\">=</span><span class=\"token delimiter\">&gt;</span> <span class=\"token string\">&quot;J&quot;</span><span class=\"token delimiter\">,</span>",
                "                        <span class=\"token string\">&quot;D&quot;</span> <span class=\"token delimiter\">=</span><span class=\"token delimiter\">&gt;</span> <span class=\"token string\">&quot;&quot;</span>",
                "                    <span class=\"token delimiter\">)</span>",
                "                <span class=\"token delimiter\">)</span><span class=\"token delimiter\">;</span>",
                "            "
            ];
            const highlighter = new CTypeHighlighter(Format.PHP);
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        highlight_whenJavaArray_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
               String[][] arr = {
                   {"A", "", "C", "D"},
                   {"", "F", "G", ""},
                   {"H", "I", "J", ""}
               };
            `);
            const expected = [
                "",
                "               <span class=\"token keyword\">String</span><span class=\"token delimiter\">[</span><span class=\"token delimiter\">]</span><span class=\"token delimiter\">[</span><span class=\"token delimiter\">]</span> arr <span class=\"token delimiter\">=</span> <span class=\"token delimiter\">{</span>",
                "                   <span class=\"token delimiter\">{</span><span class=\"token string\">&quot;A&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;C&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;D&quot;</span><span class=\"token delimiter\">}</span><span class=\"token delimiter\">,</span>",
                "                   <span class=\"token delimiter\">{</span><span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;F&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;G&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">}</span><span class=\"token delimiter\">,</span>",
                "                   <span class=\"token delimiter\">{</span><span class=\"token string\">&quot;H&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;I&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;J&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">}</span>",
                "               <span class=\"token delimiter\">}</span><span class=\"token delimiter\">;</span>",
                "            "
            ];
            const highlighter = new CTypeHighlighter(Format.JAVA);
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        highlight_whenJavaList_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                List<List<String>> arr = List.of(
                    List.of("A", "", "C", "D"),
                    List.of("", "F", "G", ""),
                    List.of("H", "I", "J", "")
                );
            `);
            const expected = [
                "",
                "                <span class=\"token keyword\">List</span><span class=\"token delimiter\">&lt;</span><span class=\"token keyword\">List</span><span class=\"token delimiter\">&lt;</span><span class=\"token keyword\">String</span><span class=\"token delimiter\">&gt;</span><span class=\"token delimiter\">&gt;</span> arr <span class=\"token delimiter\">=</span> <span class=\"token keyword\">List</span>.of<span class=\"token delimiter\">(</span>",
                "                    <span class=\"token keyword\">List</span>.of<span class=\"token delimiter\">(</span><span class=\"token string\">&quot;A&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;C&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;D&quot;</span><span class=\"token delimiter\">)</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token keyword\">List</span>.of<span class=\"token delimiter\">(</span><span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;F&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;G&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">)</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token keyword\">List</span>.of<span class=\"token delimiter\">(</span><span class=\"token string\">&quot;H&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;I&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;J&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;&quot;</span><span class=\"token delimiter\">)</span>",
                "                <span class=\"token delimiter\">)</span><span class=\"token delimiter\">;</span>",
                "            "
            ];
            const highlighter = new CTypeHighlighter(Format.JAVA);
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        highlight_whenContainsEscapedQuotes_shouldIgnoreEscapedQuotes: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                String[][] arr = {
                    {"<a href=\"#\">A</a>", "B"},
                    {"C", "D"}
                };
            `);
            const expected = [
                "",
                "                <span class=\"token keyword\">String</span><span class=\"token delimiter\">[</span><span class=\"token delimiter\">]</span><span class=\"token delimiter\">[</span><span class=\"token delimiter\">]</span> arr <span class=\"token delimiter\">=</span> <span class=\"token delimiter\">{</span>",
                "                    <span class=\"token delimiter\">{</span><span class=\"token string\">&quot;&lt;a href=&quot;</span>#<span class=\"token string\">&quot;&gt;A&lt;/a&gt;&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;B&quot;</span><span class=\"token delimiter\">}</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token delimiter\">{</span><span class=\"token string\">&quot;C&quot;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&quot;D&quot;</span><span class=\"token delimiter\">}</span>",
                "                <span class=\"token delimiter\">}</span><span class=\"token delimiter\">;</span>",
                "            "
            ];
            const highlighter = new CTypeHighlighter(Format.JAVA);
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        }
    })
