/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('highlighters.MediaWikiHighlighter')

    .beforeAll(ctx => {
        ctx.mediaWikiHighlighter = new MediaWikiHighlighter(Format.MEDIAWIKI);
        ctx.asciiDocHighlighter = new MediaWikiHighlighter(Format.ASCII_DOC);
        ctx.mockEncoderOutput = (subject) => {
            return StringUtils.escapeHTMLChars(subject).split('\n');
        }
    })

    .run({
        highlight_whenMediaWikiAndPrettify_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                {| class="wikitable"
                |-
                ! A
                ! B
                ! 
                |-
                | AA
                | 
                | 
                |-
                | 
                | BBB
                | 
                |}
            `);
            const expected = [
                "",
                "                <span class=\"token tag\">{|</span> class=<span class=\"token attribute\">&quot;wikitable&quot;</span>",
                "                <span class=\"token tag\">|-</span>",
                "                <span class=\"token delimiter\">!</span> <span class=\"token value\">A</span>",
                "                <span class=\"token delimiter\">!</span> <span class=\"token value\">B</span>",
                "                <span class=\"token delimiter\">!</span> ",
                "                <span class=\"token tag\">|-</span>",
                "                <span class=\"token delimiter\">|</span> <span class=\"token value\">AA</span>",
                "                <span class=\"token delimiter\">|</span> ",
                "                <span class=\"token delimiter\">|</span> ",
                "                <span class=\"token tag\">|-</span>",
                "                <span class=\"token delimiter\">|</span> ",
                "                <span class=\"token delimiter\">|</span> <span class=\"token value\">BBB</span>",
                "                <span class=\"token delimiter\">|</span> ",
                "                <span class=\"token tag\">|}</span>",
                "            "
            ];
            // When
            const actual = ctx.mediaWikiHighlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        highlight_whenMediaWikiAndMinify_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                {| class="wikitable"
                |-
                ! A !! B !! 
                |-
                | AA ||  || 
                |-
                |  || BBB || 
                |}
            `);
            const expected = [
                "",
                "                <span class=\"token tag\">{|</span> class=<span class=\"token attribute\">&quot;wikitable&quot;</span>",
                "                <span class=\"token tag\">|-</span>",
                "                <span class=\"token delimiter\">!</span> <span class=\"token value\">A</span> <span class=\"token delimiter\">!</span><span class=\"token delimiter\">!</span> <span class=\"token value\">B</span> <span class=\"token delimiter\">!</span><span class=\"token delimiter\">!</span> ",
                "                <span class=\"token tag\">|-</span>",
                "                <span class=\"token delimiter\">|</span> <span class=\"token value\">AA</span> <span class=\"token delimiter\">|</span><span class=\"token delimiter\">|</span>  <span class=\"token delimiter\">|</span><span class=\"token delimiter\">|</span> ",
                "                <span class=\"token tag\">|-</span>",
                "                <span class=\"token delimiter\">|</span>  <span class=\"token delimiter\">|</span><span class=\"token delimiter\">|</span> <span class=\"token value\">BBB</span> <span class=\"token delimiter\">|</span><span class=\"token delimiter\">|</span> ",
                "                <span class=\"token tag\">|}</span>",
                "            "
            ];
            // When
            const actual = ctx.mediaWikiHighlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        highlight_whenAsciiDocAndPrettify_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                [cols="<1,<1,<1"]
                |===
                | a | b | 
                
                | AA
                | 
                | 
                | 
                | BBB
                | 
                |===
            `);
            const expected = [
                "",
                "                <span class=\"token delimiter\">[</span>cols=<span class=\"token attribute\">&quot;&lt;1,&lt;1,&lt;1&quot;</span><span class=\"token delimiter\">]</span>",
                "                <span class=\"token tag\">|===</span>",
                "                <span class=\"token delimiter\">|</span> <span class=\"token value\">a</span> <span class=\"token delimiter\">|</span> <span class=\"token value\">b</span> <span class=\"token delimiter\">|</span> ",
                "                ",
                "                <span class=\"token delimiter\">|</span> <span class=\"token value\">AA</span>",
                "                <span class=\"token delimiter\">|</span> ",
                "                <span class=\"token delimiter\">|</span> ",
                "                <span class=\"token delimiter\">|</span> ",
                "                <span class=\"token delimiter\">|</span> <span class=\"token value\">BBB</span>",
                "                <span class=\"token delimiter\">|</span> ",
                "                <span class=\"token tag\">|===</span>",
                "            "
            ];
            // When
            const actual = ctx.asciiDocHighlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        highlight_whenAsciiDocAndMinify_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                [cols="<1,<1,<1"]
                |============
                | a  | b   | 
                
                | AA |     | 
                |    | BBB | 
                |============
            `);
            const expected = [
                "",
                "                <span class=\"token delimiter\">[</span>cols=<span class=\"token attribute\">&quot;&lt;1,&lt;1,&lt;1&quot;</span><span class=\"token delimiter\">]</span>",
                "                <span class=\"token tag\">|============</span>",
                "                <span class=\"token delimiter\">|</span> <span class=\"token value\">a </span> <span class=\"token delimiter\">|</span> <span class=\"token value\">b  </span> <span class=\"token delimiter\">|</span> ",
                "                ",
                "                <span class=\"token delimiter\">|</span> <span class=\"token value\">AA</span> <span class=\"token delimiter\">|</span>     <span class=\"token delimiter\">|</span> ",
                "                <span class=\"token delimiter\">|</span>    <span class=\"token delimiter\">|</span> <span class=\"token value\">BBB</span> <span class=\"token delimiter\">|</span> ",
                "                <span class=\"token tag\">|============</span>",
                "            "
            ];
            // When
            const actual = ctx.asciiDocHighlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        }
    })
