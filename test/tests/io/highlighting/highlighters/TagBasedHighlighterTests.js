/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('highlighters.TagBasedHighlighter')

    .beforeAll(ctx => {
        ctx.mockEncoderOutput = (subject) => {
            return StringUtils.escapeHTMLChars(subject).split('\n');
        }
    })

    .run({
        highlight_whenBBCode_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                [table]
                    [tr]
                        [th]aaa[/th]
                        [th]b[/th]
                        [th][/th]
                    [/tr]
                    [tr]
                        [td]AA[/td]
                        [td]BB[/td]
                        [td][/td]
                    [/tr]
                    [tr]
                        [td]A[/td]
                        [td][/td]
                        [td][/td]
                    [/tr]
                [/table]
            `);
            const expected = [
                "",
                "                <span class=\"token delimiter\">[</span><span class=\"token tag\">table</span><span class=\"token delimiter\">]</span>",
                "                    <span class=\"token delimiter\">[</span><span class=\"token tag\">tr</span><span class=\"token delimiter\">]</span>",
                "                        <span class=\"token delimiter\">[</span><span class=\"token tag\">th</span><span class=\"token delimiter\">]</span><span class=\"token value\">aaa</span><span class=\"token delimiter\">[</span><span class=\"token delimiter\">/</span><span class=\"token tag\">th</span><span class=\"token delimiter\">]</span>",
                "                        <span class=\"token delimiter\">[</span><span class=\"token tag\">th</span><span class=\"token delimiter\">]</span><span class=\"token value\">b</span><span class=\"token delimiter\">[</span><span class=\"token delimiter\">/</span><span class=\"token tag\">th</span><span class=\"token delimiter\">]</span>",
                "                        <span class=\"token delimiter\">[</span><span class=\"token tag\">th</span><span class=\"token delimiter\">]</span><span class=\"token delimiter\">[</span><span class=\"token delimiter\">/</span><span class=\"token tag\">th</span><span class=\"token delimiter\">]</span>",
                "                    <span class=\"token delimiter\">[</span><span class=\"token delimiter\">/</span><span class=\"token tag\">tr</span><span class=\"token delimiter\">]</span>",
                "                    <span class=\"token delimiter\">[</span><span class=\"token tag\">tr</span><span class=\"token delimiter\">]</span>",
                "                        <span class=\"token delimiter\">[</span><span class=\"token tag\">td</span><span class=\"token delimiter\">]</span><span class=\"token value\">AA</span><span class=\"token delimiter\">[</span><span class=\"token delimiter\">/</span><span class=\"token tag\">td</span><span class=\"token delimiter\">]</span>",
                "                        <span class=\"token delimiter\">[</span><span class=\"token tag\">td</span><span class=\"token delimiter\">]</span><span class=\"token value\">BB</span><span class=\"token delimiter\">[</span><span class=\"token delimiter\">/</span><span class=\"token tag\">td</span><span class=\"token delimiter\">]</span>",
                "                        <span class=\"token delimiter\">[</span><span class=\"token tag\">td</span><span class=\"token delimiter\">]</span><span class=\"token delimiter\">[</span><span class=\"token delimiter\">/</span><span class=\"token tag\">td</span><span class=\"token delimiter\">]</span>",
                "                    <span class=\"token delimiter\">[</span><span class=\"token delimiter\">/</span><span class=\"token tag\">tr</span><span class=\"token delimiter\">]</span>",
                "                    <span class=\"token delimiter\">[</span><span class=\"token tag\">tr</span><span class=\"token delimiter\">]</span>",
                "                        <span class=\"token delimiter\">[</span><span class=\"token tag\">td</span><span class=\"token delimiter\">]</span><span class=\"token value\">A</span><span class=\"token delimiter\">[</span><span class=\"token delimiter\">/</span><span class=\"token tag\">td</span><span class=\"token delimiter\">]</span>",
                "                        <span class=\"token delimiter\">[</span><span class=\"token tag\">td</span><span class=\"token delimiter\">]</span><span class=\"token delimiter\">[</span><span class=\"token delimiter\">/</span><span class=\"token tag\">td</span><span class=\"token delimiter\">]</span>",
                "                        <span class=\"token delimiter\">[</span><span class=\"token tag\">td</span><span class=\"token delimiter\">]</span><span class=\"token delimiter\">[</span><span class=\"token delimiter\">/</span><span class=\"token tag\">td</span><span class=\"token delimiter\">]</span>",
                "                    <span class=\"token delimiter\">[</span><span class=\"token delimiter\">/</span><span class=\"token tag\">tr</span><span class=\"token delimiter\">]</span>",
                "                <span class=\"token delimiter\">[</span><span class=\"token delimiter\">/</span><span class=\"token tag\">table</span><span class=\"token delimiter\">]</span>",
                "            "
            ];
            const highlighter = new TagBasedHighlighter(Format.BBCODE);
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        highlight_whenHtml_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                <table>
                  <thead>
                    <tr>
                      <th style="text-align: Left;">aaa</th>
                      <th style="text-align: Left;">b</th>
                      <th style="text-align: Left;"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>AA</td>
                      <td>BB</td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>A</td>
                      <td></td>
                      <td></td>
                    </tr>
                  </tbody>
                </table>
            `);
            const expected = [
                "",
                "                <span class=\"token delimiter\">&lt;</span><span class=\"token tag\">table</span><span class=\"token delimiter\">&gt;</span>",
                "                  <span class=\"token delimiter\">&lt;</span><span class=\"token tag\">thead</span><span class=\"token delimiter\">&gt;</span>",
                "                    <span class=\"token delimiter\">&lt;</span><span class=\"token tag\">tr</span><span class=\"token delimiter\">&gt;</span>",
                "                      <span class=\"token delimiter\">&lt;</span><span class=\"token tag\">th</span> <span class=\"token key\">style</span><span class=\"token delimiter\">=</span><span class=\"token attribute\">&quot;text-align: Left;&quot;</span><span class=\"token delimiter\">&gt;</span><span class=\"token value\">aaa</span><span class=\"token delimiter\">&lt;</span><span class=\"token delimiter\">/</span><span class=\"token tag\">th</span><span class=\"token delimiter\">&gt;</span>",
                "                      <span class=\"token delimiter\">&lt;</span><span class=\"token tag\">th</span> <span class=\"token key\">style</span><span class=\"token delimiter\">=</span><span class=\"token attribute\">&quot;text-align: Left;&quot;</span><span class=\"token delimiter\">&gt;</span><span class=\"token value\">b</span><span class=\"token delimiter\">&lt;</span><span class=\"token delimiter\">/</span><span class=\"token tag\">th</span><span class=\"token delimiter\">&gt;</span>",
                "                      <span class=\"token delimiter\">&lt;</span><span class=\"token tag\">th</span> <span class=\"token key\">style</span><span class=\"token delimiter\">=</span><span class=\"token attribute\">&quot;text-align: Left;&quot;</span><span class=\"token delimiter\">&gt;</span><span class=\"token delimiter\">&lt;</span><span class=\"token delimiter\">/</span><span class=\"token tag\">th</span><span class=\"token delimiter\">&gt;</span>",
                "                    <span class=\"token delimiter\">&lt;</span><span class=\"token delimiter\">/</span><span class=\"token tag\">tr</span><span class=\"token delimiter\">&gt;</span>",
                "                  <span class=\"token delimiter\">&lt;</span><span class=\"token delimiter\">/</span><span class=\"token tag\">thead</span><span class=\"token delimiter\">&gt;</span>",
                "                  <span class=\"token delimiter\">&lt;</span><span class=\"token tag\">tbody</span><span class=\"token delimiter\">&gt;</span>",
                "                    <span class=\"token delimiter\">&lt;</span><span class=\"token tag\">tr</span><span class=\"token delimiter\">&gt;</span>",
                "                      <span class=\"token delimiter\">&lt;</span><span class=\"token tag\">td</span><span class=\"token delimiter\">&gt;</span><span class=\"token value\">AA</span><span class=\"token delimiter\">&lt;</span><span class=\"token delimiter\">/</span><span class=\"token tag\">td</span><span class=\"token delimiter\">&gt;</span>",
                "                      <span class=\"token delimiter\">&lt;</span><span class=\"token tag\">td</span><span class=\"token delimiter\">&gt;</span><span class=\"token value\">BB</span><span class=\"token delimiter\">&lt;</span><span class=\"token delimiter\">/</span><span class=\"token tag\">td</span><span class=\"token delimiter\">&gt;</span>",
                "                      <span class=\"token delimiter\">&lt;</span><span class=\"token tag\">td</span><span class=\"token delimiter\">&gt;</span><span class=\"token delimiter\">&lt;</span><span class=\"token delimiter\">/</span><span class=\"token tag\">td</span><span class=\"token delimiter\">&gt;</span>",
                "                    <span class=\"token delimiter\">&lt;</span><span class=\"token delimiter\">/</span><span class=\"token tag\">tr</span><span class=\"token delimiter\">&gt;</span>",
                "                    <span class=\"token delimiter\">&lt;</span><span class=\"token tag\">tr</span><span class=\"token delimiter\">&gt;</span>",
                "                      <span class=\"token delimiter\">&lt;</span><span class=\"token tag\">td</span><span class=\"token delimiter\">&gt;</span><span class=\"token value\">A</span><span class=\"token delimiter\">&lt;</span><span class=\"token delimiter\">/</span><span class=\"token tag\">td</span><span class=\"token delimiter\">&gt;</span>",
                "                      <span class=\"token delimiter\">&lt;</span><span class=\"token tag\">td</span><span class=\"token delimiter\">&gt;</span><span class=\"token delimiter\">&lt;</span><span class=\"token delimiter\">/</span><span class=\"token tag\">td</span><span class=\"token delimiter\">&gt;</span>",
                "                      <span class=\"token delimiter\">&lt;</span><span class=\"token tag\">td</span><span class=\"token delimiter\">&gt;</span><span class=\"token delimiter\">&lt;</span><span class=\"token delimiter\">/</span><span class=\"token tag\">td</span><span class=\"token delimiter\">&gt;</span>",
                "                    <span class=\"token delimiter\">&lt;</span><span class=\"token delimiter\">/</span><span class=\"token tag\">tr</span><span class=\"token delimiter\">&gt;</span>",
                "                  <span class=\"token delimiter\">&lt;</span><span class=\"token delimiter\">/</span><span class=\"token tag\">tbody</span><span class=\"token delimiter\">&gt;</span>",
                "                <span class=\"token delimiter\">&lt;</span><span class=\"token delimiter\">/</span><span class=\"token tag\">table</span><span class=\"token delimiter\">&gt;</span>",
                "            "
            ];
            const highlighter = new TagBasedHighlighter(Format.HTML);
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        }
    })
