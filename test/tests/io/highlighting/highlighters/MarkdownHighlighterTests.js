/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('highlighters.MarkdownHighlighter')

    .beforeAll(ctx => {
        ctx.mockEncoderOutput = (subject) => {
            return StringUtils.escapeHTMLChars(subject).split('\n');
        }
    })

    .run({
        highlight_whenCucumber_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                | A   |     | C  |  |
                | AA  | BB  | CC |  |
                | AAA | BBB |    |  |
            `);
            const expected = [
                "",
                "                <span class=\"token delimiter\">|</span><span class=\"token value\"> A   </span><span class=\"token delimiter\">|</span><span class=\"token value\">     </span><span class=\"token delimiter\">|</span><span class=\"token value\"> C  </span><span class=\"token delimiter\">|</span><span class=\"token value\">  </span><span class=\"token delimiter\">|</span>",
                "                <span class=\"token delimiter\">|</span><span class=\"token value\"> AA  </span><span class=\"token delimiter\">|</span><span class=\"token value\"> BB  </span><span class=\"token delimiter\">|</span><span class=\"token value\"> CC </span><span class=\"token delimiter\">|</span><span class=\"token value\">  </span><span class=\"token delimiter\">|</span>",
                "                <span class=\"token delimiter\">|</span><span class=\"token value\"> AAA </span><span class=\"token delimiter\">|</span><span class=\"token value\"> BBB </span><span class=\"token delimiter\">|</span><span class=\"token value\">    </span><span class=\"token delimiter\">|</span><span class=\"token value\">  </span><span class=\"token delimiter\">|</span>",
                "            "
            ];
            const highlighter = new MarkdownHighlighter(Format.CUCUMBER_MARKDOWN);
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        highlight_whenGitHubMarkdown_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(
                `| A   |     | C   |     |
                 | :-- | :-- | :-- | :-- |
                 | AA  | BB  | CC  |     |
                 | AAA | BBB |     |     |`
            );
            const expected = [
                "<span class=\"token delimiter\">|</span><span class=\"token value\"> A   </span><span class=\"token delimiter\">|</span><span class=\"token value\">     </span><span class=\"token delimiter\">|</span><span class=\"token value\"> C   </span><span class=\"token delimiter\">|</span><span class=\"token value\">     </span><span class=\"token delimiter\">|</span>",
                "                 <span class=\"token delimiter\">|</span><span class=\"token attribute\"> :-- </span><span class=\"token delimiter\">|</span><span class=\"token attribute\"> :-- </span><span class=\"token delimiter\">|</span><span class=\"token attribute\"> :-- </span><span class=\"token delimiter\">|</span><span class=\"token attribute\"> :-- </span><span class=\"token delimiter\">|</span>",
                "                 <span class=\"token delimiter\">|</span><span class=\"token value\"> AA  </span><span class=\"token delimiter\">|</span><span class=\"token value\"> BB  </span><span class=\"token delimiter\">|</span><span class=\"token value\"> CC  </span><span class=\"token delimiter\">|</span><span class=\"token value\">     </span><span class=\"token delimiter\">|</span>",
                "                 <span class=\"token delimiter\">|</span><span class=\"token value\"> AAA </span><span class=\"token delimiter\">|</span><span class=\"token value\"> BBB </span><span class=\"token delimiter\">|</span><span class=\"token value\">     </span><span class=\"token delimiter\">|</span><span class=\"token value\">     </span><span class=\"token delimiter\">|</span>"
            ];
            const highlighter = new MarkdownHighlighter(Format.GITHUB_MARKDOWN);
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        highlight_whenConfluenceMarkdown_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                || A   ||     || Eeeeeee ||  ||
                |  AA  |  BB  |  CC      |    |
                |  AAA |  BBB |          |    |
            `);
            const expected = [
                "",
                "                <span class=\"token delimiter\">|</span><span class=\"token value\"></span><span class=\"token delimiter\">|</span><span class=\"token value\"> A   </span><span class=\"token delimiter\">|</span><span class=\"token value\"></span><span class=\"token delimiter\">|</span><span class=\"token value\">     </span><span class=\"token delimiter\">|</span><span class=\"token value\"></span><span class=\"token delimiter\">|</span><span class=\"token value\"> Eeeeeee </span><span class=\"token delimiter\">|</span><span class=\"token value\"></span><span class=\"token delimiter\">|</span><span class=\"token value\">  </span><span class=\"token delimiter\">|</span><span class=\"token value\"></span><span class=\"token delimiter\">|</span>",
                "                <span class=\"token delimiter\">|</span><span class=\"token value\">  AA  </span><span class=\"token delimiter\">|</span><span class=\"token value\">  BB  </span><span class=\"token delimiter\">|</span><span class=\"token value\">  CC      </span><span class=\"token delimiter\">|</span><span class=\"token value\">    </span><span class=\"token delimiter\">|</span>",
                "                <span class=\"token delimiter\">|</span><span class=\"token value\">  AAA </span><span class=\"token delimiter\">|</span><span class=\"token value\">  BBB </span><span class=\"token delimiter\">|</span><span class=\"token value\">          </span><span class=\"token delimiter\">|</span><span class=\"token value\">    </span><span class=\"token delimiter\">|</span>",
                "            "
            ];
            const highlighter = new MarkdownHighlighter(Format.CONFLUENCE_MARKDOWN);
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        }
    })
