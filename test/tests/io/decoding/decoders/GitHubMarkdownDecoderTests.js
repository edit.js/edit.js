/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('decoders.GitHubMarkdownDecoder')

    .run({
        computeAlignment_whenNotSpecified_shouldReturnLeft: () => {
            // Given
            const alignment = '-----';
            const decoder = new GitHubMarkdownDecoder();
            // When
            const actual = decoder.computeAlignment(alignment);
            // Then
            assertEquals(Alignment.LEFT, actual);
        },

        computeAlignment_whenLeft_shouldReturnLeft: () => {
            // Given
            const alignment = ':----';
            const decoder = new GitHubMarkdownDecoder();
            // When
            const actual = decoder.computeAlignment(alignment);
            // Then
            assertEquals(Alignment.LEFT, actual);
        },

        computeAlignment_whenRight_shouldReturnRight: () => {
            // Given
            const alignment = '----:';
            const decoder = new GitHubMarkdownDecoder();
            // When
            const actual = decoder.computeAlignment(alignment);
            // Then
            assertEquals(Alignment.RIGHT, actual);
        },

        computeAlignment_whenCenter_shouldReturnCenter: () => {
            // Given
            const alignment = ':----:';
            const decoder = new GitHubMarkdownDecoder();
            // When
            const actual = decoder.computeAlignment(alignment);
            // Then
            assertEquals(Alignment.CENTER, actual);
        },

        decode_shouldReturnCorrectTableModel: () => {
            // Given
            const header1 = 'header1';
            const header2 = 'header2';
            const cell1 = 'cell1';
            const cell2 = 'cell2';
            const expectedAlignments = [Alignment.LEFT, Alignment.RIGHT];
            const expectedData = [
                [header1, header2],
                [cell1, cell2]
            ];
            const expected = new TableModel(expectedAlignments, expectedData);
            const input = '\n' +
                '\n' +
                ' | ' + header1 + ' | ' + header2 + ' |\n' +
                ' | :--------- | ---------: |\n' +
                ' | ' + cell1 + '   | ' + cell2 + '   |\n' +
                '\n';
            const decoder = new GitHubMarkdownDecoder();
            // When
            const actual = decoder.decode(input).getTableModel();
            // Then
            assertArrayEquals(expected.getData(), actual.getData());
            assertArrayEquals(expected.getAlignments(), actual.getAlignments());
        },

        decode_whenOutputPolicyMinify_shouldDetectOutputPolicy: () => {
            // Given
            const input = '\n' +
                ' |Hello|world|\n' +
                ' |:---|:----|\n' +
                ' |a1|a2|\n';
            // When
            const decoder = new GitHubMarkdownDecoder();
            const actual = decoder.decode(input);
            // Then
            assertEquals(OutputPolicy.MINIFY, actual.getMetadata().getOutputPolicy());
        },

        decode_whenOutputPolicyPrettify_shouldDetectOutputPolicy: () => {
            // Given
            const input = '\n' +
                ' | Hello | world |\n' +
                ' | :---- | :---- |\n' +
                ' | a1    | a2    |\n';
            // When
            const decoder = new GitHubMarkdownDecoder();
            const actual = decoder.decode(input);
            // Then
            assertEquals(OutputPolicy.PRETTIFY, actual.getMetadata().getOutputPolicy());
        }
    });



