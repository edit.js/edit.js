/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('decoders.BBCodeDecoder')

    .beforeAll(ctx => {
        ctx.minified = '[table][tr][td]^[/td][td]F[/td][td]T[/td][/tr][tr][td]F[/td][td]0[/td][td]0[/td][/tr][tr][td]T[/td][td]0[/td][td]1[/td][/tr][/table]';
        ctx.prettified = `
                [table]
                    [tr]
                        [th]^[/th]
                        [th]F[/th]
                        [th]T[/th]
                    [/tr]
                    [tr]
                        [td]F[/td]
                        [td]0[/td]
                        [td]0[/td]
                    [/tr]
                    [tr]
                        [td]T[/td]
                        [td]0[/td]
                        [td]1[/td]
                    [/tr]
                [/table]
            `;
        ctx.data = [
            ['^', 'F', 'T'],
            ['F', '0', '0'],
            ['T', '0', '1']
        ];
    })

    .run({
        supports_whenMinifiedBBCodeFormat_shouldReturnFormatBBCode: (ctx) => {
            // Given
            const decoder = new BBCodeDecoder();
            // When
            const actual = decoder.supports(ctx.minified);
            // Then
            assertEquals(Format.BBCODE, actual);
        },

        supports_whenPrettifiedBBCodeFormat_shouldReturnFormatBBCode: (ctx) => {
            // Given
            const decoder = new BBCodeDecoder();
            // When
            const actual = decoder.supports(ctx.prettified);
            // Then
            assertEquals(Format.BBCODE, actual);
        },

        supports_whenInvalidBBCodeFormat_shouldReturnNull: () => {
            // Given
            const input = `
            <table><tr><td>A</td><td>B</td></tr></table>`;
            const decoder = new BBCodeDecoder();
            // When
            const actual = decoder.supports(input);
            // Then
            assertNull(actual);
        },

        decode_whenMinifiedBBCodeFormat_shouldReturnCorrectTableModel: (ctx) => {
            // Given
            const decoder = new BBCodeDecoder();
            // When
            const actual = decoder.decode(ctx.minified);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertNull(metadata.getHeaderCase());
            assertEquals(HeaderOrientation.NO_HEADER, metadata.getHeaderOrientation());
            assertArrayEquals(ctx.data, model.getData());
        },

        decode_whenPrettifiedBBCodeFormat_shouldReturnCorrectTableModel: (ctx) => {
            // Given
            const decoder = new BBCodeDecoder();
            // When
            const actual = decoder.decode(ctx.prettified);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(TextCase.UPPER, metadata.getHeaderCase());
            assertEquals(HeaderOrientation.FIRST_ROW, metadata.getHeaderOrientation());
            assertArrayEquals(ctx.data, model.getData());
        },

        decode_whenMissingCells_shouldReturnCorrectTableModel: (ctx) => {
            // Given
            const input = `
                [table]
                    [tr]
                        [th]Header1[/th]
                        [th]Header2[/th]
                    [/tr]
                    [tr]
                        [td]Cell A[/td]
                        [td]Cell B[/td]
                    [/tr]
                    [tr]
                        [td][/td]
                        [td]Cell D[/td]
                    [/tr]
                [/table]
            `;
            const expectedData = [
                ['Header1', 'Header2'],
                ['Cell A',  'Cell B' ],
                ['',        'Cell D' ]
            ];
            const decoder = new BBCodeDecoder();
            // When
            const actual = decoder.decode(input);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(TextCase.CAPITALIZE, metadata.getHeaderCase());
            assertEquals(HeaderOrientation.FIRST_ROW, metadata.getHeaderOrientation());
            assertArrayEquals(expectedData, model.getData());
        }
    })
