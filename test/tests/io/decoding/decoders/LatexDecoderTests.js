/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('decoders.LatexDecoder')

    .beforeAll(ctx => {
        ctx.emptyCellPlaceholder = '#';
        ctx.decoder = new LatexDecoder(ctx.emptyCellPlaceholder);
        ctx.data = [
            ['a', 'b', 'c', 'd'],
            ['e', 'f', 'g', 'h'],
            ['i', 'j', 'k', 'l']
        ];
        ctx.input = `
                \\begin{table}[!ht]
                    \\centering
                    \\caption{Table Caption}
                    \\begin{tabular}{|l|lll|}
                    \\hline
                        a & b & c & d \\\\ \\hline
                        e & f & g & h \\\\
                        i & j & k & l \\\\ \\hline
                    \\end{tabular}
                    \\label{Table Label}
                \\end{table}
            `;
    })

    .run({
        supports_whenFormatSupported_shouldReturnTrue: (ctx) => {
            assertTrue(ctx.decoder.supports(ctx.input));
        },

        supports_whenInvalidLatexSyntax_shouldReturnFalse: (ctx) => {
            // Given
            const input = `
                \\begin{table}[!ht]
                    \\centering
                    \\caption{Table Caption}
                    \\begin{tabular}
                    \\label{Table Label}
                \\end{table}
            `;
            // Then
            assertFalse(ctx.decoder.supports(input));
        },

        decode_whenNoEmptyCell_shouldReturnCorrectTableModel: (ctx) => {
            // Given
            const expected = ctx.data;
            // When
            const actual = ctx.decoder.decode(ctx.input);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(BorderPolicy.FIRST, metadata.getRowBorderPolicy())
            assertEquals(BorderPolicy.FIRST, metadata.getColumnBorderPolicy())
            assertArrayEquals(expected, model.getData());
        },

        decode_whenEmptyCells_shouldReturnCorrectTableModel: (ctx) => {
            // Given
            const expected = [
                ['a', 'b',  'c',  null],
                ['e', null, 'g',  'h' ],
                ['i', 'j',  null, null]
            ];
            const input = `
                \\begin{table}[!ht]
                    \\centering
                    \\caption{Table Caption}
                    \\begin{tabular}{|l|lll|}
                    \\hline
                        a & b & c &  \\\\ \\hline
                        e &   & g & h \\\\
                        i & j &  &  \\\\ \\hline
                    \\end{tabular}
                    \\label{Table Label}
                \\end{table}
            `;
            // When
            const actual = ctx.decoder.decode(input);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(BorderPolicy.FIRST, metadata.getRowBorderPolicy())
            assertEquals(BorderPolicy.FIRST, metadata.getColumnBorderPolicy())
            assertArrayEquals(expected, model.getData());
        },

        decode_whenPlaceholderCells_shouldReturnCorrectTableModel: (ctx) => {
            // Given
            const _ = ctx.emptyCellPlaceholder;
            const expected = [
                ['a', 'b',  'c',  null],
                ['e', null, 'g',  'h' ],
                ['i', 'j',  null, null]
            ];
            const input = `
                \\begin{table}[!ht]
                    \\centering
                    \\caption{Table Caption}
                    \\begin{tabular}{|l|lll|}
                    \\hline
                        a & b & c & ${_} \\\\ \\hline
                        e & ${_} & g & h \\\\
                        i & j & ${_} & ${_} \\\\ \\hline
                    \\end{tabular}
                    \\label{Table Label}
                \\end{table}
            `;
            // When
            const actual = ctx.decoder.decode(input);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(BorderPolicy.FIRST, metadata.getRowBorderPolicy())
            assertEquals(BorderPolicy.FIRST, metadata.getColumnBorderPolicy())
            assertArrayEquals(expected, model.getData());
        }
    })
