/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('decoders.SqlDecoder')

    .run({
        decode_whenMinimalInput_shouldReturnCorrectTableModel: () => {
            // Given
            const expected = [
                ['COL_NAME', 'COLUMN_NAME_2', 'LONGER_COLUMN_NAME', 'SHORT'],
                ['Cell1',    'Cell2',         'Cell3',              'Cell4'],
                ['Cell5',    'Cell6',         'Cell7',              'Cell8'],
                ['Cell9',    'Cell10',        'Cell11',             'Cell12']
            ];
            const input = `
    
             CREATE TABLE table_name (
                 COL_NAME           VARCHAR(255),
                 COLUMN_NAME_2      VARCHAR(255),
                 LONGER_COLUMN_NAME VARCHAR(255),
                 SHORT              VARCHAR(255);
             );
             
             INSERT INTO table_name
                 (COL_NAME, COLUMN_NAME_2, LONGER_COLUMN_NAME, SHORT)
             VALUES
                 ('Cell1', 'Cell2',  'Cell3',  'Cell4'),
                 ('Cell5', 'Cell6',  'Cell7',  'Cell8'),
                 ('Cell9', 'Cell10', 'Cell11', 'Cell12');

            `;
            const decoder = new SqlDecoder();
            // When
            const actual = decoder.decode(input).getTableModel();
            // Then
            assertArrayEquals(expected, actual.getData());
        },

        supports_whenInputNotSql_shouldReturnNull: () => {
            // Given
            const input = 'Some string';
            const decoder = new SqlDecoder();
            // Then
            assertNull(decoder.supports(input));
        },

        supports_whenInputSql_shouldReturnFormatSql: () => {
            // Given
            const input = `
    
             CREATE TABLE table_name (
                 COL_NAME           VARCHAR(255),
                 COLUMN_NAME_2      VARCHAR(255),
                 LONGER_COLUMN_NAME VARCHAR(255),
                 SHORT              VARCHAR(255);
             );
             
             INSERT INTO table_name
                 (COL_NAME, COLUMN_NAME_2, LONGER_COLUMN_NAME, SHORT)
             VALUES
                 ('Cell1', 'Cell2',  'Cell3',  'Cell4'),
                 ('Cell5', 'Cell6',  'Cell7',  'Cell8'),
                 ('Cell9', 'Cell10', 'Cell11', 'Cell12');

            `;
            const decoder = new SqlDecoder();
            // Then
            assertEquals(Format.SQL, decoder.supports(input));
        }
    });

