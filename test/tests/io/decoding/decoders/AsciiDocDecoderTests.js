/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('decoders.AsciiDocDecoder')

    .beforeAll(ctx => {
        ctx.decoder = new AsciiDocDecoder();
        ctx.data = [
            ['CELL A',    'CELL BBBB', 'CELL CC'  ],
            ['CELL AA',   'Cell BBB',  'Cell CCC' ],
            ['CELL AAA',  'Cell BB',   'Cell C'   ],
            ['CELL AAAA', 'Cell B',    'Cell CCCC'],
        ];
        ctx.alignments = ArrayUtils.repeat(Alignment.LEFT, 3);
        ctx.minifiedNoHeader = `
                |==================================
                | CELL A    | CELL BBBB | CELL CC  
                | CELL AA   | Cell BBB  | Cell CCC 
                | CELL AAA  | Cell BB   | Cell C
                | CELL AAAA | Cell B    | Cell CCCC
                |==================================
            `;
    })

    .run({
        supports_whenValidAsciiDocSyntax_shouldReturnAsciiDocFormat: (ctx) => {
            // When
            const actual = ctx.decoder.supports(ctx.minifiedNoHeader);
            // Then
            assertEquals(Format.ASCII_DOC, actual);
        },

        decode_whenNoHeaderAndMinify_shouldReturnCorrectTableModel: (ctx) => {
            // When
            const actual = ctx.decoder.decode(ctx.minifiedNoHeader);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(HeaderOrientation.NO_HEADER, metadata.getHeaderOrientation());
            assertNull(metadata.getHeaderCase());
            assertEquals(ctx.data, model.getData());
        },

        decode_whenFirstRowAsHeaderAndMinify_shouldReturnCorrectTableModel: (ctx) => {
            // Given
            const input = `
                |==================================
                | CELL A    | CELL BBBB | CELL CC  
                
                | CELL AA   | Cell BBB  | Cell CCC 
                | CELL AAA  | Cell BB   | Cell C
                | CELL AAAA | Cell B    | Cell CCCC
                |==================================
            `;
            // When
            const actual = ctx.decoder.decode(input);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(HeaderOrientation.FIRST_ROW, metadata.getHeaderOrientation());
            assertEquals(TextCase.UPPER, metadata.getHeaderCase());
            assertEquals(ctx.alignments, model.getAlignments());
            assertEquals(ctx.data, model.getData());
        },

        decode_whenFirstColumnAsHeaderAndMinify_shouldReturnCorrectTableModel: (ctx) => {
            // Given
            const input = `
                [cols="h,1,1"]
                |==================================
                | CELL A    | CELL BBBB | CELL CC  
                | CELL AA   | Cell BBB  | Cell CCC 
                | CELL AAA  | Cell BB   | Cell C
                | CELL AAAA | Cell B    | Cell CCCC
                |==================================
            `;
            // When
            const actual = ctx.decoder.decode(input);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(HeaderOrientation.FIRST_COLUMN, metadata.getHeaderOrientation());
            assertEquals(TextCase.UPPER, metadata.getHeaderCase());
            assertEquals(ctx.alignments, model.getAlignments());
            assertEquals(ctx.data, model.getData());
        },

        decode_whenMultipleColumnAlignmentsAndMinify_shouldReturnCorrectTableModel: (ctx) => {
            // Given
            const input = `
                [cols="<h,^1,>1"]
                |==================================
                | CELL A    | CELL BBBB | CELL CC  
                | CELL AA   | Cell BBB  | Cell CCC 
                | CELL AAA  | Cell BB   | Cell C
                | CELL AAAA | Cell B    | Cell CCCC
                |==================================
            `;
            const expectedAlignments = [Alignment.LEFT, Alignment.CENTER, Alignment.RIGHT];
            // When
            const actual = ctx.decoder.decode(input);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(HeaderOrientation.FIRST_COLUMN, metadata.getHeaderOrientation());
            assertEquals(TextCase.UPPER, metadata.getHeaderCase());
            assertEquals(expectedAlignments, model.getAlignments());
            assertEquals(ctx.data, model.getData());
        },

        decode_whenNoHeaderAndPrettify_shouldReturnCorrectTableModel: (ctx) => {
            // Given
            const input = `
                |===
                | CELL A | CELL BBBB | CELL CC
                | CELL AA
                | Cell BBB
                | Cell CCC
                | CELL AAA
                | Cell BB
                | Cell C
                | CELL AAAA
                | Cell B
                | Cell CCCC
                |===
            `;
            // When
            const actual = ctx.decoder.decode(input);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(HeaderOrientation.NO_HEADER, metadata.getHeaderOrientation());
            assertNull(metadata.getHeaderCase());
            assertEquals(ctx.alignments, model.getAlignments());
            assertEquals(ctx.data, model.getData());
        },

        decode_whenFirstRowAsHeaderAndPrettify_shouldReturnCorrectTableModel: (ctx) => {
            // Given
            const input = `
                |===
                | CELL A | CELL BBBB | CELL CC
                    
                | CELL AA
                | Cell BBB
                | Cell CCC
                | CELL AAA
                | Cell BB
                | Cell C
                | CELL AAAA
                | Cell B
                | Cell CCCC
                |===
            `;
            // When
            const actual = ctx.decoder.decode(input);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(HeaderOrientation.FIRST_ROW, metadata.getHeaderOrientation());
            assertEquals(TextCase.UPPER, metadata.getHeaderCase());
            assertEquals(ctx.alignments, model.getAlignments());
            assertEquals(ctx.data, model.getData());
        },

        decode_whenFirstColumnAsHeaderAndPrettify_shouldReturnCorrectTableModel: (ctx) => {
            // Given
            const input = `
                [cols="h,1,1"]
                |===
                | CELL A | CELL BBBB | CELL CC
                | CELL AA
                | Cell BBB
                | Cell CCC
                | CELL AAA
                | Cell BB
                | Cell C
                | CELL AAAA
                | Cell B
                | Cell CCCC
                |===
            `;
            // When
            const actual = ctx.decoder.decode(input);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(HeaderOrientation.FIRST_COLUMN, metadata.getHeaderOrientation());
            assertEquals(TextCase.UPPER, metadata.getHeaderCase());
            assertEquals(ctx.alignments, model.getAlignments());
            assertEquals(ctx.data, model.getData());
        },

        decode_whenEmptyCells_shouldReturnCorrectTableModel: (ctx) => {
            // Given
            const input = `
                [cols="<1,<1,<1"]
                |===
                |  | B | C
                
                | AA
                | 
                | CC
                | AAA
                | BBB
                | 
                |===
            `;
            const expectedData = [
                ['',    'B'  , 'C' ],
                ['AA',  ''   , 'CC'],
                ['AAA', 'BBB', ''  ]
            ];
            // When
            const actual = ctx.decoder.decode(input);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(HeaderOrientation.FIRST_ROW, metadata.getHeaderOrientation());
            assertEquals(TextCase.UPPER, metadata.getHeaderCase());
            assertEquals(ctx.alignments, model.getAlignments());
            assertEquals(expectedData, model.getData());
        },
    });
