/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('decoders.HtmlDecoder')

    .run({
        decode_shouldReturnCorrectTableModel: () => {
            // Given
            const header1 = 'header1',
                header2 = 'header2',
                header3 = 'header3',
                header4 = 'header4',
                cell1 = 'cell1',
                cell2 = 'cell2',
                cell3 = '',
                cell4 = 'cell4',
                cell5 = 'cell5',
                cell6 = '',
                cell7 = '',
                cell8 = 'cell8';
            const input = `
         <table>
           <thead>
             <tr>
               <th>` + header1 + `</th>
               <th>` + header2 + `</th>
               <th>` + header3 + `</th>
               <th>` + header4 + `</th>
             </tr>
           </thead>
           <tbody>
             <tr>
               <td>` + cell1 + `</td>
               <td style="text-align: Center;">` + cell2 + `</td>
               <td style="text-align: Right;">` + cell3 + `</td>
               <td>` + cell4 + `</td>
             </tr>
             <tr>
               <td>` + cell5 + `</td>
               <td style="text-align: Center;">` + cell6 + `</td>
               <td style="text-align: Right;">` + cell7 + `</td>
               <td>` + cell8 + `</td>
             </tr>
           </tbody>
         </table>
        `;
            const expectedData = [
                [header1, header2, header3, header4],
                [cell1, cell2, cell3, cell4],
                [cell5, cell6, cell7, cell8]
            ];
            const expectedAlignments = [Alignment.LEFT, Alignment.CENTER, Alignment.RIGHT, Alignment.LEFT];
            const expected = new TableModel(expectedAlignments, expectedData);
            const decoder = new HtmlDecoder();
            // When
            const actual = decoder.decode(input).getTableModel();
            // Then
            assertArrayEquals(expected.getData(), actual.getData());
            assertArrayEquals(expected.getAlignments(), actual.getAlignments());
        },

        decode_whenOutputPolicyMinify_shouldDetectOutputPolicy: () => {
            // Given
            const input = '<table><thead><tr><th>Hello</th><th>World</th></tr></thead><tbody><tr><td>a1</td><td>a2</td></tr></tbody></table>';
            // When
            const decoder = new HtmlDecoder();
            const actual = decoder.decode(input);
            // Then
            assertEquals(OutputPolicy.MINIFY, actual.getMetadata().getOutputPolicy());
        },

        decode_whenOutputPolicyPrettify_shouldDetectOutputPolicy: () => {
            // Given
            const input = '\n' +
                ' <table>\n' +
                '   <thead>\n' +
                '     <tr>\n' +
                '       <th>Hello</th>\n' +
                '       <th>World</th>\n' +
                '     </tr>\n' +
                '   </thead>\n' +
                '   <tbody>\n' +
                '     <tr>\n' +
                '       <td>a1</td>\n' +
                '       <td>a2</td>\n' +
                '     </tr>\n' +
                '   </tbody>\n' +
                ' </table>\n';
            // When
            const decoder = new HtmlDecoder();
            const actual = decoder.decode(input);
            // Then
            assertEquals(OutputPolicy.PRETTIFY, actual.getMetadata().getOutputPolicy());
        },

        decode_whenHeaderOrientationFirstColumnAsHeader_shouldDetectHeaderOrientation: () => {
            // Given
            const input = '\n' +
                ' <table>\n' +
                '   <tr>\n' +
                '     <th>Hello</th>\n' +
                '     <td>World</td>\n' +
                '   </tr>\n' +
                '   <tr>\n' +
                '     <th>a1</th>\n' +
                '     <td>a2</td>\n' +
                '   </tr>\n' +
                ' </table>\n';
            // When
            const decoder = new HtmlDecoder();
            const actual = decoder.decode(input);
            // Then
            assertEquals(HeaderOrientation.FIRST_COLUMN, actual.getMetadata().getHeaderOrientation());
        },

        decode_whenFirstRowAsHeaderAndCapitalize_shouldDetectHeaderCaseAndOrientation: () => {
            // Given
            const input = '\n' +
                ' <table>\n' +
                '   <thead>\n' +
                '     <tr>\n' +
                '       <th style="text-align: center;">Hello</th>\n' +
                '       <th style="text-align: center;">World</th>\n' +
                '     </tr>\n' +
                '   </thead>\n' +
                '   <tbody>\n' +
                '     <tr>\n' +
                '       <td>a1</td>\n' +
                '       <td>a2</td>\n' +
                '     </tr>\n' +
                '   </tbody>\n' +
                ' </table>\n';
            // When
            const decoder = new HtmlDecoder();
            const actual = decoder.decode(input);
            // Then
            const metadata = actual.getMetadata();
            assertEquals(Alignment.CENTER, metadata.getHeaderAlignment());
            assertEquals(TextCase.CAPITALIZE, metadata.getHeaderCase());
            assertEquals(HeaderOrientation.FIRST_ROW, metadata.getHeaderOrientation());
        },

        decode_whenFirstColumnAsHeaderAndUppercase_shouldDetectHeaderCaseAndOrientation: () => {
            // Given
            const input = '\n' +
                ' <table>\n' +
                '   <tr>\n' +
                '     <th style="text-align: center;">HELLO</th>\n' +
                '     <td>World</td>\n' +
                '   </tr>\n' +
                '   <tr>\n' +
                '     <th style="text-align: center;">A1</th>\n' +
                '     <td>a2</td>\n' +
                '   </tr>\n' +
                ' </table>\n';
            // When
            const decoder = new HtmlDecoder();
            const actual = decoder.decode(input);
            // Then
            const metadata = actual.getMetadata();
            assertEquals(Alignment.CENTER, metadata.getHeaderAlignment());
            assertEquals(TextCase.UPPER, metadata.getHeaderCase());
            assertEquals(HeaderOrientation.FIRST_COLUMN, actual.getMetadata().getHeaderOrientation());
        }
    });


