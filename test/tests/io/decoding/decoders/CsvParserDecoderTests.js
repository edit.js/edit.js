/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('decoders.CsvDecoder')

    .run({
        decode_shouldReturnCorrectTableModel: () => {
            // Given
            const SEPARATOR = ',';
            const value1 = 'value1',
                value2 = 'value2',
                value3 = 'value3',
                value4 = 'value4';
            const expectedData = [
                [value1, value2],
                [value3, value4]
            ];
            const input = '' +
                value1 + SEPARATOR + value2 + '\n' +
                value3 + SEPARATOR + value4;
            const decoder = new CsvDecoder(SEPARATOR);
            // When
            const actual = decoder.decode(input).getTableModel();
            // Then
            assertArrayEquals(expectedData, actual.getData());
        }
    });

