/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('decoders.PlainTextDecoder')

    .beforeAll(ctx => {
        ctx.truthTable = `
            ┌───┬──────┐
            │ ^ │ F  T │
            ├───┼──────┤
            │ F │ 0  0 │
            │ T │ 0  1 │
            └───┴──────┘
            `;
        ctx.firstColumnAndHeaderOnly = `
            +---------+-----------------------------------+
            | Header1 | foo bar       c2        c3        |
            | Header2 | c4       baz 123  centered column |
            | Header3 | c5            c6        c7        |
            +---------+-----------------------------------+
            `;
        ctx.noHeaderAndHeaderOnly = `
            +---------+---------+---------+-----------------+
            | Header1 | foo bar |      c2 |       c3        |
            | Header2 | c4      | baz 123 | centered column |
            | Header3 | c5      |      c6 |       c7        |
            +---------+---------+---------+-----------------+
            `;
        ctx.firstRowAndHeaderOnly = `
            +---------+---------+---------+-----------------+
            | Header1 | foo bar |      c2 |       c3        |
            +---------+---------+---------+-----------------+
            | Header2 | c4      | baz 123 | centered column |
            | Header3 | c5      |      c6 |       c7        |
            +---------+---------+---------+-----------------+
            `;
        ctx.firstColumnAndBorderless = `
            Header1  foo bar       c2        c3        
            Header2  c4       baz 123  centered column 
            Header3  c5            c6        c7        
            `;
        ctx.firstRowAndEveryRow = `
            +---------+---------+---------+-----------------+
            | Header1 | foo bar |      c2 |       c3        |
            +---------+---------+---------+-----------------+
            | Header2 | c4      | baz 123 | centered column |
            +---------+---------+---------+-----------------+
            | Header3 | c5      |      c6 |       c7        |
            +---------+---------+---------+-----------------+
            `;
        ctx.firstColumnAndEveryRow = `
            +---------+---------+---------+-----------------+
            | Header1 | foo bar |      c2 |       c3        |
            +---------+---------+---------+-----------------+
            | Header2 | c4      | baz 123 | centered column |
            +---------+---------+---------+-----------------+
            | Header3 | c5      |      c6 |       c7        |
            +---------+---------+---------+-----------------+
            `;
        ctx.unicode = `
            ┌─────────┬─────────┬─────────┬─────────────────┐
            │ Header1 │ foo bar │      c2 │       c3        │
            ├─────────┼─────────┼─────────┼─────────────────┤
            │ Header2 │ c4      │ baz 123 │ centered column │
            ├─────────┼─────────┼─────────┼─────────────────┤
            │ Header3 │ c5      │      c6 │       c7        │
            └─────────┴─────────┴─────────┴─────────────────┘
            `;
        ctx.unicodeBold = `
            ╔═════════╦═════════╦═════════╦═════════════════╗
            ║ Header1 ║ foo bar ║      c2 ║       c3        ║
            ║ Header2 ║ c4      ║ baz 123 ║ centered column ║
            ║ Header3 ║ c5      ║      c6 ║       c7        ║
            ╚═════════╩═════════╩═════════╩═════════════════╝
            `;
        ctx.scarcedFirstRowAndFirstColumn = `
                +---+----------------+
                | a | b              |
                +---+----------------+
                | D | e  f           |
                |   |                |
                |   |                |
                |   |                |
                |   |                |
                |   |                |
                |   |                |
                | G | h              |
                +---+----------------+
                `;
        ctx.scarcedBorderless = `
                a  b              
                D  e  f           
                                  
                                  
                                  
                                  
                                  
                                  
                G  h              
                `;
        ctx.scarcedData = [
            ['a', 'b', '' ],
            ['D', 'e', 'f'],
            ['',  '',  '' ],
            ['',  '',  '' ],
            ['',  '',  '' ],
            ['',  '',  '' ],
            ['',  '',  '' ],
            ['',  '',  '' ],
            ['G', 'h', '' ],
        ];
        ctx.data = [
            ['Header1', 'foo bar',       'c2',        'c3'],
            ['Header2', 'c4',       'baz 123',  'centered column'],
            ['Header3', 'c5',            'c6',        'c7']
        ];
        ctx.alignments = [Alignment.LEFT, Alignment.LEFT, Alignment.RIGHT, Alignment.CENTER];
    })

    .run({
        decode_whenHeaderFirstRowAndBorderFirstRowAndFirstColumn: (ctx) => {
            // Given
            const expectedData = [
                ['^', 'F', 'T'],
                ['F', '0', '0'],
                ['T', '0', '1']
            ];
            const expectedAlignments = ArrayUtils.repeat(Alignment.LEFT, 3);
            const decoder = new PlainTextDecoder(Format.UNICODE);
            // When
            const actual = decoder.decode(ctx.truthTable);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(Alignment.LEFT, metadata.getHeaderAlignment());
            assertEquals(TextCase.UPPER, metadata.getHeaderCase());
            assertEquals(BorderPolicy.FIRST, metadata.getRowBorderPolicy());
            assertEquals(BorderPolicy.FIRST, metadata.getColumnBorderPolicy());
            assertEquals(HeaderOrientation.FIRST_ROW, metadata.getHeaderOrientation());
            assertArrayEquals(expectedData, model.getData());
            assertArrayEquals(expectedAlignments, model.getAlignments());
        },

        decode_whenHeaderFirstColumnAndBorderFirstColumn: (ctx) => {
            // Given
            const decoder = new PlainTextDecoder(Format.PLAIN_ASCII);
            // When
            const actual = decoder.decode(ctx.firstColumnAndHeaderOnly);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(Alignment.LEFT, metadata.getHeaderAlignment());
            assertEquals(TextCase.CAPITALIZE, metadata.getHeaderCase());
            assertEquals(BorderPolicy.OUTLINE, metadata.getRowBorderPolicy());
            assertEquals(BorderPolicy.FIRST, metadata.getColumnBorderPolicy());
            assertEquals(HeaderOrientation.FIRST_COLUMN, metadata.getHeaderOrientation());
            assertArrayEquals(ctx.data, model.getData());
            assertArrayEquals(ctx.alignments, model.getAlignments());
        },

        decode_whenHeaderFirstRowAndBorderFirstRowAndEveryColumn: (ctx) => {
            // Given
            const decoder = new PlainTextDecoder(Format.PLAIN_ASCII);
            // When
            const actual = decoder.decode(ctx.firstRowAndHeaderOnly);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(TextCase.LOWER, metadata.getHeaderCase());
            assertEquals(BorderPolicy.FIRST, metadata.getRowBorderPolicy());
            assertEquals(BorderPolicy.EVERY, metadata.getColumnBorderPolicy());
            assertEquals(HeaderOrientation.FIRST_ROW, metadata.getHeaderOrientation());
            assertArrayEquals(ctx.data, model.getData());
            assertArrayEquals(ctx.alignments, model.getAlignments());
        },

        decode_whenHeaderFirstColumnAndBorderEveryRowAndColumn: (ctx) => {
            // Given
            const decoder = new PlainTextDecoder(Format.PLAIN_ASCII);
            // When
            const actual = decoder.decode(ctx.firstColumnAndEveryRow);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(BorderPolicy.EVERY, metadata.getRowBorderPolicy());
            assertEquals(BorderPolicy.EVERY, metadata.getColumnBorderPolicy());
            assertEquals(HeaderOrientation.FIRST_COLUMN, metadata.getHeaderOrientation());
            assertEquals(TextCase.CAPITALIZE, metadata.getHeaderCase());
            assertEquals(Alignment.LEFT, metadata.getHeaderAlignment());
            assertArrayEquals(ctx.data, model.getData());
            assertArrayEquals(ctx.alignments, model.getAlignments());
        },

        decode_whenHeaderFirstColumnAndBorderEveryColumn: (ctx) => {
            // Given
            const decoder = new PlainTextDecoder(Format.PLAIN_ASCII);
            // When
            const actual = decoder.decode(ctx.noHeaderAndHeaderOnly);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(Alignment.LEFT, metadata.getHeaderAlignment());
            assertEquals(TextCase.CAPITALIZE, metadata.getHeaderCase());
            assertEquals(BorderPolicy.OUTLINE, metadata.getRowBorderPolicy());
            assertEquals(BorderPolicy.EVERY, metadata.getColumnBorderPolicy());
            assertEquals(HeaderOrientation.FIRST_COLUMN, metadata.getHeaderOrientation());
            assertArrayEquals(ctx.data, model.getData());
            assertArrayEquals(ctx.alignments, model.getAlignments());
        },

        decode_whenHeaderFirstColumnAndBorderless: (ctx) => {
            // Given
            const decoder = new PlainTextDecoder(Format.PLAIN_ASCII);
            // When
            const actual = decoder.decode(ctx.firstColumnAndBorderless);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(Alignment.LEFT, metadata.getHeaderAlignment());
            assertEquals(TextCase.CAPITALIZE, metadata.getHeaderCase());
            assertEquals(BorderPolicy.NONE, metadata.getRowBorderPolicy());
            assertEquals(BorderPolicy.NONE, metadata.getColumnBorderPolicy());
            assertEquals(HeaderOrientation.FIRST_COLUMN, metadata.getHeaderOrientation());
            assertArrayEquals(ctx.data, model.getData());
            assertArrayEquals(ctx.alignments, model.getAlignments());
        },

        decode_whenHeaderCaseUpper_shouldDetectCorrectHeaderCase: (ctx) => {
            // Given
            const input = `
            +---------+---------+---------+-----------------+
            | HEADER1 | foo bar |      c2 |       c3        |
            | HEADER2 | c4      | baz 123 | centered column |
            | HEADER3 | c5      |      c6 |       c7        |
            +---------+---------+---------+-----------------+
            `;
            const decoder = new PlainTextDecoder(Format.PLAIN_ASCII);
            // When
            const actual = decoder.decode(input);
            // Then
            const metadata = actual.getMetadata();
            assertEquals(TextCase.UPPER, metadata.getHeaderCase());
        },

        decode_shouldSupportUnicodeFormat: (ctx) => {
            // Given
            const decoder = new PlainTextDecoder(Format.UNICODE);
            // When
            const actual = decoder.decode(ctx.unicode);
            // Then
            const model = actual.getTableModel();
            assertArrayEquals(ctx.data, model.getData());
        },

        decode_shouldSupportUnicodeBoldFormat: (ctx) => {
            // Given
            const decoder = new PlainTextDecoder(Format.UNICODE_BOLD);
            // When
            const actual = decoder.decode(ctx.unicodeBold);
            // Then
            const model = actual.getTableModel();
            assertArrayEquals(ctx.data, model.getData());
        },

        decode_whenBorderlessAndHTMLCharacters_shouldReturnCorrectTableModel: () => {
            // Given
            const a = 'a', b = 'b', c = 'c', d = 'd', e = 'e',        f = 'f',
                  g = 'g', h = 'h', i = 'i', j = 'j', k = '<a>k</a>', l = 'l',
                  m = 'm', n = 'n', o = 'o', p = 'p', q = 'q',        r = 'r';
            const expectedData = [
                [a, b, c, d, e, f],
                [g, h, i, j, k, l],
                [m, n, o, p, q, r],
            ];
            const expectedAlignments = ArrayUtils.repeat(Alignment.LEFT, 6);
            const expected = new TableModel(expectedAlignments, expectedData);
            const input = '\n\n\n' +
                a + '  ' + b + '  ' + c + '  ' + d + '  ' + e + '         ' + f + '  \n' +
                g + '  ' + h + '  ' + i + '  ' + j + '  ' + k + '  '        + l + '  \n' +
                m + '  ' + n + '  ' + o + '  ' + p + '  ' + q + '         ' + r + '  \n'
            '\n';
            const decoder = new PlainTextDecoder(Format.PLAIN_ASCII);
            // When
            const actual = decoder.decode(input).getTableModel();
            // Then
            assertArrayEquals(expected.getData(), actual.getData());
            assertArrayEquals(expected.getAlignments(), actual.getAlignments());
        },

        decode_whenBorderlessAnd4xN_shouldReturnCorrectTableModel: () => {
            // Given
            const input = `

                OPERATION          SHORTCUT              CONTEXT  VERSION
                Undo               CTRL + Z              Global   1.2.0
                Redo               CTRL + Y              Global   1.2.0
                Close dialog       ESCAPE                Dialog   1.2.0
                Insert row         ENTER                 Editor   2.0.0
                Insert column      ALT + ENTER           Editor   2.0.0
                Delete row         CTRL + D              Editor   2.0.0
                Delete column      ALT + D               Editor   2.0.0
                Duplicate row      CTRL + SHIFT + DOWN   Editor   2.0.0
                Duplicate column   CTRL + SHIFT + RIGHT  Editor   2.0.0
                Move row up        ALT + SHIFT + UP      Editor   2.0.0
                Move row down      ALT + SHIFT + DOWN    Editor   2.0.0
                Move column left   ALT + SHIFT + LEFT    Editor   2.0.0
                Move column right  ALT + SHIFT + RIGHT   Editor   2.0.0
                Next cell up       ALT + UP              Editor   2.0.0
                Next cell left     ALT + LEFT            Editor   2.0.0
                Next cell down     ALT + DOWN            Editor   2.0.0
                Next cell right    ALT + RIGHT           Editor   2.0.0

            `;
            const expectedData = [
                ['OPERATION',          'SHORTCUT',              'CONTEXT',  'VERSION'],
                ['Undo',               'CTRL + Z',              'Global',   '1.2.0'],
                ['Redo',               'CTRL + Y',              'Global',   '1.2.0'],
                ['Close dialog',       'ESCAPE',                'Dialog',   '1.2.0'],
                ['Insert row',         'ENTER',                 'Editor',   '2.0.0'],
                ['Insert column',      'ALT + ENTER',           'Editor',   '2.0.0'],
                ['Delete row',         'CTRL + D',              'Editor',   '2.0.0'],
                ['Delete column',      'ALT + D',               'Editor',   '2.0.0'],
                ['Duplicate row',      'CTRL + SHIFT + DOWN',   'Editor',   '2.0.0'],
                ['Duplicate column',   'CTRL + SHIFT + RIGHT',  'Editor',   '2.0.0'],
                ['Move row up',        'ALT + SHIFT + UP',      'Editor',   '2.0.0'],
                ['Move row down',      'ALT + SHIFT + DOWN',    'Editor',   '2.0.0'],
                ['Move column left',   'ALT + SHIFT + LEFT',    'Editor',   '2.0.0'],
                ['Move column right',  'ALT + SHIFT + RIGHT',   'Editor',   '2.0.0'],
                ['Next cell up',       'ALT + UP',              'Editor',   '2.0.0'],
                ['Next cell left',     'ALT + LEFT',            'Editor',   '2.0.0'],
                ['Next cell down',     'ALT + DOWN',            'Editor',   '2.0.0'],
                ['Next cell right',    'ALT + RIGHT',           'Editor',   '2.0.0']
            ];
            const expectedAlignments = ArrayUtils.repeat(Alignment.LEFT, 4);
            const expected = new TableModel(expectedAlignments, expectedData);
            const decoder = new PlainTextDecoder(Format.PLAIN_ASCII);
            // When
            const actual = decoder.decode(input).getTableModel();
            // Then
            assertArrayEquals(expected.getData(), actual.getData());
            assertArrayEquals(expected.getAlignments(), actual.getAlignments());
        },

        decode_whenMultipleEmptyCellsAndBorderFirstRowAndFirstColumn_shouldReturnCorrectTableModel: (ctx) => {
            // Given
            const expectedAlignments = ArrayUtils.repeat(Alignment.LEFT, 3);
            const decoder = new PlainTextDecoder(Format.PLAIN_ASCII);
            // When
            const actual = decoder.decode(ctx.scarcedFirstRowAndFirstColumn);
            // Then
            const model = actual.getTableModel();
            assertArrayEquals(expectedAlignments, model.getAlignments());
            assertArrayEquals(ctx.scarcedData, model.getData());
        },

        decode_whenMultipleEmptyCellsAndBorderless_shouldReturnCorrectTableModel: (ctx) => {
            // Given
            const expectedAlignments = ArrayUtils.repeat(Alignment.LEFT, 3);
            const decoder = new PlainTextDecoder(Format.PLAIN_ASCII);
            // When
            const actual = decoder.decode(ctx.scarcedBorderless);
            // Then
            const model = actual.getTableModel();
            assertArrayEquals(expectedAlignments, model.getAlignments());
            assertArrayEquals(ctx.scarcedData, model.getData());
        },

        detectAlignments_shouldAutomaticallyDetectAlignments: () => {
            // Given
            const expected = [Alignment.LEFT, Alignment.CENTER, Alignment.RIGHT];
            const arr = [
                ['01234', '012345', '0123'],
                ['012  ', '  12  ', '  01']
            ];
            const decoder = new PlainTextDecoder(Format.PLAIN_ASCII);
            // When
            const actual = decoder.detectAlignments(arr);
            // Then
            assertArrayEquals(expected, actual);
        },

        isDivider_whenDivider_shouldReturnTrue: () => {
            // Given
            const expected = [true, false, true];
            const rows = [
                '+-------------+',
                '| hello world |',
                '+-------------+'
            ];
            const decoder = new PlainTextDecoder(Format.PLAIN_ASCII);
            // When
            const actual = rows.map(row => decoder.isDivider(row));
            // Then
            assertArrayEquals(expected, actual);
        }
    })
