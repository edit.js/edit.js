/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('decoders.JsonDecoder')

    .beforeAll(context => {
        context.decoder = new JsonDecoder();
    })

    .run({
        decode_shouldReturnCorrectTableModel: (ctx) => {
            // Given
            const expectedData = [
                ['value1', 'value2'],
                ['value3', 'value4']
            ];
            const input = JSON.stringify(expectedData);
            // When
            const actual = ctx.decoder.decode(input).getTableModel();
            // Then
            assertArrayEquals(expectedData, actual.getData());
        },

        decode_whenOutputPolicyPrettify_shouldDetectOutputPolicy: (ctx) => {
            // Given
            const input =
                '' +
                ' [\n' +
                '   ["a","b"],\n' +
                '   ["c","d"]\n' +
                ' ]';
            // When
            const actual = ctx.decoder.decode(input);
            // Then
            assertEquals(OutputPolicy.PRETTIFY, actual.getMetadata().getOutputPolicy());
        },

        decode_whenOutputPolicyMinify_shouldDetectOutputPolicy: (ctx) => {
            // Given
            const input = '[["a","b"],["c","d"]]';
            // When
            const actual = ctx.decoder.decode(input);
            // Then
            assertEquals(OutputPolicy.MINIFY, actual.getMetadata().getOutputPolicy());
        },

        supports_whenFormatSupportedAndMultiline_shouldReturnTrue: (ctx) => {
            // Given
            const input = `

                    const expectedData = [
                        ["value1", "value2"],
                        ["value3", "value4"]
                    ];
            `;
            // Then
            assertTrue(ctx.decoder.supports(input));
        },

        supports_whenFormatSupportedAndSingleLine_shouldReturnTrue: (ctx) => {
            // Given
            const input = `

                    const expectedData = [["a","b"],["c","d"]];
            `;
            // Then
            assertTrue(ctx.decoder.supports(input));
        },

        decode_whenNoise_shouldProperlyParseJsonInput: (ctx) => {
            // Given
            const expected = [
                ['value1', 'value2'],
                ['value3', 'value4']
            ];
            const input = `

                    const expected = [
                        ["value1", "value2"],
                        ["value3", "value4"]
                    ];
            `;
            // When
            const actual = ctx.decoder.decode(input);
            // Then
            assertArrayEquals(expected, actual.getTableModel().getData());
        },

        decode_whenArrayOfObjects_shouldProperlyParseJsonInput: (ctx) => {
            // Given
            const expected = [
                ["value1", "value2"],
                ["3", "value4"]
            ];
            const input = `
                    const expected = [
                        {
                            "value1":  3,
                            "value2":  "value4"
                        }
                    ];
            `;
            const expectedStr = expected.map(row => row.join());
            // When
            const actual = ctx.decoder.decode(input);
            // Then
            const data = actual.getTableModel().getData();
            const actualStr = data.map(row => row.join());
            assertTrue(ArrayUtils.equalsIgnoreOrder(expectedStr, actualStr));
        }
    });



