/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('io.TableReader')

    .beforeAll((context) => {
        context.reader = new TableReader();
    })

    .run({
        detectFormat_whenAscii_shouldReturnAscii: (ctx) => {
            // Given
            const input = '\n' +
                '\n' +
                ' +---------+---------+\n' +
                ' | header1 | header2 |\n' +
                ' | cell1   | cell2   |\n' +
                ' | cell1   | cell2   |\n' +
                ' +---------+---------+\n' +
                '\n';
            // When
            const actual = ctx.reader.detectFormat(input);
            // Then
            assertEquals(Format.PLAIN_ASCII, actual);
        },

        detectFormat_whenUnicode_shouldReturnUnicode: (ctx) => {
            // Given
            const input = '\n' +
                ' ┌─────────┬─────────┐\n' +
                ' │ Header1 │ Header2 │\n' +
                ' ├─────────┼─────────┤\n' +
                ' │ Cell1   │ Cell2   │\n' +
                ' ├─────────┼─────────┤\n' +
                ' │ Cell1   │ Cell2   │\n' +
                ' └─────────┴─────────┘\n';
            // When
            const actual = ctx.reader.detectFormat(input);
            // Then
            assertEquals(Format.UNICODE, actual);
        },

        detectFormat_whenUnicodeBold_shouldReturnUnicodeBold: (ctx) => {
            // Given
            const input = '\n' +
                ' ╔═════════╦═════════╗\n' +
                ' ║ Header1 ║ Header2 ║\n' +
                ' ║ Cell1   ║ Cell2   ║\n' +
                ' ║ Cell1   ║ Cell2   ║\n' +
                ' ╚═════════╩═════════╝\n';
            // When
            const actual = ctx.reader.detectFormat(input);
            // Then
            assertEquals(Format.UNICODE_BOLD, actual);
        },

        detectFormat_whenGitHubMarkdown_shouldReturnGitHubMarkdown: (ctx) => {
            // Given
            const input = '\n' +
                ' | Hello | world |\n' +
                ' | :---- | :---- |\n' +
                ' | a1    | a2    |\n';
            // When
            const actual = ctx.reader.detectFormat(input);
            // Then
            assertEquals(Format.GITHUB_MARKDOWN, actual);
        },

        detectFormat_whenConfluenceMarkDown_shouldReturnConfluenceMarkDown: (ctx) => {
            // Given
            const input = '\n' +
                ' || Hello || world ||\n' +
                ' |  a1    |  a2     |\n';
            // When
            const actual = ctx.reader.detectFormat(input);
            // Then
            assertEquals(Format.CONFLUENCE_MARKDOWN, actual);
        },

        detectFormat_whenCucumberMarkdown_shouldReturnCucumberMarkdown: (ctx) => {
            // Given
            const input = '\n' +
                ' | Hello | world |\n' +
                ' | a1    | a2    |\n';
            // When
            const actual = ctx.reader.detectFormat(input);
            // Then
            assertEquals(Format.CUCUMBER_MARKDOWN, actual);
        },

        detectFormat_whenJson_shouldReturnJson: (ctx) => {
            // Given
            const input = '[["a","b"],["c","d"]]';
            // When
            const actual = ctx.reader.detectFormat(input);
            // Then
            assertEquals(Format.JSON, actual);
        },

        detectFormat_whenHtml_shouldReturnHtml: (ctx) => {
            // Given
            const input = '\n' +
                ' <table>\n' +
                '   <tr>\n' +
                '     <th>Hello</th>\n' +
                '     <td>World</td>\n' +
                '   </tr>\n' +
                '   <tr>\n' +
                '     <th>a1</th>\n' +
                '     <td>a2</td>\n' +
                '   </tr>\n' +
                ' </table>\n';
            // When
            const actual = ctx.reader.detectFormat(input);
            // Then
            assertEquals(Format.HTML, actual);
        }
    });
