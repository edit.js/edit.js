/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('encoders.LatexEncoder')

    .beforeAll(ctx => {
        ctx.emptyCellPlaceholder = '#';
        ctx.alignments = [];
        ctx.data = [
            ['a', 'b', 'c', 'd'],
            ['e', 'f', 'g', 'h'],
            ['i', 'j', 'k', 'l']
        ];
        ctx.missingCells = [
            ['a', null, 'c',  'd' ],
            ['e', 'f',  'g',  null],
            ['i', 'j',  null, 'l' ]
        ];
        ctx.split = (subject) => {
            return subject.split('\n');
        }
    })

    .run({
        encode_whenBorderPolicyFirstRowFirstColumn_shouldSerializeTableModel: (ctx) => {
            // Given
            const expected = ctx.split(
`\\begin{table}[!ht]
    \\centering
    \\caption{Table Caption}
    \\begin{tabular}{|l|lll|}
    \\hline
        a & b & c & d \\\\ \\hline
        e & f & g & h \\\\
        i & j & k & l \\\\ \\hline
    \\end{tabular}
    \\label{Table Label}
\\end{table}`
);
            const tableModel = new TableModel(ctx.alignments, ctx.data);
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.MINIFY)
                .setRowBorderPolicy(BorderPolicy.FIRST)
                .setColumnBorderPolicy(BorderPolicy.FIRST)
                .setFormat(Format.LATEX);
            const encoder = new LatexEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenBorderPolicyEveryRowAndEveryColumn_shouldSerializeTableModel: (ctx) => {
            // Given
            const expected = ctx.split(
`\\begin{table}[!ht]
    \\centering
    \\caption{Table Caption}
    \\begin{tabular}{|l|l|l|l|}
    \\hline
        a & b & c & d \\\\ \\hline
        e & f & g & h \\\\ \\hline
        i & j & k & l \\\\ \\hline
    \\end{tabular}
    \\label{Table Label}
\\end{table}`
);
            const tableModel = new TableModel(ctx.alignments, ctx.data);
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.MINIFY)
                .setRowBorderPolicy(BorderPolicy.EVERY)
                .setColumnBorderPolicy(BorderPolicy.EVERY)
                .setFormat(Format.LATEX);
            const encoder = new LatexEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenBorderless_shouldSerializeTableModel: (ctx) => {
            // Given
            const expected = ctx.split(
`\\begin{table}[!ht]
    \\centering
    \\caption{Table Caption}
    \\begin{tabular}{llll}
        a & b & c & d \\\\
        e & f & g & h \\\\
        i & j & k & l \\\\
    \\end{tabular}
    \\label{Table Label}
\\end{table}`);
            const tableModel = new TableModel(ctx.alignments, ctx.data);
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.MINIFY)
                .setRowBorderPolicy(BorderPolicy.NONE)
                .setColumnBorderPolicy(BorderPolicy.NONE)
                .setFormat(Format.LATEX);
            const encoder = new LatexEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenMissingCells_shouldSerializeTableModel: (ctx) => {
            // Given
            const _ = ctx.emptyCellPlaceholder;
            const expected = ctx.split(
`\\begin{table}[!ht]
    \\centering
    \\caption{Table Caption}
    \\begin{tabular}{|l|lll|}
    \\hline
        a & ${_} & c & d \\\\ \\hline
        e & f & g & ${_} \\\\
        i & j & ${_} & l \\\\ \\hline
    \\end{tabular}
    \\label{Table Label}
\\end{table}`);
            const tableModel = new TableModel(ctx.alignments, ctx.missingCells);
            const metadata = new TableMetadata()
                .setEmptyCellPlaceholder(_)
                .setOutputPolicy(OutputPolicy.MINIFY)
                .setRowBorderPolicy(BorderPolicy.FIRST)
                .setColumnBorderPolicy(BorderPolicy.FIRST)
                .setFormat(Format.LATEX);
            const encoder = new LatexEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenContainsHtmlCharacters_shouldProperlyRenderHtmlCharacters: (ctx) => {
            // Given
            const expected = [
                '\\begin{table}[!ht]',
                '    \\centering',
                '    \\caption{Table Caption}',
                '    \\begin{tabular}{|l|l|}',
                '    \\hline',
                '        <a>link</a> & b \\\\ \\hline',
                '        c & d \\\\ \\hline',
                '    \\end{tabular}',
                '    \\label{Table Label}',
                '\\end{table}'
            ];
            const data = [
                ['<a>link</a>', 'b'],
                ['c',           'd']
            ];
            const tableModel = new TableModel(ctx.alignments, data);
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.MINIFY)
                .setRowBorderPolicy(BorderPolicy.FIRST)
                .setColumnBorderPolicy(BorderPolicy.FIRST)
                .setFormat(Format.LATEX);
            const encoder = new LatexEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenPrettify_shouldSerializeTableModel: (ctx) => {
            // Given
            const data = [
                ['header1',   'header2',     'header3'],
                ['a',         'b',           'c'      ],
                ['Long cell', 'Longer cell', ''       ]
            ];
            const expected = ctx.split(
`\\begin{table}[!ht]
    \\centering
    \\caption{Table Caption}
    \\begin{tabular}{|l|ll|}
    \\hline
        header1   & header2     & header3 \\\\ \\hline
        a         & b           & c       \\\\
        Long cell & Longer cell &         \\\\ \\hline
    \\end{tabular}
    \\label{Table Label}
\\end{table}`
            );
            const tableModel = new TableModel(ctx.alignments, data);
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.PRETTIFY)
                .setRowBorderPolicy(BorderPolicy.FIRST)
                .setColumnBorderPolicy(BorderPolicy.FIRST)
                .setFormat(Format.LATEX);
            const encoder = new LatexEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        }
    });
