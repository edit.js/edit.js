/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('encoders.ConfluenceEncoder')

    .beforeAll((context) => {
        context.data = [
            ['header1', 'header2', 'header3'],
            ['a', 'b', 'c'],
            ['d', 'e', 'f']
        ];
        context.alignments = [];
    })

    .run({
        encode_whenPrettifyConfluence_shouldGeneratePrettifiedOutput: (ctx) => {
            // Given
            const expected = [
                '|| header1 || header2 || header3 ||',
                '|  a       |  b       |  c        |',
                '|  d       |  e       |  f        |',
            ];
            const tableModel = new TableModel(ctx.alignments, ctx.data);
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.PRETTIFY)
                .setFormat(Format.CONFLUENCE_MARKDOWN);
            const encoder = new ConfluenceEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenMinifyConfluence_shouldGenerateMinifiedOutput: (ctx) => {
            // Given
            const expected = [
                '||header1||header2||header3||',
                '|a|b|c|',
                '|d|e|f|',
            ];
            const tableModel = new TableModel(ctx.alignments, ctx.data);
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.MINIFY)
                .setFormat(Format.CONFLUENCE_MARKDOWN);
            const encoder = new ConfluenceEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenPrettifyCucumber_shouldGeneratePrettifiedOutput: (ctx) => {
            // Given
            const expected = [
                '| header1 | header2 | header3 |',
                '| a       | b       | c       |',
                '| d       | e       | f       |',
            ];
            const tableModel = new TableModel(ctx.alignments, ctx.data);
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.PRETTIFY)
                .setFormat(Format.CUCUMBER_MARKDOWN);
            const encoder = new ConfluenceEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenMinifyCucumber_shouldGenerateMinifiedOutput: (ctx) => {
            // Given
            const expected = [
                '|header1|header2|header3|',
                '|a|b|c|',
                '|d|e|f|',
            ];
            const tableModel = new TableModel(ctx.alignments, ctx.data);
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.MINIFY)
                .setFormat(Format.CUCUMBER_MARKDOWN);
            const encoder = new ConfluenceEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenContainsHtmlCharacters_shouldProperlyRenderHtmlCharacters: (ctx) => {
            // Given
            const expected = [
                '| header1               | <b>header2</b> | header3 |',
                '| <br>                  | b              | c       |',
                '| This is a longer cell | e              | f       |',
            ];
            const data = [
                ['header1',               '<b>header2</b>', 'header3'],
                ['<br>',                  'b',              'c'],
                ['This is a longer cell', 'e',              'f']
            ];
            const tableModel = new TableModel(ctx.alignments, data);
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.PRETTIFY)
                .setFormat(Format.CUCUMBER_MARKDOWN);
            const encoder = new ConfluenceEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenUppercaseCucumber_shouldPrintFirstRowWithUppercaseHeaders: (ctx) => {
            // Given
            const expected = [
                '|HEADER1|HEADER2|HEADER3|',
                '|a|b|c|',
                '|d|e|f|',
            ];
            const tableModel = new TableModel(ctx.alignments, ctx.data);
            const metadata = new TableMetadata()
                .setHeaderCase(TextCase.UPPER)
                .setOutputPolicy(OutputPolicy.MINIFY)
                .setFormat(Format.CUCUMBER_MARKDOWN);
            const encoder = new ConfluenceEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        }

    });
