/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('encoders.CsvEncoder')

    .run({
        encode_shouldGenerateCorrectOutput: () => {
            // Given
            const separator = ',';
            const expected = [
                'header1' + separator + 'header2' + separator + 'header3',
                'a' + separator + 'b' + separator + 'c'
            ];
            const data = [
                ['header1', 'header2', 'header3'],
                ['a', 'b', 'c']
            ];
            const metadata = new TableMetadata().setSeparator(separator);
            const tableModel = new TableModel([], data);
            const encoder = new CsvEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        }
    });

