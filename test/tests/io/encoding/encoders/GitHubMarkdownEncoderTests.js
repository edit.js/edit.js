/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('encoders.GitHubMarkdownEncoder')

    .beforeAll((context) => {
        context.data = [
            ['header1', 'header2', 'header3'],
            ['a',       'b',       'c'],
            ['d',       'e',       'f']
        ];
        context.createMetadata = (outputPolicy) => new TableMetadata().setOutputPolicy(outputPolicy);
    })

    .run({
        encode_whenPrettify_shouldGeneratePrettifiedOutput: (ctx) => {
            // Given
            const expected = [
                '| header1 | header2 | header3 |',
                '| :------ | :------ | :------ |',
                '| a       | b       | c       |',
                '| d       | e       | f       |',
            ];
            const alignments = [Alignment.LEFT, Alignment.LEFT, Alignment.LEFT];
            const tableModel = new TableModel(alignments, ctx.data);
            const encoder = new GitHubMarkdownEncoder(ctx.createMetadata(OutputPolicy.PRETTIFY));
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenMinify_shouldGeneratePrettifiedOutput: (ctx) => {
            // Given
            const expected = [
                '|header1|header2|header3|',
                '|:--|:--|:--|',
                '|a|b|c|',
                '|d|e|f|',
            ];
            const alignments = [Alignment.LEFT, Alignment.LEFT, Alignment.LEFT];
            const tableModel = new TableModel(alignments, ctx.data);
            const encoder = new GitHubMarkdownEncoder(ctx.createMetadata(OutputPolicy.MINIFY));
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenPrettify_shouldComplyToSpecifiedAlignments: (ctx) => {
            // Given
            const expected = [
                '| header1 | header2 | header3 |',
                '| :------ | :-----: | ------: |',
                '| a       | b       | c       |',
                '| d       | e       | f       |',
            ];
            const alignments = [Alignment.LEFT, Alignment.CENTER, Alignment.RIGHT];
            const tableModel = new TableModel(alignments, ctx.data);
            const encoder = new GitHubMarkdownEncoder(ctx.createMetadata(OutputPolicy.PRETTIFY));
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenMinify_shouldComplyToSpecifiedAlignments: (ctx) => {
            // Given
            const expected = [
                '|header1|header2|header3|',
                '|:--|:-:|--:|',
                '|a|b|c|',
                '|d|e|f|',
            ];
            const alignments = [Alignment.LEFT, Alignment.CENTER, Alignment.RIGHT];
            const tableModel = new TableModel(alignments, ctx.data);
            const encoder = new GitHubMarkdownEncoder(ctx.createMetadata(OutputPolicy.MINIFY));
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenHeaderUppercase_shouldPrintFirstRowAsUppercase: (ctx) => {
            // Given
            const expected = [
                '| HEADER1 | HEADER2 | HEADER3 |',
                '| :------ | :-----: | ------: |',
                '| a       | b       | c       |',
                '| d       | e       | f       |',
            ];
            const alignments = [Alignment.LEFT, Alignment.CENTER, Alignment.RIGHT];
            const tableModel = new TableModel(alignments, ctx.data);
            const metadata = new TableMetadata()
                .setHeaderCase(TextCase.UPPER)
                .setOutputPolicy(OutputPolicy.PRETTIFY);
            const encoder = new GitHubMarkdownEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        }
    });
