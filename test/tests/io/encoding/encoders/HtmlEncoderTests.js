/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('encoders.HtmlEncoder')

    .beforeAll((context) => {
        context.alignments = [Alignment.LEFT, Alignment.CENTER, Alignment.RIGHT];
        context.singleRowData = [
            ['header1', 'header2', 'header3'],
            ['a', 'b', 'c']
        ];
        context.multiRowData = [
            ['aa',      'bb',      'cc'],
            ['cell #1', 'cell #2', 'cell #3'],
            ['cell #4', 'cell #5', 'cell #6']
        ];
    })

    .run({
        encode_shouldComplyToSpecifiedAlignments: (ctx) => {
            // Given
            const expected = [
                '<table>',
                '  <thead>',
                '    <tr>',
                '      <th>header1</th>',
                '      <th>header2</th>',
                '      <th>header3</th>',
                '    </tr>',
                '  </thead>',
                '  <tbody>',
                '    <tr>',
                '      <td>a</td>',
                '      <td style="text-align: Center;">b</td>',
                '      <td style="text-align: Right;">c</td>',
                '    </tr>',
                '  </tbody>',
                '</table>'
            ];
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.PRETTIFY)
                .setHeaderOrientation(HeaderOrientation.FIRST_ROW);
            const tableModel = new TableModel(ctx.alignments, ctx.singleRowData);
            const encoder = new HtmlEncoder(metadata)
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenMinify_shouldGenerateMinifiedMarkup: (ctx) => {
            // Given
            const expected = [
                '<table><thead><tr><th>header1</th><th>header2</th><th>header3</th></tr></thead><tbody><tr><td>a</td><td style="text-align: Center;">b</td><td style="text-align: Right;">c</td></tr></tbody></table>'
            ];
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.MINIFY)
                .setHeaderOrientation(HeaderOrientation.FIRST_ROW);
            const tableModel = new TableModel(ctx.alignments, ctx.singleRowData);
            const encoder = new HtmlEncoder(metadata)
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenFirstColumnAsHeader_shouldUseFirstColumnAsHeader: (ctx) => {
            // Given
            const expected = [
                '<table>',
                '  <tr>',
                '    <th>header1</th>',
                '    <td style="text-align: Center;">a</td>',
                '    <td style="text-align: Right;">b</td>',
                '  </tr>',
                '  <tr>',
                '    <th>header2</th>',
                '    <td style="text-align: Center;">c</td>',
                '    <td style="text-align: Right;">d</td>',
                '  </tr>',
                '</table>'
            ];
            const data = [
                ['header1', 'a', 'b'],
                ['header2', 'c', 'd']
            ];
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.PRETTIFY)
                .setHeaderOrientation(HeaderOrientation.FIRST_COLUMN);
            const tableModel = new TableModel(ctx.alignments, data);
            const encoder = new HtmlEncoder(metadata)
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenFirstColumnAsHeaderAndHeaderUpperCase_shouldUpperCaseFirstColumn: (ctx) => {
            // Given
            const expected = [
                '<table>',
                '  <tr>',
                '    <th>AA</th>',
                '    <td style="text-align: Center;">bb</td>',
                '    <td style="text-align: Right;">cc</td>',
                '  </tr>',
                '  <tr>',
                '    <th>CELL #1</th>',
                '    <td style="text-align: Center;">cell #2</td>',
                '    <td style="text-align: Right;">cell #3</td>',
                '  </tr>',
                '  <tr>',
                '    <th>CELL #4</th>',
                '    <td style="text-align: Center;">cell #5</td>',
                '    <td style="text-align: Right;">cell #6</td>',
                '  </tr>',
                '</table>'
            ];
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.PRETTIFY)
                .setHeaderCase(TextCase.UPPER)
                .setHeaderOrientation(HeaderOrientation.FIRST_COLUMN);
            const tableModel = new TableModel(ctx.alignments, ctx.multiRowData);
            const encoder = new HtmlEncoder(metadata)
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenFirstRowAsHeaderAndHeaderUpperCase_shouldUpperCaseFirstRow: (ctx) => {
            // Given
            const expected = [
                '<table>',
                '  <thead>',
                '    <tr>',
                '      <th>AA</th>',
                '      <th>BB</th>',
                '      <th>CC</th>',
                '    </tr>',
                '  </thead>',
                '  <tbody>',
                '    <tr>',
                '      <td>cell #1</td>',
                '      <td style="text-align: Center;">cell #2</td>',
                '      <td style="text-align: Right;">cell #3</td>',
                '    </tr>',
                '    <tr>',
                '      <td>cell #4</td>',
                '      <td style="text-align: Center;">cell #5</td>',
                '      <td style="text-align: Right;">cell #6</td>',
                '    </tr>',
                '  </tbody>',
                '</table>'
            ];
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.PRETTIFY)
                .setHeaderCase(TextCase.UPPER)
                .setHeaderOrientation(HeaderOrientation.FIRST_ROW);
            const tableModel = new TableModel(ctx.alignments, ctx.multiRowData);
            const encoder = new HtmlEncoder(metadata)
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenFirstRowAsHeaderAndAlignRightHeaders_shouldAlignRightFirstRow: (ctx) => {
            // Given
            const expected = [
                '<table>',
                '  <thead>',
                '    <tr>',
                '      <th style="text-align: Right;">aa</th>',
                '      <th style="text-align: Right;">bb</th>',
                '      <th style="text-align: Right;">cc</th>',
                '    </tr>',
                '  </thead>',
                '  <tbody>',
                '    <tr>',
                '      <td>cell #1</td>',
                '      <td style="text-align: Center;">cell #2</td>',
                '      <td style="text-align: Right;">cell #3</td>',
                '    </tr>',
                '    <tr>',
                '      <td>cell #4</td>',
                '      <td style="text-align: Center;">cell #5</td>',
                '      <td style="text-align: Right;">cell #6</td>',
                '    </tr>',
                '  </tbody>',
                '</table>'
            ];
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.PRETTIFY)
                .setHeaderAlignment(Alignment.RIGHT)
                .setHeaderOrientation(HeaderOrientation.FIRST_ROW);
            const tableModel = new TableModel(ctx.alignments, ctx.multiRowData);
            const encoder = new HtmlEncoder(metadata)
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenFirstColumnAsHeaderAndAlignRightHeaders_shouldAlignRightFirstColumn: (ctx) => {
            // Given
            const expected = [
                '<table>',
                '  <tr>',
                '    <th style="text-align: Right;">aa</th>',
                '    <td style="text-align: Center;">bb</td>',
                '    <td style="text-align: Right;">cc</td>',
                '  </tr>',
                '  <tr>',
                '    <th style="text-align: Right;">cell #1</th>',
                '    <td style="text-align: Center;">cell #2</td>',
                '    <td style="text-align: Right;">cell #3</td>',
                '  </tr>',
                '  <tr>',
                '    <th style="text-align: Right;">cell #4</th>',
                '    <td style="text-align: Center;">cell #5</td>',
                '    <td style="text-align: Right;">cell #6</td>',
                '  </tr>',
                '</table>'
            ];
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.PRETTIFY)
                .setHeaderAlignment(Alignment.RIGHT)
                .setHeaderOrientation(HeaderOrientation.FIRST_COLUMN);
            const tableModel = new TableModel(ctx.alignments, ctx.multiRowData);
            const encoder = new HtmlEncoder(metadata)
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        }
    });



