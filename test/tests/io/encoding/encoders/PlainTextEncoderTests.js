/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('encoders.PlainTextEncoder')

    .beforeAll((context) => {
        context.singleRowData = [
            ['header1', 'header2', 'header3'],
            ['a',       'b',       'c']
        ];
        context.multiRowData = [
            ['header1', 'header2', 'header3'],
            ['a',       'b',       'c'],
            ['d',       'e',       'f']
        ];
        context.alignments = [Alignment.LEFT, Alignment.CENTER, Alignment.RIGHT];
    })

    .run({
        encode_shouldComplyToSpecifiedAlignments: (ctx) => {
            // Given
            const expected = [
                '+---------+---------+---------+',
                '| header1 | header2 | header3 |',
                '+---------+---------+---------+',
                '| a       |    b    |       c |',
                '+---------+---------+---------+'
            ];
            const metadata = new TableMetadata()
                .setRowBorderPolicy(BorderPolicy.FIRST)
                .setColumnBorderPolicy(BorderPolicy.EVERY)
                .setHeaderOrientation(HeaderOrientation.FIRST_ROW)
                .setFormat(Format.PLAIN_ASCII);
            const tableModel = new TableModel(ctx.alignments, ctx.singleRowData);
            const encoder = new PlainTextEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenUnicodeFormat_shouldGenerateUnicode: (ctx) => {
            // Given
            const expected = [
                '┌─────────┬─────────┬─────────┐',
                '│ header1 │ header2 │ header3 │',
                '├─────────┼─────────┼─────────┤',
                '│ a       │    b    │       c │',
                '└─────────┴─────────┴─────────┘',
            ];
            const metadata = new TableMetadata()
                .setRowBorderPolicy(BorderPolicy.FIRST)
                .setColumnBorderPolicy(BorderPolicy.EVERY)
                .setHeaderOrientation(HeaderOrientation.FIRST_ROW)
                .setFormat(Format.UNICODE);
            const tableModel = new TableModel(ctx.alignments, ctx.singleRowData);
            const encoder = new PlainTextEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenUnicodeBoldFormat_shouldGenerateUnicodeBold: (ctx) => {
            // Given
            const expected = [
                '╔═════════╦═════════╦═════════╗',
                '║ header1 ║ header2 ║ header3 ║',
                '╠═════════╬═════════╬═════════╣',
                '║ a       ║    b    ║       c ║',
                '╚═════════╩═════════╩═════════╝'
            ];
            const metadata = new TableMetadata()
                .setRowBorderPolicy(BorderPolicy.FIRST)
                .setColumnBorderPolicy(BorderPolicy.EVERY)
                .setHeaderOrientation(HeaderOrientation.FIRST_ROW)
                .setFormat(Format.UNICODE_BOLD);
            const tableModel = new TableModel(ctx.alignments, ctx.singleRowData);
            const encoder = new PlainTextEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenEveryRow_shouldPrintBordersEveryRow: (ctx) => {
            // Given
            const expected = [
                '+---------+---------+---------+',
                '| header1 | header2 | header3 |',
                '+---------+---------+---------+',
                '| a       |    b    |       c |',
                '+---------+---------+---------+',
                '| d       |    e    |       f |',
                '+---------+---------+---------+'
            ];
            const metadata = new TableMetadata()
                .setRowBorderPolicy(BorderPolicy.EVERY)
                .setColumnBorderPolicy(BorderPolicy.EVERY)
                .setHeaderOrientation(HeaderOrientation.FIRST_ROW)
                .setFormat(Format.PLAIN_ASCII);
            const tableModel = new TableModel(ctx.alignments, ctx.multiRowData);
            const encoder = new PlainTextEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenNoHeaderAndHeaderOnly_shouldNotPrintHeaderBorder: (ctx) => {
            // Given
            const expected = [
                '+---------+---------+---------+',
                '| header1 | header2 | header3 |',
                '| a       |    b    |       c |',
                '| d       |    e    |       f |',
                '+---------+---------+---------+'
            ];
            const metadata = new TableMetadata()
                .setRowBorderPolicy(BorderPolicy.OUTLINE)
                .setColumnBorderPolicy(BorderPolicy.EVERY)
                .setHeaderOrientation(HeaderOrientation.NO_HEADER)
                .setFormat(Format.PLAIN_ASCII);
            const tableModel = new TableModel(ctx.alignments, ctx.multiRowData);
            const encoder = new PlainTextEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenFirstRowAsHeaderAndHeaderOnly_shouldOnlyPrintHeaderBorder: (ctx) => {
            // Given
            const expected = [
                '+---------+---------+---------+',
                '| header1 | header2 | header3 |',
                '+---------+---------+---------+',
                '| a       |    b    |       c |',
                '| d       |    e    |       f |',
                '+---------+---------+---------+'
            ];
            const metadata = new TableMetadata()
                .setRowBorderPolicy(BorderPolicy.FIRST)
                .setColumnBorderPolicy(BorderPolicy.EVERY)
                .setHeaderOrientation(HeaderOrientation.FIRST_ROW)
                .setFormat(Format.PLAIN_ASCII);
            const tableModel = new TableModel(ctx.alignments, ctx.multiRowData);
            const encoder = new PlainTextEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenFirstColumnAsHeaderAndHeaderOnly_shouldOnlyPrintHeaderBorder: (ctx) => {
            // Given
            const expected = [
                '+---------+------------------+',
                '| Header1 | Column2  Column3 |',
                '| Header2 | b              c |',
                '| Header3 | e              f |',
                '+---------+------------------+'
            ];
            const data = [
                ['Header1', 'Column2', 'Column3'],
                ['Header2', 'b',             'c'],
                ['Header3', 'e',             'f']
            ];
            const alignments = [Alignment.LEFT, Alignment.LEFT, Alignment.RIGHT];
            const metadata = new TableMetadata()
                .setRowBorderPolicy(BorderPolicy.OUTLINE)
                .setColumnBorderPolicy(BorderPolicy.FIRST)
                .setHeaderOrientation(HeaderOrientation.FIRST_COLUMN)
                .setFormat(Format.PLAIN_ASCII);
            const tableModel = new TableModel(alignments, data);
            const encoder = new PlainTextEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenBordersAndNoHeader_shouldOnlyPrintOuterBorders: (ctx) => {
            // Given
            const expected = [
                '+---------+---------+---------+',
                '| header1 | header2 | header3 |',
                '| a       |    b    |       c |',
                '| d       |    e    |       f |',
                '+---------+---------+---------+'
            ];
            const metadata = new TableMetadata()
                .setRowBorderPolicy(BorderPolicy.OUTLINE)
                .setColumnBorderPolicy(BorderPolicy.EVERY)
                .setHeaderOrientation(HeaderOrientation.NO_HEADER)
                .setFormat(Format.PLAIN_ASCII);
            const tableModel = new TableModel(ctx.alignments, ctx.multiRowData);
            const encoder = new PlainTextEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenContainsHtmlCharacters_shouldProperlyRenderHtmlCharacters: (ctx) => {
            // Given
            const expected = [
                '+--------------+---------+--------------+',
                '| header1      | header2 |      header3 |',
                '+--------------+---------+--------------+',
                '| <div>a</div> |    b    |            c |',
                '| d            |    e    | <pre>f</pre> |',
                '+--------------+---------+--------------+'
            ]
            const data = [
                ['header1',      'header2', 'header3'     ],
                ['<div>a</div>', 'b',       'c'           ],
                ['d',            'e',       '<pre>f</pre>']
            ];
            const metadata = new TableMetadata()
                .setRowBorderPolicy(BorderPolicy.FIRST)
                .setColumnBorderPolicy(BorderPolicy.EVERY)
                .setHeaderOrientation(HeaderOrientation.FIRST_ROW)
                .setFormat(Format.PLAIN_ASCII);
            const tableModel = new TableModel(ctx.alignments, data);
            const encoder = new PlainTextEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenBorderless_shouldNotPrintAnyBorder: (ctx) => {
            // Given
            const expected = [
                'header1  header2  header3 ',
                'a           b           c ',
                'd           e           f ',
            ];
            const metadata = new TableMetadata()
                .setRowBorderPolicy(BorderPolicy.NONE)
                .setColumnBorderPolicy(BorderPolicy.NONE)
                .setHeaderOrientation(HeaderOrientation.FIRST_ROW)
                .setFormat(Format.PLAIN_ASCII);
            const tableModel = new TableModel(ctx.alignments, ctx.multiRowData);
            const encoder = new PlainTextEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenHeaderUppercaseAndCenter_shouldPrintUppercaseAndCenterHeaders: (ctx) => {
            // Given
            const expected = [
                '+------------------+----------------------+---------+---------+',
                '|     OPERATION    |       SHORTCUT       | CONTEXT | VERSION |',
                '+------------------+----------------------+---------+---------+',
                '| Undo             | CTRL + Z             | Global  | 1.2.0   |',
                '| Duplicate column | CTRL + SHIFT + RIGHT | Global  | 1.2.0   |',
                '| Close dialog     | ESCAPE               | Dialog  | 1.2.0   |',
                '+------------------+----------------------+---------+---------+'
            ];
            const input = [
                ['Operation',         'shortcut',              'ContEXT',  'veRSion'],
                ['Undo',              'CTRL + Z',              'Global',   '1.2.0'],
                ['Duplicate column',  'CTRL + SHIFT + RIGHT',  'Global',   '1.2.0'],
                ['Close dialog',      'ESCAPE',                'Dialog',   '1.2.0']
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.PLAIN_ASCII)
                .setRowBorderPolicy(BorderPolicy.FIRST)
                .setColumnBorderPolicy(BorderPolicy.EVERY)
                .setHeaderOrientation(HeaderOrientation.FIRST_ROW)
                .setHeaderCase(TextCase.UPPER)
                .setHeaderAlignment(Alignment.CENTER);
            const alignments = ArrayUtils.repeat(Alignment.LEFT, 4);
            const tableModel = new TableModel(alignments, input);
            const encoder = new PlainTextEncoder(metadata);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual);
        }
    });
