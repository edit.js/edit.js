/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('models.Vector')

    .run({
        toString_shouldReturnStringifiedVector: () => {
            // Given
            const x = 1;
            const y = 2;
            // When
            const vector = new Vector(x, y);
            // Then
            assertEquals('(' + x + ', ' + y + ')', vector.toString());
        },

        equals_whenEqual_shouldReturnTrue: () => {
            // Given
            const x = 1;
            const y = 2;
            const a = new Vector(x, y);
            const b = new Vector(x, y);
            // Then
            assertTrue(a.equals(b));
        },

        equals_whenNotEqual_shouldReturnFalse: () => {
            // Given
            const x = 1;
            const y = 2;
            const a = new Vector(x, y);
            const b = new Vector(x, y + 1);
            // Then
            assertFalse(a.equals(b));
        },

        add_shouldReturnSumOfSpecifiedVector: () => {
            // Given
            const x1 = 1;
            const y1 = 2;
            const x2 = 3;
            const y2 = 4;
            const a = new Vector(x1, y1);
            const b = new Vector(x2, y2);
            // When
            const actual = a.add(b);
            // Then
            assertEquals(x1 + x2, actual.getX());
            assertEquals(y1 + y2, actual.getY());
        },

        sub_shouldReturnDifferenceBetweenSpecifiedVector: () => {
            // Given
            const x1 = 1;
            const y1 = 2;
            const x2 = 3;
            const y2 = 4;
            const a = new Vector(x1, y1);
            const b = new Vector(x2, y2);
            // When
            const actual = a.sub(b);
            // Then
            assertEquals(x1 - x2, actual.getX());
            assertEquals(y1 - y2, actual.getY());
        },

        mult_shouldReturnProductOfMagnitudes: () => {
            // Given
            const x = 1;
            const y = 2;
            const vector = new Vector(x, y);
            const scalar = -3;
            // When
            const actual = vector.mult(scalar);
            // Then
            assertEquals(x * scalar, actual.getX());
            assertEquals(y * scalar, actual.getY());
        },

        div_shouldReturnQuotientOfMagnitudes: () => {
            // Given
            const x = 1;
            const y = 2;
            const vector = new Vector(x, y);
            const scalar = -3;
            // When
            const actual = vector.div(scalar);
            // Then
            assertEquals(x / scalar, actual.getX());
            assertEquals(y / scalar, actual.getY());
        },

        apply_shouldApplySpecifiedFunctionToEachVectorComponent: () => {
            // Given
            const x = 1;
            const y = 2;
            const vector = new Vector(x, y);
            // When
            const actual = vector.map((c) => Math.pow(c, 2));
            // Then
            assertEquals(Math.pow(x, 2), actual.getX());
            assertEquals(Math.pow(y, 2), actual.getY());
        }
    });
