/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const MetricsContainer = function() {

    Component.call(this);

    let entriesContainer;
    let pieContainer;
    let legendContainer;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.update = (metrics) => {
        displayEntries(metrics);
        displayPie(metrics);
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const displayEntries = (metrics) => {
        const defaultClass = 'metrics-value';
        entriesContainer.clearNodes();
        const count = metrics.getTotalExecutedCount();
        [
            count + ' ' + StringUtils.pluralize('test', count) + ' executed',
            'Total time: ' + metrics.getTotalTime() + 'ms',
            'Passed: ' + StyleUtils.colorizeSuccess(metrics.getTotalSuccessCount(), defaultClass),
            'Failed: ' + StyleUtils.colorizeError(metrics.getTotalErrorCount(), defaultClass),
            'Remaining: ' + StyleUtils.colorizeWarning(metrics.getTotalRemainingCount(), defaultClass)
        ].forEach(entry => entriesContainer.add(createEntry(entry)));
        const cssClass = StyleUtils.computeErrorStatus(metrics.getTotalErrorCount());
        this.toggleClass(Status.SUCCESS, (cssClass === Status.SUCCESS));
        this.toggleClass(Status.ERROR, (cssClass === Status.ERROR));
    }

    const displayPie = (metrics) => {
        const total = metrics.getTotalCount();
        const passed = metrics.getTotalSuccessCount() / total * 100;
        const failed = metrics.getTotalErrorCount() / total * 100;
        const remaining = metrics.getTotalRemainingCount() / total * 100;
        const data = [
            {name: 'Remain', color: '#ffd45e', value: remaining},
            {name: 'Failed', color: '#f07171', value: failed},
            {name: 'Passed', color: '#53c95c', value: passed}
        ];
        pieContainer.clearNodes();
        pieContainer.add(new Pie(data, 30));
        displayLegend(data);
    }

    const displayLegend = (data) => {
        legendContainer.clearNodes();
        data.forEach(slice => {
            legendContainer.add(createLegend(slice));
        });
    }

    const createLegend = (slice) => {
        const color = new Component()
            .addClass('legend-entry-color')
            .putStyle('background', slice.color);
        const name = new Component()
            .addClass('legend-entry-name')
            .setText(slice.name + ': ' + slice.value.toFixed(1) + '%')
        return new Component()
            .addClass('legend-entry')
            .add(color)
            .add(name);
    }

    const createEntry = (html) => {
        return new Component()
            .addClass('metrics-entry')
            .setHtml(html);
    }

    const createEntriesContainer = () => {
        return new Component()
            .addClass('metrics-entries');
    }

    const createPieContainer = () => {
        return new Component()
            .addClass('metrics-pie');
    }

    const createLegendContainer = () => {
        return new Component()
            .addClass('legend-container');
    }

    const init = () => {
        entriesContainer = createEntriesContainer();
        pieContainer = createPieContainer();
        legendContainer = createLegendContainer();
        const pieWrapper = new Component()
            .addClass('metrics-pie-container')
            .add(legendContainer)
            .add(pieContainer);
        this.setId('metrics')
            .add(entriesContainer)
            .add(pieWrapper);
    }

    init();

}
