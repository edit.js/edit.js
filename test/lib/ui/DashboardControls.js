/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const DashboardControls = function() {

    Component.call(this);

    const EXPAND_LABEL = 'Expand All';
    const FOLD_LABEL = 'Fold All';

    // UI
    let expandBtn;
    let filterInput;
    // Toggle buttons
    let filterAllBtn;
    let filterSuccessBtn;
    let filterErrorBtn;
    // State
    let expandedState = false;
    // Config
    let filterByStatusCallback = () => {}
    let filterByNameCallback = () => {}
    let expandCallback = () => {}

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.onFilterByStatus = (callback) => {
        filterByStatusCallback = callback;
        return this;
    }

    this.onFilterByName = (callback) => {
        filterByNameCallback = callback;
        return this;
    }

    this.onExpand = (callback) => {
        expandCallback = callback;
        return this;
    }

    this.setMetrics = (metrics) => {
        filterAllBtn.setText('All (' + metrics.getTotalExecutedCount() + ')');
        filterSuccessBtn.setText('Success (' + metrics.getTotalSuccessCount() + ')');
        filterErrorBtn.setText('Error (' + metrics.getTotalErrorCount() + ')');
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const toggle = () => {
        this.setExpanded(!expandedState);
    }

    this.setExpanded = (expand) => {
        expandedState = expand;
        expandBtn.setText(expand ? FOLD_LABEL : EXPAND_LABEL);
        expandCallback(expand);
    }

    const filterByName = () => {
        const searchString = StringUtils.escapeRegexCharacters(filterInput.getUI().value);
        const expand = (searchString !== '');
        if (expand !== expandedState) {
            expandBtn.getUI().click();
        }
        filterByNameCallback(searchString);
    }

    const createFilterButtons = () => {
        filterAllBtn = new ToggleButton('All', () => filterByStatusCallback(null));
        filterSuccessBtn = new ToggleButton('Success', () => filterByStatusCallback(true));
        filterErrorBtn = new ToggleButton('Error', () => filterByStatusCallback(false));
        filterAllBtn.setActive(true);
        new ButtonGroup(false, filterAllBtn, filterSuccessBtn, filterErrorBtn);
        return new Component()
            .addClass('filter-buttons')
            .add(filterAllBtn)
            .add(filterSuccessBtn)
            .add(filterErrorBtn);
    }

    const createFilterInput = () => {
        filterInput = new Component('input')
            .addClass('filter-input')
            .addListener('input', filterByName, false);
        filterInput.getUI().placeholder = 'Filter by test name...';
        return filterInput;
    }

    const init = () => {
        const label = expandedState ? FOLD_LABEL : EXPAND_LABEL;
        expandBtn = new Button(label, toggle, 'Expands/folds all items', ButtonStyle.FILLED, ButtonType.PRIMARY);
        const toolbar = new Component()
            .setId('dashboard-controls')
            .add(createFilterButtons())
            .add(createFilterInput())
            .add(expandBtn);
        this.setUI(toolbar.getUI());
    }

    init();

}
