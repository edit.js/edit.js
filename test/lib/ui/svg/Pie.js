/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const Pie = function(data, thickness, size) {

    Donut.call(this, thickness, size);

    // State
    let completionSoFar = 0;

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const plot = (data) => {
        data.forEach(slice => this.getSvg().appendChild(addSegment(slice)));
    }

    const addSegment = (slice) => {
        const ratio = this.computeRatio(slice.value);
        completionSoFar += ratio;
        const dashArray = this.computeDashArray(ratio);
        const dashOffset = this.computeDashOffset(completionSoFar);
        return this.createCircle(0, 0, this.getRadius(), slice.color, dashArray, dashOffset);
    }

    const createCenterCircle = () => {
        const circle = this.createCircle(0, 0, this.getRadius(), 'transparent', 0, 0);
        circle.setAttribute('fill', '#eee');
        circle.setAttribute('stroke', 'transparent');
        return circle;
    }

    const init = () => {
        plot(data);
        this.getSvg().appendChild(createCenterCircle());
    }

    init();

}
