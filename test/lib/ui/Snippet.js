/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A code snippet {@link Component} providing minimal, regex-based syntax highlighting.
 */
const Snippet = function() {

    Component.call(this, 'pre');

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.display = (code) => {
        const formatted = format(code);
        this.setHtml(highlight(formatted));
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const highlight = (code) => {
        const matches = [];
        let text = escape(code);
        let matchIndex = 0;

        find(Pattern.REGEX, Type.REGEX);
        find(Pattern.COMMENT_MULTILINE, Type.COMMENT);
        find(Pattern.COMMENT_INLINE, Type.COMMENT);
        find(Pattern.TEMPLATE, Type.STRING);
        find(Pattern.STRING, Type.STRING);
        find(/assert[a-zA-Z]+/, Type.LIB);

        // let highlighted = text.replaceAll(Pattern.LIB, StyleUtils.colorize('$&', Type.LIB));
        let highlighted = highlightKeywords(text);
        return restore(highlighted);

        function find(pattern, type) {
            let result;
            while (result = pattern.exec(text)) {
                const match = result[0];
                text = text.replace(match, quote(matchIndex))
                matches.push({match, type});
                matchIndex++;
            }
        }
        function restore(text) {
            matches.forEach((e, i) => {
                text = text.replace(quote(i), StyleUtils.colorize(e.match, e.type));
            });
            return text;
        }
        function quote(n) {
            const prefix = '$___';
            const suffix = '___$';
            return prefix + n + suffix;
        }
    }

    const escape = (code) => {
        return code
            .replaceAll(Character.DOUBLE_QUOTE, '"')
            .replaceAll(Character.SINGLE_QUOTE, "'");
    }

    const highlightKeywords = (input) => {
        let highlighted = input;
        for (let keyword of KEYWORDS) {
            const KEYWORD_PATTERN = '\\b' + keyword + '\\b';
            const keywordReg = new RegExp(KEYWORD_PATTERN, 'g');
            highlighted = highlighted.replaceAll(keywordReg, StyleUtils.colorize(keyword, Type.KEYWORD));
        }
        return highlighted;
    }

    const format = (code) => {
        const NEW_LINE = '\n';
        const lines = code.split(/\r?\n/);
        let firstIndex = 0;
        for (let line of lines) {
            if (!StringUtils.isBlank(line)) {
                break;
            }
            firstIndex++;
        }
        const safe = lines.slice(firstIndex);
        const n = safe[0].search(/\S/);
        const stripped = safe.map(line => line.slice(n));
        return stripped.join(NEW_LINE) + NEW_LINE;
    }

    const init = () => {
        this.addClass('snippet');
    }

    init();

    //////////////////////////////////////////////////////////////////
    // CONSTANTS
    //////////////////////////////////////////////////////////////////

    const Type = {
        LIB:     'lib',
        KEYWORD: 'keyword',
        COMMENT: 'comment',
        STRING:  'string',
        REGEX:   'regex'
    }

    const Character = {
        SINGLE_QUOTE: '&#039;',
        DOUBLE_QUOTE: '&quot;'
    }

    const Pattern = {
        COMMENT_INLINE:     /(\/\/.*)/,
        COMMENT_MULTILINE:  /(\/\*.*[\s\S]*\*\/)/,
        STRING:             /('[^']*'|"[^"]*")/m, // /'[^']*'|"[^"]*"/g
        TEMPLATE:           /(`[^`]*`)/m, // /'[^']*'|"[^"]*"/g
        LIB:                /\bassert[A-Z]\w+/g,
        // This REGEX pattern is based on Mike Samuel answer to this StackOverflow question:
        // https://stackoverflow.com/questions/17843691/javascript-regex-to-match-a-regex
        REGEX:              /(\/(?![*+?])(?:[^\r\n\[/\\]|\\.|\[(?:[^\r\n\]\\]|\\.)*])+\/(?:g(?:im?|mi?)?|i(?:gm?|mg?)?|m(?:gi?|ig?)?)?)/,
    };

    const SYNTAX = [
        '{', '}', '[', ']', '(', ')',
        '+', '-', '/', '*', '.',
        '>', '<', '=', '!',
        ':', '?',
        ',', ';'
    ];

    const KEYWORDS = [
        'await',
        'break',
        'case',
        'catch',
        'class',
        'const',
        'continue',
        'debugger',
        'default',
        'delete',
        'do',
        'else',
        'enum',
        'export',
        'extends',
        'false',
        'finally',
        'for',
        'function',
        'if',
        'implements',
        'import',
        'in',
        'instanceof',
        'interface',
        'let',
        'new',
        'null',
        'package',
        'private',
        'protected',
        'public',
        'return',
        'super',
        'switch',
        'static',
        'this',
        'throw',
        'try',
        'true',
        'typeof',
        'var',
        'void',
        'while',
        'with',
        'yield'
    ];

};
