/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const TestsSettings = {

    /**
     * If true, stops the tests execution when an assertion fails.
     * If false, the test runner will keep executing every test.
     * If gui === true, the passing and failing tests will be summarized
     * in the dashboard.
     */
    debug: false,

    /**
     * If true, generates an interactive dashboard which display information about
     * the unit tests in addition to the console logs.
     * If false, only the console logs will be used.
     */
    gui: true,

    /**
     * Defines the log level to be used by the test runner when running tests
     */
    logLevel: LogLevel.ERROR

};
