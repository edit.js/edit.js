/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * Duplicates the specified column in a {@link EditorTable}.
 *
 * @param {EditorTable} table  the target {@link EditorTable} for this operation
 * @param {number} index  the column index
 */
const DuplicateColumnCommand = function(table, index) {

    this.execute = () => {
        table.duplicateColumn(index);
    }

    this.undo = () => {
        table.removeColumn(index + 1);
    }

}
