/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const CommentType = Object.freeze({

    NONE:             {lineStart: '',      prefix: '',       suffix: '',     type: '',       displayName: 'None'},
    DOUBLE_DASH:      {lineStart: '--',    prefix: '',       suffix: '',     type: 'inline', displayName: 'SQL, Ada, Eiffel, Lua, Haskell, AppleScript...'},
    DOUBLE_SLASH:     {lineStart: '//',    prefix: '',       suffix: '',     type: 'inline', displayName: 'C, C++, C#, Go, Java, JavaScript, Kotlin, Objective-C, PHP, Rust, Scala, Swift, ActionScript, Delphi...'},
    TRIPLE_SLASH:     {lineStart: '///',   prefix: '',       suffix: '',     type: 'inline', displayName: 'C#'},
    HASH:             {lineStart: '#',     prefix: '',       suffix: '',     type: 'inline', displayName: 'Unix Shell, PowerShell, Python, Perl, R, Ruby, PHP...'},
    MODULO:           {lineStart: '%',     prefix: '',       suffix: '',     type: 'inline', displayName: 'MatLab, TeX, Erlang...'},
    SEMI_COLON:       {lineStart: ';',     prefix: '',       suffix: '',     type: 'inline', displayName: 'Assembly x86, Lisp, Clojure...'},
    EXCLAMATION_MARK: {lineStart: '!',     prefix: '',       suffix: '',     type: 'inline', displayName: 'Fortran, Basic Plus, Inform, Pick Basic'},
    SINGLE_QUOTE:     {lineStart: '\'',    prefix: '',       suffix: '',     type: 'inline', displayName: 'VBScript'},
    DOUBLE_COLUMN:    {lineStart: '::',    prefix: '',       suffix: '',     type: 'inline', displayName: 'Batch files, cmd.exe, COMMAND.COM'},
    REM:              {lineStart: 'REM',   prefix: '',       suffix: '',     type: 'inline', displayName: 'Basic, batch files'},
    JAVADOC:          {lineStart: ' *',    prefix: '/**',    suffix: ' */',  type: 'block',  displayName: 'Java, C#, C++, JavaScript, Rust, Swift...'},
    C_STYLE:          {lineStart: '',      prefix: '/*',     suffix: '*/',   type: 'block',  displayName: 'Java, JavaScript, PHP, CSS...'},
    XML_STYLE:        {lineStart: '',      prefix: '<!--',   suffix: '-->',  type: 'block',  displayName: 'XML, HTML'},
    MATLAB:           {lineStart: '',      prefix: '%{',     suffix: '%}',   type: 'block',  displayName: 'MatLab'},
    SMALLTALK:        {lineStart: '',      prefix: '"',      suffix: '"',    type: 'block',  displayName: 'SmallTalk'},
    DELPHI_STYLE:     {lineStart: '',      prefix: '(*',     suffix: '*)',   type: 'block',  displayName: 'Delphi, Mathematica, Pascal, AppleScript, OCaml'}, //
    PASCAL_STYLE:     {lineStart: '',      prefix: '{',      suffix: '}',    type: 'block',  displayName: 'Delphi, Pascal'},
    LISP_STYLE:       {lineStart: '',      prefix: '#|',     suffix: '|#',   type: 'block',  displayName: 'Lisp, Scheme, Racket'},
    RUBY:             {lineStart: '',      prefix: '=begin', suffix: '=end', type: 'block',  displayName: 'Ruby'}

});
