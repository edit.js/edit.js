/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const Format = Object.freeze({

  PLAIN_ASCII:          {type: FormatType.PLAIN_TEXT, displayName: 'ASCII'},
  UNICODE:              {type: FormatType.PLAIN_TEXT, displayName: 'Unicode - Line'},
  UNICODE_BOLD:         {type: FormatType.PLAIN_TEXT, displayName: 'Unicode - Bold'},
  GITHUB_MARKDOWN:      {type: FormatType.MARKDOWN,   displayName: 'Github / GitLab'},
  CONFLUENCE_MARKDOWN:  {type: FormatType.MARKDOWN,   displayName: 'Confluence'},
  CUCUMBER_MARKDOWN:    {type: FormatType.MARKDOWN,   displayName: 'Cucumber'},
  LATEX:                {type: FormatType.MARKUP,     displayName: 'LaTeX'},
  ASCII_DOC:            {type: FormatType.MARKUP,     displayName: 'AsciiDoc'},
  BBCODE:               {type: FormatType.WEB,        displayName: 'BBCode'},
  MEDIAWIKI:            {type: FormatType.WEB,        displayName: 'MediaWiki'},
  HTML:                 {type: FormatType.WEB,        displayName: 'HTML'},
  JSON:                 {type: FormatType.DATA,       displayName: 'JSON'},
  CSV:                  {type: FormatType.DATA,       displayName: 'CSV'},
  SQL:                  {type: FormatType.CODE,       displayName: 'SQL'},
  JAVA:                 {type: FormatType.CODE,       displayName: 'Java'},
  C_SHARP:              {type: FormatType.CODE,       displayName: 'C#'},
  PHP:                  {type: FormatType.CODE,       displayName: 'PHP'},
  JAVASCRIPT:           {type: FormatType.CODE,       displayName: 'JavaScript'},
  PYTHON:               {type: FormatType.CODE,       displayName: 'Python'}

});
