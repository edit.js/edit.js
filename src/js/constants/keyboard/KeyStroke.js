/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * Represents a key action on the keyboard.
 *
 * @param key  a {@link Key} constant
 * @param modifiers  an optional array of {@link Modifier} keys
 */
const KeyStroke = function(key, ...modifiers) {

    this.getKey = () => {
        return key;
    }

    this.getModifiers = () => {
        return modifiers;
    }

    this.toString = () => {
        const modifiersStr = (modifiers.length === 0) ? '' : modifiers.join(' + ').replace(/control/i, 'Ctrl') + ' + ';
        return modifiersStr + key.replace(/arrow/i, '').toUpperCase();
    }

}

