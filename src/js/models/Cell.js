/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * Represents a single row/column intersection in a table model.
 *
 * @param {*|null} value  an optional value
 * @param {Alignment} alignment  an optional {@link Alignment} constant
 * @see CellType
 */
const Cell = function(value = null, alignment = Alignment.LEFT) {

    let cellValue = value;
    let cellAlignment = alignment;
    let type;

    this.getValue = () => {
        return cellValue;
    }

    this.getAlignment = () => {
        return cellAlignment;
    }

    this.getType = () => {
        return type;
    }

    this.setValue = (value) => {
        cellValue = value;
    }

    this.setAlignment = (alignment) => {
        cellAlignment = alignment;
    }

    this.setType = (cellType) => {
        type = cellType;
    }

    this.isEmpty = () => {
        return (cellValue == null);
    }

    this.toString = () => {
        return '[Cell] ' + cellAlignment.displayName + ', ' + cellValue;
    }

}
