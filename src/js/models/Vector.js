/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A vector represents quantities which can not be expressed by a single number,
 * such as a direction, a dimension, a point in a two-dimensional space, etc.
 * Instances of this class are typically used to identify or move elements in a
 * {@link Matrix}.
 *
 * @param {number} x  the x component of this vector
 * @param {number} y  the y component of this vector
 */
const Vector = function(x, y) {

    this.getX = () => {
        return x;
    };

    this.getY = () => {
        return y;
    };

    this.toString = () => {
        return '(' + x + ', ' + y + ')';
    };

    this.equals = (vector) => {
        return (
            x === vector.getX() &&
            y === vector.getY()
        );
    };

    /**
     * Subtracts the two vectors without mutating the original.
     *
     * @param vector  the vector to subtract
     * @return the difference between the two vectors as a new {@link Vector}
     */
    this.sub = (vector) => {
        return new Vector(
            x - vector.getX(),
            y - vector.getY()
        );
    };

    /**
     * Adds the two vectors without mutating the original.
     *
     * @param vector  the vector to add
     * @return the sum of the two vectors as a new {@link Vector}
     */
    this.add = (vector) => {
        return new Vector(
            x + vector.getX(),
            y + vector.getY()
        );
    };

    /**
     * Multiplies each component of this vector (seen as a vector) by a scalar.
     * This method returns a new instance without mutating the vector.
     *
     * @param scalar  the scalar to multiply the vector with
     * @return a new {@link Vector}
     */
    this.mult = (scalar) => {
        return new Vector(x * scalar, y * scalar);
    };

    /**
     * Divides each component of this vector (seen as a vector) by a scalar.
     * This method returns a new instance without mutating the vector.
     *
     * @param scalar  the scalar to divide the vector with
     * @return a new {@link Vector}
     */
    this.div = (scalar) => {
        return new Vector(x / scalar, y / scalar);
    };

    /**
     * Returns a new vector where each member is the absolute value of the
     * corresponding member in this object.
     * For instance, the following invocation:
     * <code>new Vector(-1, 2).abs()</code>
     * Will return <code>(1, 2)</code>
     *
     * @return a new {@link Vector}
     */
    this.abs = () => {
        return new Vector(Math.abs(x), Math.abs(y));
    };

    /**
     * Creates a new vector populated with the results of calling the specified
     * function on every component of this vector.
     *
     * @param fn  the function to apply
     * @return a new {@link Vector}
     */
    this.map = (fn) => {
        return new Vector(fn(x), fn(y));
    }

}
