/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const TableMetadata = function() {

    let format;
    let headerCase;
    let headerAlignment;
    let headerOrientation;
    let outputPolicy;
    let rowBorderPolicy;
    let columnBorderPolicy;
    let commentType;
    let jsonFormat;
    let separator;
    let javaFormat;
    let cSharpFormat;
    let phpFormat;
    let pythonFormat;
    let emptyCellPlaceholder;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.toString = () => {
        const divider = StringUtils.repeat('-', 5) + '\n';
        return '\n'+
               divider +
               'METADATA:\n' +
               '  format               = ' + printProperty(format)              + '\n' +
               '  headerCase           = ' + printProperty(headerCase)          + '\n' +
               '  headerAlignment      = ' + printProperty(headerAlignment)     + '\n' +
               '  headerOrientation    = ' + printProperty(headerOrientation)   + '\n' +
               '  outputPolicy         = ' + printProperty(outputPolicy)        + '\n' +
               '  rowBorderPolicy      = ' + printProperty(rowBorderPolicy)     + '\n' +
               '  columnBorderPolicy   = ' + printProperty(columnBorderPolicy)  + '\n' +
               '  commentType          = ' + printProperty(commentType)         + '\n' +
               '  jsonFormat           = ' + printProperty(jsonFormat)          + '\n' +
               '  javaFormat           = ' + printProperty(javaFormat)          + '\n' +
               '  cSharpFormat         = ' + printProperty(cSharpFormat)        + '\n' +
               '  phpFormat            = ' + printProperty(phpFormat)           + '\n' +
               '  pythonFormat         = ' + printProperty(pythonFormat)        + '\n' +
               '  separator            = ' + printProperty(separator)           + '\n' +
               '  emptyCellPlaceholder = ' + printProperty(emptyCellPlaceholder)+ '\n' +
               divider + '\n';
    }

    this.hasFormat = (f) => {
        return (f === format);
    }

    //////////////////////////////////////////////////////////////////
    // Getters
    //////////////////////////////////////////////////////////////////

    this.getFormat = () => {
        return format;
    };

    this.getHeaderOrientation = () => {
        return headerOrientation;
    };

    this.getHeaderCase = () => {
        return headerCase;
    };

    this.getHeaderAlignment = () => {
        return headerAlignment;
    };

    this.getOutputPolicy = () => {
        return outputPolicy;
    };

    this.getRowBorderPolicy = () => {
        return rowBorderPolicy;
    };

    this.getColumnBorderPolicy = () => {
        return columnBorderPolicy;
    };

    this.getJsonFormat = () => {
        return jsonFormat;
    };

    this.getSeparator = () => {
        return separator;
    };

    this.getJavaFormat = () => {
        return javaFormat;
    };

    this.getCSharpFormat = () => {
        return cSharpFormat;
    };

    this.getPhpFormat = () => {
        return phpFormat;
    }
    this.getPythonFormat = () => {
        return pythonFormat;
    }

    this.getEmptyCellPlaceholder = () => {
        return emptyCellPlaceholder;
    };

    this.getCommentType = () => {
        return commentType;
    };

    //////////////////////////////////////////////////////////////////
    // Fluent setters
    //////////////////////////////////////////////////////////////////

    this.setFormat = (f) => {
        format = f;
        return this;
    };

    this.setHeaderOrientation = (orientation) => {
        headerOrientation = orientation;
        return this;
    };

    this.setHeaderCase = (textCase) => {
        headerCase = textCase;
        return this;
    };

    this.setHeaderAlignment = (alignment) => {
        headerAlignment = alignment;
        return this;
    };

    this.setRowBorderPolicy = (policy) => {
        rowBorderPolicy = policy;
        return this;
    };

    this.setColumnBorderPolicy = (policy) => {
        columnBorderPolicy = policy;
        return this;
    };

    this.setOutputPolicy = (policy) => {
        outputPolicy = policy;
        return this;
    };

    this.setSeparator = (str) => {
        separator = str;
        return this;
    };

    this.setJavaFormat = (type) => {
        javaFormat = type;
        return this;
    };

    this.setCSharpFormat = (type) => {
        cSharpFormat = type;
        return this;
    };

    this.setPhpFormat = (type) => {
        phpFormat = type;
        return this;
    };

    this.setPythonFormat = (type) => {
        pythonFormat = type;
        return this;
    };

    this.setEmptyCellPlaceholder = (str) => {
        emptyCellPlaceholder = str;
        return this;
    };

    this.setJsonFormat = (format) => {
        jsonFormat = format;
        return this;
    };

    this.setCommentType = (f) => {
        commentType = f;
        return this;
    };

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const printProperty = (property) => {
        return StringUtils.prettyPrint(property).toUpperCase();
    }

};
