/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const Rectangle = function(x, y, width, height) {

    this.getX = () => { return x; }

    this.getY = () => { return y; }

    this.getWidth = () => { return width; }

    this.getHeight = () => { return height; }

    this.getCoordinate = () => { return new Vector(x, y); }

    this.getDimension = () => { return new Vector(width, height); }

    this.getEndCoordinate = () => { return this.getCoordinate().add(this.getDimension()); }

    this.containsCoordinate = (coordinate) => {
        const endCoordinate = this.getEndCoordinate();
        return (
            Values.between(coordinate.getX(), x, endCoordinate.getX() - 1) &&
            Values.between(coordinate.getY(), y, endCoordinate.getY() - 1)
        );
    }

    this.containsRectangle = (rectangle) => {
        return (
            this.containsCoordinate(rectangle.getCoordinate()) &&
            this.containsCoordinate(rectangle.getEndCoordinate().sub(new Vector(1, 1)))
        );
    }

    this.equals = (rectangle) => {
        return (
            rectangle != null &&
            this.getCoordinate().equals(rectangle.getCoordinate()) &&
            this.getDimension().equals(rectangle.getDimension())
        );
    }

    this.toString = () => {
        return '[Rect] x=' + x + ', y=' + y + ', w=' + width + ', h=' + height;
    }

}
