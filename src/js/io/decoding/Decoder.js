/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * Encoders provide methods parsing generating a string representation
 * of a 2d data structure as a {@link TableModel}.
 *
 * @see TableModel
 * @see Format
 */
const Decoder = function() {

    /**
     * Parses this input string as a {@link TableModel}.
     * This virtual method must be implemented by subtypes.
     *
     * @param {string} input
     * @return {ParserResult}
     */
    this.decode = (input) => { throw new Error('Not implemented yet'); }

    /**
     * Returns the {@link Format} constant matching the specified input string if this encoder
     * supports it; null otherwise.
     * This virtual method must be implemented by subtypes.
     *
     * @param {string} input  the input string to process
     * @return {Format|null}
     */
    this.supports = (input) => { throw new Error('Not implemented yet'); };

}
