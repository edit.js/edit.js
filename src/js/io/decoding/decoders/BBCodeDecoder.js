/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Decoder} provides support for BBCode tables
 *
 * @see Format.BBCODE
 */
const BBCodeDecoder = function() {

    Decoder.call(this);

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.decode = (input) => {
        const rows = Decoders.strip(input);
        const minified = (rows.length === 1);
        const preparsed = minified ? parseMinified(rows) : parsePrettified(rows);
        const data = extractData(preparsed);
        const model = new TableModel([], data);
        const orientation = detectHeaderOrientation(preparsed);
        const metadata = new TableMetadata()
            .setHeaderOrientation(orientation)
            .setHeaderCase(detectHeaderCase(data, orientation))
            .setOutputPolicy(minified ? OutputPolicy.MINIFY : OutputPolicy.PRETTIFY);
        return new ParserResult(model, metadata);
    };

    /**
     * @inheritDoc
     * @override
     */
    this.supports = (input) => {
        const reg = /^\[table]/;
        const rows = Decoders.strip(input);
        return rows[0].match(reg) ? Format.BBCODE : null;
    };

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const detectHeaderCase = (data, orientation) => {
        if (orientation !== HeaderOrientation.FIRST_ROW) {
            return null;
        }
        return Decoders.detectHeaderCase(data, orientation, false);
    }

    const detectHeaderOrientation = (preparsed) => {
        return (preparsed[0].match(/\[th]/)) ? HeaderOrientation.FIRST_ROW : HeaderOrientation.NO_HEADER;
    }

    const parsePrettified = (rows) => {
        return parseMinified([rows.join('')]);
    }

    const parseMinified = (rows) => {
        return preparse(rows);
    }

    const preparse = (rows) => {
        const reg = /\[tr]|\[\/tr]/;
        const subject = rows[0];
        let tmp = subject.split(reg);
        tmp = Decoders.removeNoise(tmp);
        return tmp.slice(1, tmp.length - 1);
    }

    const extractData = (preparsed) => {
        const reg = /\[td]|\[th]|\[\/td]\[td]|\[\/th]\[th]|\[\/td]$|\[\/th]$/;
        let tmp = preparsed.map(rowStr => rowStr.split(reg));
        return Decoders.trimMatrix(tmp);
    }

};
