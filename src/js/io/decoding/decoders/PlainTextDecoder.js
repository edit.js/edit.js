/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Decoder} provides support for ASCII and UNICODE tables.
 *
 * @param format  a {@link Format} constant
 * @see Format.PLAIN_ASCII
 * @see Format.UNICODE
 * @see Format.UNICODE_BOLD
 */
const PlainTextDecoder = function(format) {

    Decoder.call(this);

    const SUPPORTED_FORMATS = [Format.PLAIN_ASCII, Format.UNICODE, Format.UNICODE_BOLD];
    const REPLACE = '\|';
    const borders = BorderFactory.of(format);

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.decode = (input) => {
        const preparsed = preparse(input);
        const postparsed = postparse(preparsed);
        const compositeList = detectCompositeState(postparsed);
        const nonComposite = findNonComposite(postparsed, compositeList);
        const matrix = new Matrix().of(nonComposite);
        combine(matrix, postparsed, compositeList);
        const parsedWithFormatting = matrix.toArray();
        const data = extractData(parsedWithFormatting);
        const alignments = this.detectAlignments(parsedWithFormatting);
        const tableModel = new TableModel(alignments, data);
        const orientation = detectHeaderOrientation(preparsed, data);
        const metadata = new TableMetadata()
            .setHeaderOrientation(orientation)
            .setRowBorderPolicy(detectRowBorderPolicy(preparsed))
            .setColumnBorderPolicy(detectColumnBorderPolicy(preparsed))
            .setHeaderAlignment(detectHeaderAlignment(parsedWithFormatting, orientation))
            .setHeaderCase(detectHeaderCase(data, orientation));
        return new ParserResult(tableModel, metadata);
    };

    /**
     * @inheritDoc
     * @override
     */
    this.supports = (input) => {
        const rows = Decoders.strip(input);
        const firstCharacter = rows[0][0];
        for (let format of SUPPORTED_FORMATS) {
            let border = BorderFactory.of(format);
            if (firstCharacter === border.getTopLeft()) {
                return format;
            }
        }
        // Note that this method does not currently support detection for borderless tables
        return null;
    };

    this.isDivider = (row) => {
        const c = row.trim().charAt(0);
        const border = [borders.getTopLeft(), borders.getMiddleLeft(), borders.getBottomLeft()];
        return ArrayUtils.contains(border, c);
    }

    /**
     * Detects the columns alignment in the specified matrix of strings.
     *
     * Implementation note:
     * There is no point to differentiate between:
     * - CENTER and RIGHT alignment
     *   when smallestElement.length === longestElement.length - 1
     * - LEFT, CENTER, and RIGHT alignment
     *   when smallestElement.length === longestElement.length
     *
     * @param formattedData the 2d array of strings to process
     * @return an array of {@link Alignment} constants
     */
    this.detectAlignments = (formattedData) => {
        const width = formattedData[0].length;
        const alignments = [];
        for (let x = 0; x < width; x++) {
            const column = MatrixUtils.getColumn(formattedData, x);
            const alignment = detectColumnAlignment(column);
            alignments.push(alignment);
        }
        return alignments;
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const detectColumnAlignment = (column) => {
        const alignments = column.map(cell => detectCellAlignment(cell));
        const nonLeft = alignments.filter(a => a !== Alignment.LEFT);
        return (nonLeft.length === 0) ? Alignment.LEFT : ArrayUtils.mostFrequent(nonLeft);
    }

    const detectCellAlignment = (cell) => {
        if (cell.trim().length === cell.length) {
            return Alignment.LEFT;
        }
        const spacesBefore = StringUtils.countSpacesBefore(cell);
        if (spacesBefore === 0) {
            return Alignment.LEFT;
        }
        const spacesAfter = StringUtils.countSpacesAfter(cell);
        if (spacesAfter === 0) {
            return Alignment.RIGHT;
        }
        const diff = spacesBefore - spacesAfter;
        if (Values.between(diff, -1, 1)) {
            return Alignment.CENTER;
        } else if (spacesBefore > spacesAfter) {
            return Alignment.RIGHT;
        }
        return Alignment.LEFT;
    }

    const detectRowBorderPolicy = (preparsed) => {
        let policy = BorderPolicy.NONE;
        if (isBorderless(preparsed)) {
            return policy;
        }
        if (this.isDivider(preparsed[2])) {
            policy = BorderPolicy.FIRST;
            if (preparsed.length > 3 && this.isDivider(preparsed[4])) {
                policy = BorderPolicy.EVERY;
            }
            return policy;
        }
        return BorderPolicy.OUTLINE;
    }

    const detectColumnBorderPolicy = (preparsed) => {
        let policy = BorderPolicy.NONE;
        if (isBorderless(preparsed)) {
            return policy;
        }
        if (countVerticalBorders(preparsed) >= 3) {
            policy = BorderPolicy.FIRST;
            if (countVerticalBorders(preparsed) >= 5) {
                policy = BorderPolicy.EVERY;
            }
            return policy;
        }
        return BorderPolicy.OUTLINE;
    }

    const detectHeaderOrientation = (preparsed, data) => {
        if (isFirstRowHeader(preparsed)) {
            return HeaderOrientation.FIRST_ROW;
        }
        if (isFirstColumnHeader(preparsed)) {
            return HeaderOrientation.FIRST_COLUMN;
        }
        // At this point there is no hard evidence the table has headers;
        // we'll try and guess the table orientation using the first row and first column text case.
        // If the results are inconclusive, we'll just return a NO_HEADER constant
        if (couldFirstRowBeHeader(data)) {
            return HeaderOrientation.FIRST_ROW;
        }
        if (couldFirstColumnBeHeader(data)) {
            return HeaderOrientation.FIRST_COLUMN;
        }
        return HeaderOrientation.NO_HEADER;
    }

    const detectHeaderAlignment = (parsedWithFormatting, orientation) => {
        switch (orientation) {
            case HeaderOrientation.FIRST_ROW:
                let horizontalHeader = parsedWithFormatting[0];
                return detectColumnAlignment(horizontalHeader);
            case HeaderOrientation.FIRST_COLUMN:
                const verticalHeader = MatrixUtils.getColumn(parsedWithFormatting, 0);
                return detectColumnAlignment(verticalHeader);
            default:
                return null;
        }
    }

    const detectHeaderCase = (data, orientation) => {
        return Decoders.detectHeaderCase(data, orientation);
    }

    const couldFirstRowBeHeader = (data) => {
        return Decoders.detectHeaderCase(data, HeaderOrientation.FIRST_ROW, true) !== null;
    }

    const couldFirstColumnBeHeader = (data) => {
        return Decoders.detectHeaderCase(data, HeaderOrientation.FIRST_COLUMN, true) !== null;
    }

    const isFirstRowHeader = (preparsed) => {
        return countHorizontalBorders(preparsed) === 3;
    }

    const isFirstColumnHeader = (preparsed) => {
        return countVerticalBorders(preparsed) === 3;
    }

    const countHorizontalBorders = (preparsed) => {
        return fetchDividers(preparsed).length;
    }

    const countVerticalBorders = (preparsed) => {
        const sums = removeDividers(preparsed)
            .map(row => StringUtils.countOccurrences(row, StringUtils.escapeRegexCharacters(borders.getRight())));
        return ArrayUtils.mostFrequent(sums);
    }

    const removeDividers = (arr) => {
        return arr.filter(row => !this.isDivider(row));
    }

    const fetchDividers = (arr) => {
        return arr.filter(row => this.isDivider(row));
    }

    const preparse = (rawInput) => {
        let tmp = rawInput.split('\n');
        tmp = Decoders.removeBlankCells(tmp, true);
        if (!isBorderless(tmp)) {
            tmp = Decoders.trimArray(tmp);
        }
        return tmp;
    }

    const postparse = (preparsed) => {
        let tmp = [...preparsed];
        const separator = ' ?' + StringUtils.escapeRegexCharacters(borders.getRight()) + ' ?';
        const reg = new RegExp(separator);
        tmp = removeDividers(tmp)
        tmp = tmp.map(row => Decoders.extractCellContent(row, reg, false));
        tmp = tmp.map(row => Decoders.removeBlankCells(row));
        tmp = square(tmp);
        return trimArray(tmp);
    }

    const square = (arr) => {
        let tmp = MatrixUtils.square(arr);
        tmp = MatrixUtils.nullSafe(tmp);
        tmp = tmp.map(row => row.map((cell, x) => {
            let fixed = cell;
            if (fixed === '') {
                const column = MatrixUtils.getColumn(tmp, x);
                const length = MatrixUtils.getLongestElement(column).length;
                fixed = StringUtils.repeat(' ', length);
            }
            return fixed;
        }));
        return tmp;
    }

    const isBorderless = (data) => {
        return !this.isDivider(data[0]);
    }

    const extractData = (formattedData) => {
        return formattedData.map(row => Decoders.trimArray(row));
    }

    const combine = (matrix, preparsed, compositeList) => {
        if (compositeList.some(composite => composite)) {
            const colIndices = ArrayUtils.indicesOf(compositeList, true);
            const columns = colIndices.map(index => MatrixUtils.getColumn(preparsed, index));
            const occurrences = findRowCutIndices(columns);
            const result = cutCompositeCells(columns, occurrences);
            new Matrix().of(result)
                .forEachColumn(column => matrix.insertColumn(-1, column));
        }
    }

    const findNonComposite = (preparsed, compositeList) => {
        const r = [];
        preparsed.forEach(row => {
            const tmpRow = [];
            row.forEach((cell, x) => {
                if (!compositeList[x]) {
                    tmpRow.push(cell);
                }
            });
            r.push(tmpRow);
        });
        return r;
    }

    /** Cuts every row using specified list of occurrences */
    const cutCompositeCells = (columns, occurrences) => {
        const result = [];
        columns[0].forEach(cell => {
            const indices = occurrences.map(o => o.getOffset());
            result.push(StringUtils.cut(cell, indices, 2));
        });
        return result;
    }

    /** Finds where to cut each row to extract individual cells */
    const findRowCutIndices = (columns) => {
        const gaps = [];
        const columnsClone = clone(columns);
        columnsClone.forEach((col, x) => {
            let newRow = false;
            for (let y = 0; y < col.length; y++) {
                let cellCursor = 0;
                let found = new Occurrence('', -1);
                while (found != null) {
                    found = findNext(gaps, columnsClone, x, y, cellCursor, newRow);
                    if (found == null) {
                        newRow = true;
                        break;
                    }
                    if (cellCursor > gaps.length - 1) {
                        gaps.push(found);
                    } else {
                        const max = gaps[cellCursor];
                        if (found.getOffset() > max.getOffset()) {
                            gaps[cellCursor] = found;
                        }
                    }
                    newRow = false;
                    cellCursor++;
                }
            }
        });
        return gaps;
    }

    const detectCompositeState = (preparsed) => {
        const compositeList = ArrayUtils.repeat(false, preparsed[0].length);
        preparsed.forEach(row => row.forEach((cell, x) => {
            if (!compositeList[x]) {
                compositeList[x] = isComposite(cell);
            }
        }));
        return compositeList;
    }

    const findNext = (gaps, columns, x, y, cursor, newRow) => {
        const column = columns[x];
        const i = Values.restrict(cursor - 1, 0, gaps.length - 1);
        const previous = (gaps.length === 0) ? null : gaps[i];
        const replaceIndex = newRow ? y - 1 : y;
        const replaced = replace(column[replaceIndex], previous);
        if (replaced != null) {
            column[replaceIndex] = replaced;
        }
        const match = column[y].match(/ {2,}[^ |]/);
        if (match == null) {
            return null;
        }
        return new Occurrence(match[0], match.index);
    }

    const replace = (subject, occurrence) => {
        if (occurrence == null || subject == null) {
            return subject;
        }
        const offset = occurrence.getOffset();
        const before = subject.substring(0, offset);
        const toReplace = subject.substring(offset, occurrence.getEndIndex());
        const replaced = toReplace.replace(/ {2,}/, match => {
            return StringUtils.repeat(REPLACE, match.length);
        });
        const after = subject.substring(occurrence.getEndIndex());
        return before + replaced + after;
    }

    const isComposite = (cell) => {
        const match = cell.match(/[^ |] {2,}[^ |]/);
        return (match !== null);
    }

    const clone = (arr) => {
        return arr.map(row => row.map(cell => cell));
    }

    const trimArray = (data) => {
        const columns = MatrixUtils.rotate(data);
        const indices = [];
        columns.forEach(row => {
            let minFirst = 10000;
            let maxLast = 0;
            row.forEach(cell => {
                const first = StringUtils.indexOfFirstNonWhitespaceCharacter(cell);
                const last = StringUtils.indexOfFirstNonWhitespaceCharacter(cell, true);
                if (Values.between(first, 0, minFirst)) {
                    minFirst = first;
                }
                if (last > maxLast) {
                    maxLast = last;
                }
            });
            indices.push({first: minFirst, last: maxLast});
        });
        const result = data.map(row =>
            row.map((cell, x) => {
                const o = indices[x];
                return cell.substring(o.first, o.last);
            })
        );
        return result;
    }

}
