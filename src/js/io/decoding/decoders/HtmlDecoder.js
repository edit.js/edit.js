/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Decoder} provides support for HTML tables.
 *
 * @see Format.HTML
 */
const HtmlDecoder = function() {

    Decoder.call(this);

    const REG = /<table>(.*[\s\S]*)<\/table>/;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.decode = (input) => {
        const stripped = Decoders.strip(input);
        const joined = stripped.join('');
        const match = joined.match(REG);
        const htmlTableContent = match[1];
        const htmlTable = document.createElement('TABLE');
        htmlTable.innerHTML = htmlTableContent;
        const data = extractData(htmlTable);
        const alignments = extractAlignments(htmlTable);
        const tableModel = new TableModel(alignments, data);
        const metadata = detectMetadata(input, data, htmlTable);
        return new ParserResult(tableModel, metadata);
    };

    /**
     * @inheritDoc
     * @override
     */
    this.supports = (input) => {
        const rows = Decoders.strip(input);
        const firstCharacter = rows[0][0];
        return (firstCharacter === '<') ? Format.HTML : null;
    };

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const detectMetadata = (input, data, table) => {
        const orientation = detectHeaderOrientation(table);
        return new TableMetadata()
            .setHeaderOrientation(orientation)
            .setHeaderAlignment(detectHeaderAlignment(table, orientation))
            .setHeaderCase(detectHeaderCase(data, orientation))
            .setOutputPolicy(detectOutputPolicy(input));
    };

    const detectHeaderCase = (data, orientation) => {
        return Decoders.detectHeaderCase(data, orientation);
    }

    const detectHeaderAlignment = (table, orientation) => {
        const alignments = [];
        switch (orientation) {
            case HeaderOrientation.FIRST_ROW:
                for (let cell of table.rows[0].cells) {
                    alignments.push(readAlignment(cell));
                }
                break;
            case HeaderOrientation.FIRST_COLUMN:
                for (let row of table.rows) {
                    alignments.push(readAlignment(row.cells[0]));
                }
                break;
            default:
                return null;
        }
        return ArrayUtils.mostFrequent(alignments);
    }

    const detectOutputPolicy = (input) => {
        const stripped = Decoders.strip(input);
        return (stripped.length > 1) ? OutputPolicy.PRETTIFY : OutputPolicy.MINIFY;
    }

    const detectHeaderOrientation = (table) => {
        if (table.tHead != null) {
            return HeaderOrientation.FIRST_ROW;
        }
        for (let row of table.rows) {
            const cell = row.cells[0];
            if (cell.tagName.toUpperCase() === 'TH') {
                return HeaderOrientation.FIRST_COLUMN;
            }
        }
        return HeaderOrientation.NO_HEADER;
    };

    const extractAlignments = (table) => {
        const columnCount = table.rows[0].cells.length;
        const alignments = ArrayUtils.repeat(null, columnCount);
        for (let row of table.rows) {
            for (let x = 0; x < row.cells.length; x++) {
                alignments[x] = readAlignment(row.cells[x]);
            }
        }
        return alignments;
    }

    const extractData = (table) => {
        const data = [];
        for (let row of table.rows) {
            const rowData = [];
            for (let cell of row.cells) {
                rowData.push(cell.innerHTML);
            }
            data.push(rowData);
        }
        return data;
    }

    const readAlignment = (cellNode) => {
        const rawAlignment = cellNode.style.textAlign;
        return StringUtils.isBlank(rawAlignment) ? Alignment.LEFT : Enums.lookup(Alignment, rawAlignment);
    }

};
