/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Decoder} provides support for MediaWiki tables.
 *
 * @see Format.MEDIAWIKI
 */
const MediaWikiDecoder = function() {

    Decoder.call(this);

    const TABLE_PATTERN = /{\|\s*class="[^"]*"\s*(?:\|-)?\s*([\s\S]*?(?=\|}))\|}/;
    const ROW_PATTERN = /\s*\|-\s*(?:style="[^"]*"\s*)?/;
    const CELL_PATTERN = /(?:\s*(?:(?:\|\|)|(?:!!))\s*)\s*|\n\s*[|!]\s*/;
    const ATTRIBUTE_PATTERN = /\|\s*/;
    const ALIGNMENT_PATTERN = /text-align:(\w+);/;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.decode = (input) => {
        const table = input.match(TABLE_PATTERN)[1];
        const rowStrings = table.split(ROW_PATTERN)
        let alignments;
        const data = [];
        for (let rowString of rowStrings) {
            const row = [];
            let cellStrings = rowString.split(CELL_PATTERN)
            cellStrings = removeFirstPipe(cellStrings);
            if (alignments === undefined) {
                alignments = new Array(cellStrings.length).fill(Alignment.LEFT);
            }
            cellStrings.forEach((cellString, i) => {
                if (cellString.match(ATTRIBUTE_PATTERN)) {
                    const cellSplit = cellString.split(ATTRIBUTE_PATTERN);
                    const cellStyle = cellSplit[0];
                    const cell = cellSplit[1];
                    row.push(cell);
                    const alignmentMatch = cellStyle.match(ALIGNMENT_PATTERN);
                    if (alignmentMatch != null) {
                        alignments[i] = Enums.lookup(Alignment, alignmentMatch[1]);
                    }
                } else {
                    row.push(cellString);
                }
            });
            data.push(row);
        }

        removeTrailingNewLine(data);

        const tableModel = new TableModel(alignments, data);
        const orientation = detectHeaderOrientation(rowStrings);
        const metadata = new TableMetadata()
            .setHeaderCase(Decoders.detectHeaderCase(data, orientation))
            .setOutputPolicy(detectOutputPolicy(rowStrings))
            .setHeaderOrientation(orientation);
        return new ParserResult(tableModel, metadata);
    };

    /**
     * @inheritDoc
     * @override
     */
    this.supports = (input) => {
        return input.match(TABLE_PATTERN) ? Format.MEDIAWIKI : null;
    };

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const detectOutputPolicy = (rows) => {
        return (rows.join(',').match(/\|\|/) != null) ?
            OutputPolicy.MINIFY : OutputPolicy.PRETTIFY;
    };

    const detectHeaderOrientation = (rows) => {
        let a = rows[0][0] === '!';
        let b = rows[1][0] === '!';
        if (!a && !b) {
            return HeaderOrientation.NO_HEADER;
        }
        return b ? HeaderOrientation.FIRST_COLUMN : HeaderOrientation.FIRST_ROW;
    };

    // remove the pipe in first cell of the specified row
    const removeFirstPipe = (arr) => {
        const split = arr[0].split(/[|!]\s*/);
        split.splice(0, 1);
        arr[0] = split.join('| ');
        return arr;
    }

    // remove the trailing new line in the last cell of the last row of a matrix of strings
    const removeTrailingNewLine = (arr) => {
        const y = arr.length - 1;
        const x = arr[y].length - 1;
        const reg = /\n+\s*/;
        arr[y][x] = arr[y][x].replace(reg, '');
    }

};
