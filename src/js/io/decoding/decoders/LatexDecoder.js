/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Decoder} provides support for LaTeX tables.
 *
 * @param {string} placeholder  the string to use as a placeholder for empty cells
 * @see Format.LATEX
 */
const LatexDecoder = function(placeholder) {

    Decoder.call(this);

    const REG = /\\begin{tabular}([\S\s]*?)\\end{tabular}/;
    const VERTICAL_BORDERS_DEFINITION = /{(\|?(?:l\|?)+)}/;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.decode = (input) => {
        const preparsed = preparse(input);
        const data = extractData(preparsed);
        const tableModel = new TableModel([], data);
        const metadata = detectMetadata(preparsed);
        return new ParserResult(tableModel, metadata);
    };

    /**
     * @inheritDoc
     * @override
     */
    this.supports = (input) => {
        const match = input.match(REG);
        if (match != null) {
            const group = match[1];
            return (group != null) ? Format.LATEX : null;
        }
        return null;
    };

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const preparse = (subject) => {
        const match = subject.match(REG);
        const group = match[1];
        return Decoders.strip(group);
    }

    const detectMetadata = (preparsed) => {
        return new TableMetadata()
            .setRowBorderPolicy(detectRowBorderPolicy(preparsed))
            .setColumnBorderPolicy(detectColumnBorderPolicy(preparsed));
    }

    const detectRowBorderPolicy = (preparsed) => {
        const count = preparsed.filter(row => row.match(/\\hline/)).length;
        return intToBorderPolicy(count);
    }

    const detectColumnBorderPolicy = (preparsed) => {
        const bordersDefinition = preparsed.filter(row => isVerticalBordersDefinition(row));
        if (bordersDefinition.length === 0) {
            return BorderPolicy.NONE;
        }
        const subject = bordersDefinition[0];
        const inside = subject.match(VERTICAL_BORDERS_DEFINITION);
        const group = inside[1];
        const count = StringUtils.countOccurrences(group, /\|/);
        return intToBorderPolicy(count);
    }

    const intToBorderPolicy = (count) => {
        if (count === 0) {
            return BorderPolicy.NONE;
        }
        if (count === 3) {
            return BorderPolicy.FIRST;
        }
        return BorderPolicy.EVERY;
    }

    const extractData = (preparsed) => {
        const filtered = preparsed.filter(row => isDataRow(row));
        const rawData = filtered.map(row => row.replace(/ ?\\\\ ?(?:\\hline)?/, ''));
        const data = rawData.map(row => row.split(/ ?& ?/));
        return data.map(row =>
            row.map(cell => (StringUtils.isBlank(cell) || cell === placeholder) ? null : cell)
        );
    }

    const isDataRow = (row) => {
        return (!isVerticalBordersDefinition(row) && !isHorizontalLine(row));
    }

    const isHorizontalLine = (row) => {
        const reg = /^\\hline/;
        return row.match(reg);
    }

    const isVerticalBordersDefinition = (row) => {
        return row.match(VERTICAL_BORDERS_DEFINITION);
    }

};
