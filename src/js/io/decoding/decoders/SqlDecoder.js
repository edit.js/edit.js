/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Decoder} provides support for SQL scripts.
 *
 * @see Format.SQL
 */
const SqlDecoder = function() {

    Decoder.call(this);

    const INSERT_PATTERN = 'INSERT INTO\\s*\\w*(\\([^\\)]*\\))';
    const VALUES_PATTERN = INSERT_PATTERN + '\\s*VALUES(\\([^\\;]*)';

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.decode = (input) => {
        const sqlStatement = formatInput(input);
        const columnNames = parseColumnNames(sqlStatement);
        const values = parseValues(sqlStatement);
        // add column names to the matrix
        values.splice(0, 0, columnNames);
        const tableModel = new TableModel([], values);
        return new ParserResult(tableModel, new TableMetadata());
    };

    /**
     * @inheritDoc
     * @override
     */
    this.supports = (input) => {
        const reg = new RegExp(VALUES_PATTERN);
        const sqlStatement = formatInput(input);
        return (sqlStatement.match(reg) !== null) ? Format.SQL : null;
    };

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const formatInput = (input) => {
        const stripped = Decoders.strip(input);
        return stripped.join('');
    };

    const parseValues = (input) => {
        const valuesRegexp = new RegExp(VALUES_PATTERN);
        const valuesStatement = input.match(valuesRegexp)[2];
        const split = valuesStatement.split(/\(|\),*/);
        const stripped = Decoders.removeEmptyCells(split);
        const values = stripped.map(line => line.split(/,\s*/));
        return values.map(row => row.map(cell => StringUtils.removeFirstAndLastCharacter(cell)));
    };

    const parseColumnNames = (input) => {
        const insertRegexp = new RegExp(INSERT_PATTERN);
        const insertStatement = input.match(insertRegexp)[1];
        // get parentheses content
        const columnNames = insertStatement.replaceAll(/[()]/g, '').split(',');
        return columnNames.map(e => e.trim());
    };

};
