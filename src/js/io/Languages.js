/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A singleton object providing global read-only access to C-type programming
 * languages descriptors.
 *
 * @see Format
 */
const Languages = (function() {

    const TokenType = Object.freeze({
        PREFIX:          'prefix',
        SUFFIX:          'suffix',
        ROW_PREFIX:      'rowPrefix',
        ROW_SUFFIX:      'rowSuffix',
        VALUE_SEPARATOR: 'valueSeparator'
    });

    const DESCRIPTORS = [
        {
            language: Format.C_SHARP,
            quotes: {key: '"', value: '"', htmlEntity: HtmlEntities.QUOTE_DOUBLE},
            keywords: ['new', 'string'],
            specificFormats: [
                CSharpFormat.ARRAY_2D,  CSharpFormat.ARRAY_JAGGED
            ],
            config: [
                ['string[,] arr = {',   'string[][] arr = {'],
                ['};',                  '};'],
                ['{',                   'new string[] {'],
                ['}',                   '}'],
                [',',                   ',']
            ],
            detection: [
                /\w+\[,] ?\w+ ?= ?{/,   /new \w+\[] ? {/
            ]
        },
        {
            language: Format.JAVA,
            quotes: {key: '"', value: '"', htmlEntity: HtmlEntities.QUOTE_DOUBLE},
            keywords: ['String', 'List'],
            specificFormats: [
                JavaFormat.ARRAY_2D,    JavaFormat.LIST_2D
            ],
            config: [
                ['String[][] arr = {',  'List<List<String>> arr = List.of('],
                ['};',                  ');'],
                ['{',                   'List.of('],
                ['}',                   ')'],
                [',',                   ',']
            ],
            detection: [
                /\w+\[]\[] /,           /List<List<\w+>>/
            ]
        },
        {
            language: Format.PHP,
            quotes: {key: '"', value: '"', htmlEntity: HtmlEntities.QUOTE_DOUBLE},
            keywords: ['$arr', 'array'],
            specificFormats: [
                PhpFormat.ARRAY_2D,     PhpFormat.ASSOCIATIVE_2D
            ],
            config: [
                ['$arr = array(',       '$arr = array('],
                [');',                  ');'],
                ['array(',              'array('],
                [')',                   ')'],
                [',',                   ' =>']
            ],
            detection: [
                /array\s*\(\s*array\s*\(\s*"[^"]*",/, /array\s*\(\s*array\s*\(\s*"[^"]*" *=>/
            ]
        },
        {
            language: Format.JAVASCRIPT,
            quotes: {key: '', value: "'", htmlEntity: HtmlEntities.QUOTE_SINGLE},
            keywords: ['const'],
            specificFormats: [
                JsonFormat.ARRAY_2D,    JsonFormat.ASSOCIATIVE_2D
            ],
            config: [
                ['const arr = [',       'const arr = ['],
                ['];',                  '];'],
                ['[',                   '{'],
                [']',                   '}'],
                [',',                   ':']
            ],
            detection: [
                /\[\s*\[\s*'/,          /\[\s*{\s*[^:]:/
            ]
        },
        {
            language: Format.JSON,
            quotes: {key: '"', value: '"', htmlEntity: HtmlEntities.QUOTE_DOUBLE},
            keywords: [],
            specificFormats: [
                JsonFormat.ARRAY_2D,    JsonFormat.ASSOCIATIVE_2D
            ],
            config: [
                ['[',                   '['],
                [']',                   ']'],
                ['[',                   '{'],
                [']',                   '}'],
                [',',                   ':']
            ],
            detection: [
                /\[\s*\[\s*"/,          /\[\s*{\s*[^:]:/
            ]
        },
        {
            language: Format.PYTHON,
            quotes: {key: "'", value: "'", htmlEntity: HtmlEntities.QUOTE_SINGLE},
            keywords: [],
            specificFormats: [
                PythonFormat.ARRAY_2D,  PythonFormat.ASSOCIATIVE_2D
            ],
            config: [
                ['arr = [',             'arr = ['],
                [']',                   ']'],
                ['[',                   '{'],
                [']',                   '}'],
                [',',                   ':']
            ],
            detection: [
                /\[\s*\[\s*'/,          /\[\s*{\s*'/
            ]
        }
    ].map(e => createDescriptor(e));

    const ASSOCIATIVE_FORMATS = [JsonFormat.ASSOCIATIVE_2D, PhpFormat.ASSOCIATIVE_2D, PythonFormat.ASSOCIATIVE_2D];

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    return singleton({
        getDescriptors: () => {
            return [...DESCRIPTORS];
        },

        getDescriptor: (format) => {
            return DESCRIPTORS.find(descriptor => descriptor.language === format);
        },

        isAssociative: (specificFormat) => {
            return ArrayUtils.contains(ASSOCIATIVE_FORMATS, specificFormat);
        }
    });

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    function createDescriptor(languageConfig) {
        const config = languageConfig.config;
        const specific = languageConfig.specificFormats;
        const descriptor = {
            language: languageConfig.language,
            quotes: languageConfig.quotes,
            keywords: languageConfig.keywords,
            specificFormats: specific,
            getToken: function(tokenType, specificFormat) {
                return this[tokenType].find(descriptor => descriptor.type === specificFormat).str;
            }
        };
        [TokenType.PREFIX, TokenType.SUFFIX, TokenType.ROW_PREFIX, TokenType.ROW_SUFFIX, TokenType.VALUE_SEPARATOR]
            .forEach((prop, i) => descriptor[prop] = chars(specific[0], config[i][0], specific[1], config[i][1]));
        descriptor.detection = specific.map((e, i) => patterns(e, languageConfig.detection[i]));
        return Object.freeze(descriptor);
        function patterns(a, b) {
            return {specific: a, pattern: b};
        }
        function chars(a, b, c, d) {
            return [
                {type: a, str: b},
                {type: c, str: d}
            ];
        }
    }

    function singleton(obj) {
        ObjectUtils.forEach(TokenType, (key, value) => {
            obj[key] = value;
        });
        return Object.freeze(obj);
    }

})();
