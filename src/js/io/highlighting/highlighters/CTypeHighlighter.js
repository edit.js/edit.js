/*
* Copyright (c) 2021. Julien Fischer
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*         http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/

/**
 * This {@link Highlighter} provides support for CType languages syntax.
 *
 * @param {Format} format  the format of the input to highlight
 *
 * @see Format.JSON
 * @see Format.JAVASCRIPT
 * @see Format.JAVA
 * @see Format.C_SHARP
 * @see Format.PHP
 * @see Format.PYTHON
 */
const CTypeHighlighter = function(format) {

    Highlighter.call(this);

    const DELIMITERS = ['[', ']', '{', '}', '(', ')', ',', ':', '=', ';'];
    const ESCAPED_DELIMITERS = [HtmlEntities.GREATER_THAN.escaped, HtmlEntities.LESS_THAN.escaped];

    const descriptor = Languages.getDescriptor(format);
    const keywords = descriptor.keywords;
    const quoteEntity = descriptor.quotes.htmlEntity;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.parseTokens = (arr) => {
        return arr.map(line => highlightLine(line));
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const highlightLine = (subject) => {
        let insideString = false;
        let openingStringIndex = -1;
        const tokens = [];
        const startIndex = StringUtils.indexOfFirstNonWhitespaceCharacter(subject);
        const endIndex = StringUtils.indexOfFirstNonWhitespaceCharacter(subject, true);
        if (startIndex < 0) {
            return [];
        }
        for (let i = startIndex; i < endIndex; i++) {
            const char = subject.charAt(i);
            const escapedEntity = (char === '&') ? this.getEscapedHtmlEntity(subject, i) : null;
            if (isEscapedHtmlQuoteEntity(escapedEntity) && !this.isBackSlashed(subject, i)) {
                insideString = !insideString;
                const length = escapedEntity.length;
                if (!insideString) {
                    const substr = subject.substring(openingStringIndex, i + length);
                    this.save(tokens, substr, openingStringIndex, TokenType.STRING);
                } else {
                    openingStringIndex = i;
                }
                i += length - 1;
            } else if (!insideString) {
                if (isDelimiter(char)) {
                    this.save(tokens, char, i, TokenType.DELIMITER);
                } else if (isEscapedDelimiter(escapedEntity)) {
                    this.save(tokens, escapedEntity, i, TokenType.DELIMITER);
                    i += escapedEntity.length - 1;
                } else {
                    const word = StringUtils.match(subject, /\$?\w+/, i);
                    if (word != null && i === word.getOffset() && isKeyword(word.getMatch())) {
                        this.save(tokens, word.getMatch(), i, TokenType.KEYWORD);
                        i += word.getLength() - 1;
                    }
                }
            }
        }
        return tokens;
    }

    const isEscapedHtmlQuoteEntity = (entity) => {
        return (entity === quoteEntity.escaped);
    }

    const isDelimiter = (char) => {
        return ArrayUtils.contains(DELIMITERS, char);
    }

    const isEscapedDelimiter = (entity) => {
        return ArrayUtils.contains(ESCAPED_DELIMITERS, entity);
    }

    const isKeyword = (word) => {
        return ArrayUtils.contains(keywords, word);
    }

}
