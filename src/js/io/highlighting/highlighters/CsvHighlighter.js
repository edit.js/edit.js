/*
* Copyright (c) 2021. Julien Fischer
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*         http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/

/**
 * This {@link Highlighter} provides support for CSV format.
 *
 * @param {string} separator  the character to use as a value separator.
 *
 * @see Format.CSV
 */
const CsvHighlighter = function(separator) {

    Highlighter.call(this);

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.parseTokens = (arr) => {
        return arr.map(line => highlightLine(line));
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const highlightLine = (subject) => {
        const tokens = [];
        const startIndex = StringUtils.indexOfFirstNonWhitespaceCharacter(subject);
        const endIndex = StringUtils.indexOfFirstNonWhitespaceCharacter(subject, true);
        if (startIndex < 0) {
            return [];
        }
        for (let i = startIndex; i < endIndex; i++) {
            const c = subject.charAt(i);
            if (isSeparator(c)) {
                this.save(tokens, c, i, TokenType.DELIMITER);
            } else {
                let valueEndIndex = subject.length;
                for (let x = i; x < subject.length; x++) {
                    const char = subject.charAt(x);
                    if (isSeparator(char)) {
                        valueEndIndex = x;
                        break;
                    }
                }
                const substr = subject.substring(i, valueEndIndex);
                this.save(tokens, substr, i, TokenType.STRING);
                const inc = substr.length - 1;
                if (inc > 0) {
                    i += inc;
                }
            }
        }
        return tokens;
    }

    const isSeparator = (c) => {
        return (c === separator);
    }

}
