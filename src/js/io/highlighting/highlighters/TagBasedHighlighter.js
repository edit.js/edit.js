/*
* Copyright (c) 2021. Julien Fischer
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*         http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/

/**
 * This {@link Highlighter} provides support for tag-based tables.
 *
 * @param {Format} format  the format of the input to highlight
 *
 * @see Format.BBCODE
 * @see Format.HTML
 */
const TagBasedHighlighter = function(format) {

    Highlighter.call(this);

    const TAG_DELIMITERS = StringUtils.escapeHTMLChars(['[', ']']);

    const isBBCode = (format === Format.BBCODE);
    const openingTagDelimiterChar = isBBCode ? '[' : '&';
    const closingTagDelimiterChar = isBBCode ? ']' : '&';

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.parseTokens = (arr) => {
        return arr.map(line => highlightLine(line));
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const highlightLine = (subject) => {
        const tokens = [];
        const startIndex = StringUtils.indexOfFirstNonWhitespaceCharacter(subject);
        const endIndex = StringUtils.indexOfFirstNonWhitespaceCharacter(subject, true);
        if (startIndex < 0) {
            return [];
        }
        for (let i = startIndex; i <= endIndex; i++) {
            const entity = getTagDelimiter(subject, i);
            if (isTagDelimiter(entity)) {
                this.save(tokens, entity, i, TokenType.DELIMITER);
                const slashIndex = i + entity.length;
                const optionalSlash = subject.charAt(slashIndex);
                if (optionalSlash === '/') {
                    this.save(tokens, optionalSlash, slashIndex, TokenType.DELIMITER);
                }
                const tagName = StringUtils.match(subject, /\w+/, i + entity.length);
                if (tagName != null) {
                    this.save(tokens, tagName.getMatch(), tagName.getOffset(), TokenType.TAG);
                    i = tagName.getEndIndex();
                }
                let attrNameIndex = this.getIndexOfNextNonWhitespaceCharacter(subject, i, endIndex, i + 1);
                const nextChar = subject.charAt(attrNameIndex);
                i = attrNameIndex;
                let openingTagClosedIndex = i;
                if (nextChar !== closingTagDelimiterChar) {
                    const attributeName = StringUtils.match(subject, /\w+/, i);
                    i = attributeName.getEndIndex();
                    this.save(tokens, attributeName.getMatch(), attributeName.getOffset(), TokenType.KEY);
                    const equalsIndex = this.getIndexOfNextNonWhitespaceCharacter(subject, i, endIndex);
                    this.save(tokens, subject.charAt(equalsIndex), equalsIndex, TokenType.DELIMITER);
                    const attrValueIndex = this.getIndexOfNextNonWhitespaceCharacter(subject, equalsIndex + 1, endIndex);
                    const openingQuoteEntity = getTagDelimiter(subject, attrValueIndex);
                    const closingQuoteEntity = StringUtils.match(subject, HtmlEntities.QUOTE_DOUBLE.escaped, i + openingQuoteEntity.length);
                    const attributeString = subject.substring(i + 1, closingQuoteEntity.getEndIndex());
                    this.save(tokens, attributeString, i + 1, TokenType.ATTRIBUTE);
                    i = closingQuoteEntity.getEndIndex();
                }
                openingTagClosedIndex = this.getIndexOfNextNonWhitespaceCharacter(subject, i, endIndex);
                const openingTagClosedEntity = getTagDelimiter(subject, openingTagClosedIndex);
                this.save(tokens, openingTagClosedEntity, openingTagClosedIndex, TokenType.DELIMITER);
                i += openingTagClosedEntity.length;
                const closingTagOpenIndex = this.getIndexOfNextCharacter(subject, i, openingTagDelimiterChar, endIndex, i + 1);
                const innerTextIndex = openingTagClosedIndex + openingTagClosedEntity.length;
                const tagInnerText = subject.substring(innerTextIndex, closingTagOpenIndex);
                if (tagInnerText !== '') {
                    this.save(tokens, tagInnerText, innerTextIndex, TokenType.VALUE);
                }
                i = closingTagOpenIndex - 1;
            }
        }

        return tokens;
    }

    const getTagDelimiter = (subject, index) => {
        return isBBCode ? subject.charAt(index) : this.getEscapedHtmlEntity(subject, index);
    }

    const isTagDelimiter = (entity) => {
        return isBBCode ? isBBCodeTagDelimiter(entity) : isHtmlTagDelimiter(entity);
    }

    const isHtmlTagDelimiter = (entity) => {
        return this.isEscapedHtmlDelimiterEntity(entity);
    }

    const isBBCodeTagDelimiter = (entity) => {
        return ArrayUtils.contains(TAG_DELIMITERS, entity);
    }

}
