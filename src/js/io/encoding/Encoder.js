/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * Encoders provide methods for generating a string representation of a {@link TableModel}.
 *
 * @param {TableMetadata} metadata  the formatting options to use for tuning the
 *                                  serialization process
 *
 * @see TableModel
 * @see Format
 */
const Encoder = function(metadata) {

    /**
     * Serializes the specified {@link TableModel}.
     * This virtual method must be implemented by subtypes.
     *
     * @param {TableModel} tableModel  the {@link TableModel} to serialize
     * @return {string[]}
     */
    this.encode = (tableModel) => { throw new Error('Not implemented yet'); }

}
