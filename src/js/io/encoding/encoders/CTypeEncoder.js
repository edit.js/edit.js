/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Encoder} provides support for C-Type languages.
 *
 * @param {TableMetadata} metadata  the formatting options to use for tuning the
 *                                  serialization process
 * @see Format.JAVA
 * @see Format.C_SHARP
 * @see Format.PHP
 * @see Format.JAVASCRIPT
 * @see Format.PYTHON
 * @see Format.JSON
 */
const CTypeEncoder = function(metadata) {

    Encoder.call(this, metadata);

    const BACKSLASH = '\\';
    const SPACE = ' ';
    const INDENT_SIZE = 4;

    const languageDescriptor = Languages.getDescriptor(metadata.getFormat());
    const languageSpecificFormat = getLanguageSpecificFormat();
    const isMinify = (metadata.getOutputPolicy() === OutputPolicy.MINIFY);
    const rowIndent = or(StringUtils.repeat(SPACE, INDENT_SIZE));
    const optionalSpace = isMinify ? SPACE : '';
    const valueQuotes = languageDescriptor.quotes.value;
    const keyQuotes = languageDescriptor.quotes.key;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.encode = (tableModel) => {
        const data = MatrixUtils.nullSafe(tableModel.getData());
        const escaped = escapeQuotes(data);
        const result = [getToken(Languages.PREFIX), ...printArray(escaped), getToken(Languages.SUFFIX)];
        return isMinify ? [result.join('')] : result;
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const printArray = (data) => {
        return Languages.isAssociative(languageSpecificFormat) ? printAssociativeArray(data) : print2dArray(data);
    }

    const print2dArray = (data) => {
        const result = [];
        data.forEach((row, y) => {
            let rowStr = rowIndent + getToken(Languages.ROW_PREFIX);
            row.forEach((cell, x) => {
                const cellSeparator = isLastElement(x, row) ? '' : getToken(Languages.VALUE_SEPARATOR) + SPACE;
                rowStr += valueQuotes + cell + valueQuotes + cellSeparator;
            });
            const rowSeparator = isLastElement(y, data) ? '' : ',' + optionalSpace;
            rowStr += getToken(Languages.ROW_SUFFIX) + rowSeparator;
            result.push(rowStr);
        });
        return result;
    }

    const printAssociativeArray = (data) => {
        const CELL_INDENT = rowIndent + rowIndent;
        const properties = data[0];
        data.splice(0, 1);
        const result = [];
        data.forEach((row, y) => {
            let rowStr = rowIndent + getToken(Languages.ROW_PREFIX);
            if (!isMinify) {
                result.push(rowStr);
                rowStr = CELL_INDENT;
            }
            row.forEach((cell, x) => {
                const cellSeparator = isLastElement(x, row) ? '' : ',' + optionalSpace;
                const keyStr = keyQuotes + properties[x] + keyQuotes;
                const valueStr = valueQuotes + cell + valueQuotes;
                rowStr += keyStr + getToken(Languages.VALUE_SEPARATOR) + SPACE + valueStr + cellSeparator;
                if (!isMinify) {
                    result.push(rowStr);
                    rowStr = isLastElement(x, row) ? rowIndent : CELL_INDENT;
                }
            });
            const rowSeparator = isLastElement(y, data) ? '' : ',' + optionalSpace;
            rowStr += getToken(Languages.ROW_SUFFIX) + rowSeparator;
            result.push(rowStr);
        });
        return result;
    }

    const isLastElement = (i, arr) => {
        return (i === arr.length - 1);
    }

    const getToken = (tokenType) => {
        return languageDescriptor.getToken(tokenType, languageSpecificFormat);
    }

    const escapeQuotes = (data) => {
        return data.map(row => row.map(cell => escapeQuotesInCell(cell)));
    }

    const escapeQuotesInCell = (cell) => {
        const quote = valueQuotes;
        let escaped = cell;
        let index = -1;
        for(;;) {
            index = StringUtils.indexOf(escaped, quote, index + 1);
            if (index < 0) {
                break;
            }
            const isEscaped = (escaped.charAt(index - 1) === BACKSLASH);
            if (!isEscaped) {
                escaped = escaped.substring(0, index) + BACKSLASH + quote + escaped.substring(index + 1);
            }
        }
        return escaped;
    }

    function getLanguageSpecificFormat() {
        switch (metadata.getFormat()) {
            case Format.C_SHARP:
                return metadata.getCSharpFormat();
            case Format.PHP:
                return metadata.getPhpFormat();
            case Format.JAVA:
                return metadata.getJavaFormat();
            case Format.JAVASCRIPT:
                return metadata.getJsonFormat();
            case Format.PYTHON:
                return metadata.getPythonFormat();
            case Format.JSON:
                return metadata.getJsonFormat();
            default:
                throw new Error(`Unsupported format: ${metadata.getFormat()}`);
        }
    }

    function or(subject) {
        return isMinify ? '' : subject;
    }

}
