/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Encoder} provides support for GitHub/GitLab markdown tables.
 *
 * @param {TableMetadata} metadata  the formatting options to use for tuning the
 *                                  serialization process
 * @see Format.GITHUB_MARKDOWN
 */
const GitHubMarkdownEncoder = function(metadata) {

    Encoder.call(this, metadata);

    const BORDER_CHARACTER = "|";
    // Cache
    const minify = (metadata.getOutputPolicy() === OutputPolicy.MINIFY);

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.encode = (tableModel) => {
        const result = [];
        const nullSafeData = MatrixUtils.nullSafe(tableModel.getData());
        const longestEls = MatrixUtils.getLongestElementPerColumn(nullSafeData);
        nullSafeData.forEach((row, y) => {
            if (y === 0) {
                result.push(printLine(row, longestEls, minify, true));
                result.push(printLine(row, longestEls, minify, false, '-', tableModel.getAlignments()));
            } else {
                result.push(printLine(row, longestEls, minify, false));
            }
        });
        return result;
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const printLine = (row, longestEls, minify, header, char, alignments) => {
        const minSeparatorLength = 3;
        const space = minify ? '' : ' ';
        let asciiRow = BORDER_CHARACTER;
        row.forEach((cell, x) => {
            const longestElLength = Math.max(longestEls[x].length, minSeparatorLength);
            const separatorLength = minify ? minSeparatorLength : longestElLength;
            let content;
            if (char != null) {
                const start = computeAlignmentCharacter(alignments[x], Alignment.LEFT);
                const end = computeAlignmentCharacter(alignments[x], Alignment.RIGHT);
                content = start + StringUtils.repeat(char, separatorLength - 2) + end;
            } else {
                const datum = header ? StringUtils.formatCase(cell, metadata.getHeaderCase()) : cell;
                const diff = longestElLength - datum.length;
                const spaces = StringUtils.repeat(space, diff);
                content = datum + spaces;
            }
            asciiRow += space + content + space + BORDER_CHARACTER
        });
        return asciiRow;
    }

    const computeAlignmentCharacter = (a, alignment) => {
        return (a === alignment || a === Alignment.CENTER) ? ':' : '-';
    }

};
