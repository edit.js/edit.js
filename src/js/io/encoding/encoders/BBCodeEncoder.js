/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Encoder} provides support for BBCode tables.
 *
 * @param {TableMetadata} metadata  the formatting options to use for tuning the
 *                                  serialization process
 * @see Format.BBCODE
 */
const BBCodeEncoder = function(metadata) {

    Encoder.call(this, metadata);

    const isMinify = (metadata.getOutputPolicy() === OutputPolicy.MINIFY);
    const SPACE = ' ';
    const INDENT_SIZE = 4;
    const ROW_INDENT = or(StringUtils.repeat(SPACE, INDENT_SIZE));
    const CELL_INDENT = or(StringUtils.repeat(ROW_INDENT, 2));

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.encode = (tableModel) => {
        const nullSafeData = MatrixUtils.nullSafe(tableModel.getData());
        const result = ['[table]'];
        nullSafeData.forEach((row, y) => {
            add(`${ROW_INDENT}[tr]`, result);
            const tag = getTag(y);
            row.forEach(cell => {
                const datum = isHeader(y) ? format(cell) : cell;
                const cellStr = `${CELL_INDENT}[${tag}]${datum}[/${tag}]`;
                add(cellStr, result);
            });
            add(`${ROW_INDENT}[/tr]`, result);
        });
        add('[/table]', result);
        return result;
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const add = (subject, result) => {
        if (isMinify) {
            result[0] += subject;
        } else {
            result.push(subject);
        }
    }

    const format = (subject) => {
        return StringUtils.formatCase(subject, metadata.getHeaderCase());
    }

    const getTag = (y) => {
        return isHeader(y) ? 'th' : 'td';
    }

    const isHeader = (y) => {
        return (y === 0) && hasHeader();
    }

    const hasHeader = () => {
        return (metadata.getHeaderOrientation() === HeaderOrientation.FIRST_ROW);
    }

    function or(subject) {
        return isMinify ? '' : subject;
    }

}
