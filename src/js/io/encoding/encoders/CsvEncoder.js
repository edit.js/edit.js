/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Encoder} provides support for CSV format.
 *
 * @param {TableMetadata} metadata  the formatting options to use for tuning the
 *                                  serialization process
 * @see Format.CSV
 */
const CsvEncoder = function(metadata) {

    Encoder.call(this, metadata);

    const separator = metadata.getSeparator() || ',';

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.encode = function(tableModel) {
        const nullSafeData = MatrixUtils.nullSafe(tableModel.getData());
        const result = [];
        nullSafeData.forEach(row => {
            const rowStr = row.join(separator);
            result.push(rowStr);
        });
        return result;
    }

};
