/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Encoder} provides support for ASCII and UNICODE tables.
 *
 * @param {TableMetadata} metadata  the formatting options to use for tuning the
 *                                  serialization process
 * @see Format.PLAIN_ASCII
 * @see Format.UNICODE
 * @see Format.UNICODE_BOLD
 */
const PlainTextEncoder = function(metadata) {

    Encoder.call(this, metadata);

    const borders = BorderFactory.of(metadata.getFormat());
    // Cache
    const rowPolicy = metadata.getRowBorderPolicy();
    const columnPolicy = metadata.getColumnBorderPolicy();
    const headerOrientation = metadata.getHeaderOrientation();

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.encode = (tableModel) => {
        // configuration
        const nullSafeData = MatrixUtils.nullSafe(tableModel.getData());
        const longestElements = MatrixUtils.getLongestElementPerColumn(nullSafeData);
        const columnWidths = longestElements.map(e => e.length);
        const SPACE = ' ';
        const BORDER_LEFT = ifBorders(borders.getLeft() + SPACE);
        const BORDER_RIGHT = SPACE + ifBorders(borders.getRight());
        const borderLeft = BORDER_LEFT;
        const matrix = new Matrix().of(nullSafeData);

        const result = [];

        // generate top border
        if (!isBorderless()) {
            const chars = createBorders(borders.getTopLeft(), borders.getHorizontal(), borders.getTopRight(), borders.getTopMiddle());
            const topDivider = generateDivider(columnWidths, chars);
            result.push(topDivider);
        }

        // generate table content
        matrix.forEachRow((row, y) => {
            let asciiRow = '';
            asciiRow += borderLeft;

            row.forEach((datum, x) => {
                const formattedDatum = Decoders.formatHeaderCell(x, y, datum, metadata);
                const maxLength = longestElements[x].length;
                const diff = maxLength - datum.length;
                const alignment = getAlignment(x, y, tableModel.getAlignment(x));
                const cell = StringUtils.alignText(formattedDatum, diff, alignment);
                asciiRow += cell + (borderRight(x, row.length - 1) ? BORDER_RIGHT : SPACE);
                if (x < row.length - 1) {
                    asciiRow += SPACE;
                }
            });

            result.push(asciiRow);
            if (shouldAppendDivider(nullSafeData, y)) {
                const chars = createBorders(borders.getMiddleLeft(), borders.getHorizontal(), borders.getMiddleRight(), borders.getMiddleMiddle());
                const midDivider = generateDivider(columnWidths, chars);
                result.push(midDivider);
            }
        });

        // generate bottom border
        if (!isBorderless() && nullSafeData.length > 1) {
            const chars = createBorders(borders.getBottomLeft(), borders.getHorizontal(), borders.getBottomRight(), borders.getBottomMiddle());
            const bottomDivider = generateDivider(columnWidths, chars);
            result.push(bottomDivider);
        }

        return result;
    };

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const isBorderless = () => {
        return (
            rowPolicy === BorderPolicy.NONE ||
            columnPolicy === BorderPolicy.NONE
        );
    };

    /**
     * @param columnWidths  a list of integers
     * @param borders  the map of characters to use as borders
     */
    const generateDivider = (columnWidths, borders) => {
        let divider = '';
        columnWidths.forEach((width, x) => {
            const intersection = borderRight(x - 1, columnWidths.length - 1) ? borders.intersection : '';
            const separator = (x === 0) ? borders.left : intersection;
            divider += separator + StringUtils.repeat(borders.middle, width + 2);
        });
        return divider + borders.right;
    };

    const createBorders = (left, middle, right, intersection) => {
        return {left, middle, right, intersection};
    };

    const ifBorders = (str) => {
        return (isBorderless() ? '' : str);
    };

    const borderRight = (x, max) => {
        if (isBorderless()) {
            return false;
        }
        return (
            columnPolicy === BorderPolicy.EVERY ||
            (columnPolicy === BorderPolicy.FIRST && x === 0) ||
            x === max
        );
    }

    const getAlignment = (x, y, cellAlignment) => {
        return Decoders.isHeaderCell(x, y, headerOrientation) ? metadata.getHeaderAlignment() || cellAlignment : cellAlignment;
    }

    const shouldAppendDivider = (data, y) => {
        return (
            !isBorderless() &&(
            (rowPolicy === BorderPolicy.FIRST && y === 0) ||
            (rowPolicy === BorderPolicy.EVERY && y < data.length - 1))
        );
    }

};
