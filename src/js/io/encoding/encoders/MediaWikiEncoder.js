/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Encoder} provides support for MediaWiki tables.
 *
 * @param {TableMetadata} metadata  the formatting options to use for tuning the
 *                                  serialization process
 * @see Format.MEDIAWIKI
 */
const MediaWikiEncoder = function(metadata) {

    Encoder.call(this, metadata);

    const HEADER_SEPARATOR = '!';
    const CELL_SEPARATOR = '|';
    const ROW_SEPARATOR = CELL_SEPARATOR + '-';
    const Q = HtmlEntities.QUOTE_DOUBLE.unescaped;

    const outputPolicy = metadata.getOutputPolicy();
    const headerOrientation = metadata.getHeaderOrientation();

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * Implementation note:
     * MediaWiki table specification doesn't currently support column attributes.
     * So in order to align a column, we have to align each cell in that column.
     *
     * @inheritDoc
     * @override
     */
    this.encode = (tableModel) => {
        const data = MatrixUtils.nullSafe(tableModel.getData());

        const result = [`{| class=${Q}wikitable${Q}`];

        data.forEach((row, y) => {
            result.push(ROW_SEPARATOR);
            printRow(row, y);
        });

        function printRow(row, y) {
            let str = '';
            row.forEach((cell, x) => {
                const firstSeparator = computeFirstSeparator(y, x);
                const middleSeparator = computeMiddleSeparator(y, x);
                const alignment = tableModel.getAlignment(x);
                const datum = Decoders.formatHeaderCell(x, y, cell, metadata);
                const line = firstSeparator + align(alignment, middleSeparator) + ' ' + datum;
                if (outputPolicy === OutputPolicy.PRETTIFY || (headerOrientation === HeaderOrientation.FIRST_COLUMN && x === 0)) {
                    result.push(line);
                } else {
                    const midSeparator = (x < row.length - 1) ? ' ' + middleSeparator : '';
                    str += line + midSeparator;
                }
            });
            if (outputPolicy !== OutputPolicy.PRETTIFY) {
                result.push(str);
            }
        }

        result.push(CELL_SEPARATOR + '}');
        return result;
    };

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const align = (alignment, separator) => {
        return (alignment === Alignment.LEFT) ?
            '' :
            ' style="text-align: ' + alignment.displayName.toLowerCase() + ';" ' + separator;
    };

    const computeFirstSeparator = (rowIndex, cellIndex) => {
        if (headerOrientation === HeaderOrientation.NO_HEADER) {
            return CELL_SEPARATOR;
        }
        if (headerOrientation === HeaderOrientation.FIRST_ROW) {
            return (rowIndex === 0) ? HEADER_SEPARATOR : CELL_SEPARATOR;
        } else if (headerOrientation === HeaderOrientation.FIRST_COLUMN) {
            return (cellIndex === 0) ? HEADER_SEPARATOR : CELL_SEPARATOR;
        }
    };

    const computeMiddleSeparator = (rowIndex, cellIndex) => {
        if (headerOrientation === HeaderOrientation.NO_HEADER) {
            return CELL_SEPARATOR;
        }
        if (headerOrientation === HeaderOrientation.FIRST_ROW) {
            return (rowIndex === 0) ? HEADER_SEPARATOR : CELL_SEPARATOR;
        } else if (headerOrientation === HeaderOrientation.FIRST_COLUMN) {
            return CELL_SEPARATOR;
        }
    };

};
