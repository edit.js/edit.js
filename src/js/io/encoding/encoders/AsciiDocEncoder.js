/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Encoder} provides support for AsciiDoc tables.
 *
 * @param {TableMetadata} metadata  the formatting options to use for tuning the
 *                                  serialization process
 * @see Format.ASCII_DOC
 */
const AsciiDocEncoder = function(metadata) {

    Encoder.call(this, metadata);

    const CELL_SEPARATOR = '|';
    const TABLE_SEPARATOR = '=';
    const SPACE = ' ';

    const isHorizontal = (metadata.getHeaderOrientation() === HeaderOrientation.FIRST_ROW);
    const isVertical = (metadata.getHeaderOrientation() === HeaderOrientation.FIRST_COLUMN);
    const isMinify = (metadata.getOutputPolicy() === OutputPolicy.MINIFY);

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.encode = (tableModel) => {
        const alignments = tableModel.getAlignments();
        const data = MatrixUtils.nullSafe(tableModel.getData());
        const columnWidths = MatrixUtils.getColumnWidths(data);
        const result = generateOutput(data, alignments, columnWidths);
        const dividerLength = isMinify ? result[0].length - 1 : 3;
        addDividers(result, dividerLength);
        addOptionalColumnSpecifiers(result, alignments, columnWidths);
        return result;
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const generateOutput = (data, alignments, columnWidths) => {
        const result = [];
        data.forEach((row, y) => {
            const firstRow = (y === 0);
            const inlineCells = (isMinify || firstRow);
            let rowStr = CELL_SEPARATOR;
            row.forEach((cell, x) => {
                const formatted = formatHeaderCase(cell, x, y);
                const aligned = isMinify ? alignCell(formatted, columnWidths, alignments, x) : formatted;
                const separator = (x < row.length - 1) ? SPACE + CELL_SEPARATOR : '';
                if (inlineCells) {
                    rowStr += SPACE + aligned + separator;
                } else {
                    rowStr = CELL_SEPARATOR + SPACE + aligned;
                    result.push(rowStr);
                }
            });
            if (inlineCells) {
                result.push(rowStr);
            }
            if (isHorizontal && firstRow) {
                result.push('');
            }
        });
        return result;
    }

    const addOptionalColumnSpecifiers = (result, alignments, columnWidths) => {
        let arr;
        if (alignments.length > 0) {
            arr = alignments
                .map(alignment => translate(alignment))
                .map((symbol, x) => symbol + columnSpecifier(x));
        } else if (isVertical) {
            const numberOfColumns = columnWidths.length - 1;
            arr = ['h', ...ArrayUtils.repeat(1, numberOfColumns)];
        } else {
            return;
        }
        addTableMetadata(result, arr);
    }

    const addTableMetadata = (result, columnSpecifiers) => {
        return result.splice(0, 0, `[cols="${columnSpecifiers.join(',')}"]`);
    }

    const columnSpecifier = (x) => {
        return (isHorizontalHeader(x) ? 'h' : '1');
    }

    const translate = (alignment) => {
        switch (alignment) {
            case Alignment.CENTER:
                return '^';
            case Alignment.RIGHT:
                return '>';
            case Alignment.LEFT:
                // Fall through
            default:
                return '<';
        }
    }

    const alignCell = (cell, columnWidths, alignments, x) => {
        const width = columnWidths[x];
        const diff = width - cell.length;
        const alignment = getAlignment(alignments, x);
        return StringUtils.alignText(cell, diff, alignment);
    }

    const getAlignment = (alignments, x) => {
        return (x < alignments.length) ? alignments[x] : Alignment.LEFT;
    }

    const formatHeaderCase = (cell, x, y) => {
        return isHeaderCell(x, y) ? StringUtils.formatCase(cell, metadata.getHeaderCase()) : cell;
    }

    const isHeaderCell = (x, y) => {
        return (isHorizontal && y === 0) || isHorizontalHeader(x);
    }

    const isHorizontalHeader = (x) => {
        return (isVertical && x === 0);
    }

    const addDividers = (arr, n) => {
        const divider = CELL_SEPARATOR + StringUtils.repeat(TABLE_SEPARATOR, n);
        arr.splice(0, 0, divider);
        arr.push(divider);
    }

}
