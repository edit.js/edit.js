/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A factory class vending {@link TextBorder} objects.
 */
const BorderFactory = {

    /**
     * Returns the {@link TextBorder} mapped to this {@link Format}.
     *
     * @param format  the borders {@link Format} to lookup
     * @return a {@link TextBorder} instance
     */
    of: (format) => {
        switch (format) {
            case Format.UNICODE:
                return new TextBorder(
                    '┌', '┬', '┐',
                    '│', '─', '│',
                    '├', '┼', '┤',
                    '└', '┴', '┘'
                );
            case Format.UNICODE_BOLD:
                return new TextBorder(
                    '╔', '╦', '╗',
                    '║', '═', '║',
                    '╠', '╬', '╣',
                    '╚', '╩', '╝'
                );
            case Format.PLAIN_ASCII:
                return new TextBorder(
                    '+', '+', '+',
                    '|', '-', '|',
                    '+', '+', '+',
                    '+', '+', '+'
                );
        }
    }

};
