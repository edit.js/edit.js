/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A value class used to print the borders of a plain text table using
 * its set of ASCII or unicode characters.
 *
 * @param {...string} characters  a set of ASCII or unicode characters
 */
const TextBorder = function(...characters) {

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.contains = (char) => {
        return ArrayUtils.contains(characters, char);
    }

    this.getCharacterList = () => { return characters; }

    // delimiters
    this.getLeft            = () => { return characters[3]; }
    this.getHorizontal      = () => { return characters[4]; }
    this.getRight           = () => { return characters[5]; }
    // top
    this.getTopLeft         = () => { return characters[0]; }
    this.getTopMiddle       = () => { return characters[1]; }
    this.getTopRight        = () => { return characters[2]; }
    // middle
    this.getMiddleLeft      = () => { return characters[6]; }
    this.getMiddleMiddle    = () => { return characters[7]; }
    this.getMiddleRight     = () => { return characters[8]; }
    // bottom
    this.getBottomLeft      = () => { return characters[9]; }
    this.getBottomMiddle    = () => { return characters[10]; }
    this.getBottomRight     = () => { return characters[11]; }

}
