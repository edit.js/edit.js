/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const editjs = {

    VERSION: '2.3.1',
    REPOSITORY: 'https://gitlab.com/edit.js/edit.js',
    HOST: 'GitLab',
    // must be true for production
    PRODUCTION: true,
    LOG_LEVEL: LogLevel.ERROR,

    init: () => {
        // config
        Logger.setProduction(editjs.PRODUCTION);
        Logger.setLevel(editjs.LOG_LEVEL);
        const layoutManager = new LayoutManager(Settings.DEFAULT_LAYOUT_ORIENTATION);
        const themeManager = new ThemeManager(Settings.THEME, Settings.FALLBACK_THEME);
        const rootComponent = new Component()
            .setUI(document.getElementById('editjs'))
        // event processors
        const GlobalActions = {
            TOGGLE_IMPORT:         new Action('Toggle Import Dialog',  Context.GLOBAL, new KeyStroke(Key.I,   Modifier.CTRL), toggleImportDialog),
            TOGGLE_HELP:           new Action('Toggle Help',           Context.GLOBAL, new KeyStroke(Key.Q,   Modifier.CTRL), displayHelp),
            TOGGLE_EDITOR_ACTIONS: new Action('Toggle Editor Actions', Context.GLOBAL, new KeyStroke(Key.E,   Modifier.CTRL), toggleEditorActions),
            UNDO:                  new Action('Undo Last Operation',   Context.GLOBAL, new KeyStroke(Key.Z,   Modifier.CTRL), onUndo),
            REDO:                  new Action('Redo Last Operation',   Context.GLOBAL, new KeyStroke(Key.Y,   Modifier.CTRL), onRedo),
            CLOSE_DIALOG:          new Action('Close Dialog',          Context.GLOBAL, new KeyStroke(Key.ESCAPE),             closeDialog)
        }
        const globalActions = [
            GlobalActions.UNDO,
            GlobalActions.REDO,
            GlobalActions.CLOSE_DIALOG,
            GlobalActions.TOGGLE_HELP,
            GlobalActions.TOGGLE_IMPORT,
            GlobalActions.TOGGLE_EDITOR_ACTIONS
        ];
        const globalEventProcessor = new EventProcessor(...globalActions);
        const editorEventProcessor = new EventProcessor();
        const importDialogEventProcessor = new EventProcessor();
        // components initialization and wiring
        const messagePool = new MessagePool(Settings.MESSAGE_POOL_CAPACITY, Settings.MESSAGE_DURATION, rootComponent);
        const rendererButton = new ExpandButton(Side.RIGHT, layoutManager.hideEditor, layoutManager.restoreComponents, 'Expand Result', 'Result');
        const editorButton = new ExpandButton(Side.RIGHT, layoutManager.hideRenderer, layoutManager.restoreComponents, 'Expand Editor', 'Editor');
        const history = new CommandHistory(Settings.HISTORY_CAPACITY);
        const renderer = new TableRenderer(rendererButton, messagePool, onPrefillRequest, layoutManager.getOrientation());
        const table = new EditorTable(Settings.INITIAL_ROWS, Settings.INITIAL_COLUMNS);
        const reader = new TableReader();
        const editor = new Editor(table, history, editorEventProcessor, editorButton);
        const createDialog = new CreateDialog(Settings.INITIAL_ROWS, Settings.INITIAL_COLUMNS);
        const importDialog = new ImportDialog(reader, messagePool, importDialogEventProcessor);
        const helpDialog = new HelpDialog([...globalActions, ...editorEventProcessor.getActions(), ...importDialogEventProcessor.getActions()]);
        const formatPicker = Forms.createFormatPicker('Defines the target format', 'fp-toolbar');
        const toolbar = new Toolbar(formatPicker, importDialog, createDialog, GlobalActions);
        const splitPane = new SplitPane(editor, renderer, layoutManager.getOrientation());
        const helpButton = new HelpButton(helpDialog, GUI.ppAction(GlobalActions.TOGGLE_HELP));
        const switchLayoutButton = createSwitchButton();
        const toggleSyntaxHighlightingButton = createSyntaxHighlightingButton();
        const themeButton = createThemeButton();
        layoutManager.initLayout();
        // event subscriptions
        toolbar.addEditorActionListener(editor);
        toolbar.addRenderOptionChangeListener(editor);
        toolbar.addRenderOptionChangeListener(renderer);
        toolbar.addCopyActionListener(renderer);
        reader.addImportListener(editor);
        reader.addFormatChangeListener(toolbar);
        editor.addTableEditListener(renderer);
        createDialog.addTableCreateListener(editor);

        // wrap up
        const defaultLayer = new Component()
            .addClass('app-container')
            .add(splitPane)
            .add(importDialog)
            .add(createDialog)
            .add(helpDialog)
            .add(helpButton)
            .add(switchLayoutButton)
            .add(toggleSyntaxHighlightingButton)
            .add(themeButton);

        rootComponent
            .add(toolbar)
            .add(defaultLayer);

        updateSyntaxHighlightingButton();
        themeManager.applyPreferredTheme();
        table.focus();

        document.onkeydown = evt => {
            const action = globalEventProcessor.getAction(evt);
            if (action != null) {
                evt.preventDefault();
                action.process(evt);
            }
        };
        window.addEventListener('resize', layoutManager.onResize, true);

        function closeDialog(ignored) {
            [formatPicker, createDialog, importDialog, helpDialog]
                .forEach(dialog => dialog.setOpen(false));
            toolbar.closeContextualMenu();
            toolbar.openHamburger(false);
        }
        function onUndo(e) {
            history.undo();
            e.preventDefault();
        }
        function onRedo(e) {
            history.redo();
            e.preventDefault();
        }
        function displayHelp() {
            helpButton.getUI().click();
        }
        function toggleImportDialog() {
            importDialog.toggle();
        }
        function toggleEditorActions() {
            toolbar.toggleHamburger();
        }

        function onPrefillRequest(model) {
            editor.onModelImport(model);
        }

        function createSwitchButton() {
            const targets = [editor, renderer];
            return new FabButton(Icons.LAYOUT, layoutManager.switchLayout, 'Switch Layout Orientation', 'switch-layout-btn', 'float-btn')
                .addListener('mouseenter', () => targets.forEach(t => t.addClass('highlighted')), false)
                .addListener('mouseleave', () => targets.forEach(t => t.removeClass('highlighted')), false);
        }

        function createThemeButton() {
            return new FabButton(Icons.THEME, themeManager.toggleTheme, 'Toggle Dark Mode', 'switch-theme-btn', 'float-btn');
        }

        function createSyntaxHighlightingButton() {
            return new FabButton(Icons.HIGHLIGHTING, toggleSyntaxHighlighting, 'Toggle Syntax Highlighting', 'toggle-highlighting-btn', 'float-btn');
        }

        function toggleSyntaxHighlighting() {
            renderer.toggleSyntaxHighlighting();
            updateSyntaxHighlightingButton();
        }

        function updateSyntaxHighlightingButton() {
            const angle = renderer.isSyntaxHighlightingEnabled() ? 0 : 90;
            toggleSyntaxHighlightingButton.rotateIcon(angle);
        }


        function ThemeManager(them, defaultTheme) {

            const NODE_ID = 'theme-stylesheet';

            // config
            let theme;
            // cache
            let stylesheetNode;

            //////////////////////////////////////////////////////////////////
            // API
            //////////////////////////////////////////////////////////////////

            this.toggleTheme = () => {
                this.applyTheme(isLight(theme) ? Themes.DARK : Themes.LIGHT);
            }

            this.applyPreferredTheme = () => {
                this.applyTheme(getPreferredTheme());
            }

            this.applyTheme = (t) => {
                rootComponent.removeClass('theme-' + theme);
                theme = t;
                rootComponent.addClass('theme-' + theme);
                themeButton.rotateIcon(this.getAngle(theme));
                linkCssFile(theme);
            }

            this.getAngle = (t) => {
                return isLight(t) ? 0 : 180;
            }

            //////////////////////////////////////////////////////////////////
            // Internal
            //////////////////////////////////////////////////////////////////

            const isLight = (t) => {
                return (t === Themes.LIGHT);
            }

            const linkCssFile = (t) => {
                if (t == null) {
                    throw new Error('Cannot set null theme');
                }
                stylesheetNode.href = buildStylesheetPath(t);
            }

            const initThemeStylesheetNode = (t) => {
                const head = document.getElementsByTagName('head')[0];
                stylesheetNode = document.createElement('link');
                stylesheetNode.id = NODE_ID;
                stylesheetNode.rel = 'stylesheet';
                stylesheetNode.type = 'text/css';
                stylesheetNode.media = 'all';
                head.appendChild(stylesheetNode);
                linkCssFile(t);
            }

            const buildStylesheetPath = (t) => {
                return `./css/themes/theme-${t}.css`;;
            }

            const getPreferredTheme = () => {
                if (them != null) {
                    return them;
                }
                const fallbackTheme = defaultTheme || Themes.LIGHT;
                return GUI.isDarkModePreferred() ? Themes.DARK : fallbackTheme;
            }

            const init = () => {
                theme = getPreferredTheme();
                initThemeStylesheetNode(theme);
            }

            init();

        }


        function LayoutManager(defaultOrientation) {

            let orientation;

            //////////////////////////////////////////////////////////////////
            // API
            //////////////////////////////////////////////////////////////////

            this.switchLayout = () => {
                const layoutOrientation = GUI.switchAxis(splitPane.getOrientation());
                this.setLayoutOrientation(layoutOrientation);
            }

            this.getAngle = (orientation) => {
                return (orientation === Axis.X) ? 0 : 90;
            }

            this.onResize = () => {
                const isSmall = shouldBreak();
                const currentOrientation = splitPane.getOrientation();
                if (isSmall && currentOrientation === Axis.X) {
                    this.setLayoutOrientation(Axis.Y);
                } else if (!isSmall && currentOrientation === Axis.Y) {
                    this.setLayoutOrientation(Settings.DEFAULT_LAYOUT_ORIENTATION);
                }
                toolbar.setLayout(GUI.getScreenSize());
            }

            this.setLayoutOrientation = (axis) => {
                splitPane.setOrientation(axis);
                renderer.setOrientation(axis);
                switchLayoutButton.rotateIcon(this.getAngle(axis));
                orientation = axis;
            }

            this.getOrientation = () => {
                return orientation;
            }

            this.hideEditor = () => { splitPane.expand(false); }

            this.hideRenderer = () => { splitPane.expand(true); }

            this.restoreComponents = () => { splitPane.expand(null); }

            this.initLayout = () => {
                this.setLayoutOrientation(orientation);
            }

            //////////////////////////////////////////////////////////////////
            // Internal
            //////////////////////////////////////////////////////////////////

            const shouldBreak = () => {
                return (GUI.getScreenSize() <= Settings.LAYOUT_BREAKPOINT);
            }

            const init = () => {
                orientation = shouldBreak() ? Axis.Y : defaultOrientation;
            }

            init();

        }

    }

}

window.onload = editjs.init;
