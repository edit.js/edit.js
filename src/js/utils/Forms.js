/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const Forms = {

    createFormatPicker: (tip, cssClass, callback = () => {}) => {
        return new FormatPicker(Settings.DEFAULT_FORMAT, tip, cssClass);
    },

    createCsvSeparatorField: (callback = () => {}) => {
        const description = 'Defines the character to use as a placeholder for empty cells';
        return new TextField('Separator', Settings.DEFAULT_CSV_SEPARATOR, description)
            .registerCallback(callback);
    },

    createLatexPlaceholderField: (callback = () => {}) => {
        const description = 'Defines the character to use as a placeholder for empty cells';
         return new TextField('Empty Cell', Settings.DEFAULT_LATEX_PLACEHOLDER, description)
             .registerCallback(callback);
    },

    createJavaDataStructureCombobox: (callback = () => {}) => {
        const description = 'Defines the type of data structure to generate';
        return new Combobox('Data structure', JavaFormat, JavaFormat.ARRAY_2D, description)
            .registerCallback(callback);
    },

    createCSharpDataStructureCombobox: (callback = () => {}) => {
        const description = 'Defines the type of data structure to generate';
        return new Combobox('Data structure', CSharpFormat, CSharpFormat.ARRAY_2D, description)
            .registerCallback(callback);
    },

    createPhpFormatCombobox: (callback = () => {}) => {
        const description = 'Defines the type of data structure to generate';
        return new Combobox('Data structure', PhpFormat, PhpFormat.ARRAY_2D, description)
            .registerCallback(callback);
    },

    createPythonFormatCombobox: (callback = () => {}) => {
        const description = 'Defines the type of data structure to generate';
        return new Combobox('Data structure', PythonFormat, PythonFormat.ARRAY_2D, description)
            .registerCallback(callback);
    },

    createJsonOptionCombobox: (callback = () => {}) => {
        const description = 'Defines the type of array to generate';
        return new Combobox('Data structure', JsonFormat, JsonFormat.ARRAY_2D, description)
            .registerCallback(callback);
    },
    
    createCommentCombobox: (callback = () => {}) => {
        const description = 'Generates the table as a programming comment';
        return new Combobox('Comment Type', CommentType, CommentType.NONE, description)
            .registerCallback(callback);
    },
    
    createOutputCombobox: (callback = () => {}) => {
        const description = 'Minifies or prettifies the generated table';
        return new Combobox('Output', OutputPolicy, OutputPolicy.PRETTIFY, description)
            .registerCallback(callback);
    },
    
    createHeaderCombobox: (callback = () => {}) => {
        const description = 'Configures the table header';
        return new Combobox('Header Orientation', HeaderOrientation, HeaderOrientation.FIRST_ROW, description)
            .registerCallback(callback);
    },
    
    createHeaderAlignmentCombobox: (callback = () => {}) => {
        const description = 'Defines the header cells text-alignment';
        return new Combobox('Header Alignment', Alignment, Alignment.LEFT, description)
            .registerCallback(callback);
    },
    
    createHeaderCaseCombobox: (callback = () => {}) => {
        const description = 'Defines the header cells case';
        return new Combobox('Header Case', TextCase, TextCase.SENTENCE, description)
            .registerCallback(callback);
    },
    
    createRowBorderCombobox: (callback = () => {}) => {
        const description = 'Configures the table borders';
        return new Combobox('Horizontal Lines', BorderPolicy, BorderPolicy.FIRST, description)
            .registerCallback(callback);
    },
    
    createColumnBorderCombobox: (callback = () => {}) => {
        const description = 'Configures the table borders';
        return new Combobox('Vertical Lines', BorderPolicy, BorderPolicy.EVERY, description)
            .registerCallback(callback);
    }

}
