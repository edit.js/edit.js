/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const ArrayUtils = {

    swap: (arr, a, b) => {
        const tmp = arr[a];
        arr[a] = arr[b];
        arr[b] = tmp;
    },

    insert: (arr, index, element) => {
        if (index === -1) {
            arr.push(element);
        } else {
            arr.splice(index, 0, element);
        }
    },

    /**
     * Returns the index of the first string matching this regex in the specified array.
     *
     * @param {string[]} arr  the array to process
     * @param {RegExp} reg  the regex to match to match the elements in this array to
     * @return {number} the index of the first match, or -1 if no match was found
     */
    indexOfMatch: (arr, reg) => {
        for (let i = 0; i < arr.length; i++) {
            if (arr[i].match(reg)) {
                return i;
            }
        }
        return -1;
    },

    /**
     * Returns the indices of every occurrence of the specified element in this array.
     *
     * @param {any[]} arr  the array to process
     * @param {any} element  the element to look for
     * return {number[]} the indices of every occurrence of the specified element in this array.
     */
    indicesOf: (arr, element) => {
        const indices = [];
        let i = -1;
        while ((i = arr.indexOf(element, i + 1)) > -1) {
            indices.push(i);
        }
        return indices;
    },

    contains: (arr, element) => {
        return arr.indexOf(element) >= 0;
    },

    isEmpty: (arr) => {
        return (arr.length === 0);
    },

    /**
     * Returns true if a and b contain the same elements, ignoring their order of
     * appearance in their respective array; false otherwise.
     *
     * @param a  an array of primitive values
     * @param b  an array of primitive values
     * @return true if a and b contain the same elements, ignoring their order of
     * appearance in their respective array; false otherwise
     */
    equalsIgnoreOrder: (a, b) => {
        for (let element of a) {
            if (b.indexOf(element) < 0) {
                return false;
            }
        }
        return (a.length === b.length);
    },

    convert2dArrayToArrayOfObjects: (arr) => {
        const propertyNames = arr.splice(0, 1)[0];
        const result = [];
        for (let i = 0; i < arr.length; i++) {
            const obj = {}
            for (let j = 0; j < propertyNames.length; j++) {
                const propertyName = propertyNames[j];
                obj[propertyName] = arr[i][j];
            }
            result.push(obj);
        }
        return result;
    },

    convertArrayOfObjectsTo2dArray: (arr) => {
        const propertyNames = Object.keys(arr[0]);
        const result = [propertyNames];
        for (let obj of arr) {
            const row = [];
            for (let prop of propertyNames) {
                row.push(obj[prop]);
            }
            result.push(row);
        }
        return result;
    },

    /**
     * Generates a new array of length n, containing n times the specified element.
     *
     * @param element  the element to populate the array of length n with
     * @param {number} n  the length of the array to generate
     * @return {any[]}
     */
    repeat: (element, n) => {
        return new Array(n).fill(element);
    },

    /**
     * Returns a sequential ordered array from startInclusive to endExclusive by
     * an incremental step of 1.
     *
     * @param {number} startInclusive  the lower bound (inclusive)
     * @param {number} endExclusive  the higher bound (exclusive)
     * @return {number[]} an array of integers from startInclusive to endExclusive
     */
    range: (startInclusive, endExclusive) => {
        const arr = [];
        for (let i = startInclusive; i < endExclusive; i++) {
            arr.push(i);
        }
        return arr;
    },

    /**
     * Gets the last element from this array, or the specified default value if
     * the array is empty
     *
     * @param {any[]} arr  the array to fetch the last element from
     * @param {any|undefined} defaultVal  (optional) the value to return if the array is empty
     * @return {any} the last element from this array, or the specified default value
     * if the array is empty
     * @throws an error if the array is empty and no default value is specified
     */
    getLast: (arr, defaultVal = undefined) => {
        if (ArrayUtils.isEmpty(arr)) {
            if (defaultVal !== undefined) {
                return defaultVal;
            } else {
                throw new Error('Can not get last element from empty array');
            }
        }
        return arr[arr.length - 1];
    },

    /**
     * Determines the most frequent element in this array.
     *
     * @param {any[]} arr  the array of elements to process
     * returns one of these elements if true; null otherwise
     * @return {any} the element with the highest number of occurrences in this array, or
     * null if the array is empty
     * @throws an error if any element in this array is not a primitive and does not
     * override displayName
     */
    mostFrequent: (arr) => {
        if (arr.length === 0) {
            return null;
        } else if (arr.length === 1) {
            return arr[0];
        } else {
            const cache = {};
            let max = 0;
            let item;
            arr.forEach(e => {
                const key = hash(e);
                if (!cache.hasOwnProperty(key)) {
                    cache[key] = 1;
                    item = e;
                } else {
                    cache[key]++;
                    if (cache[key] > max) {
                        max = cache[key];
                        item = e;
                    }
                }
            });
            return item;
        }
        function hash(element) {
            if (isPrimitive(element)) {
                return '' + element;
            }
            if (!element.hasOwnProperty('displayName')) {
                throw new Error('Specified element does not override displayName: ' + element);
            }
            return element.displayName;
        }
    },

    indexOfFirstNonEmptyCell: (arr) => {
        for (let i = 0; i < arr.length; i++) {
            if (!StringUtils.isBlank(arr[i])) {
                return i;
            }
        }
        return 0;
    },

    indexOfLastNonEmptyCell: (arr) => {
        const lastIndex = arr.length - 1;
        for (let i = lastIndex; i >= 0; i--) {
            if (!StringUtils.isBlank(arr[i])) {
                return i;
            }
        }
        return lastIndex;
    },

    /**
     * Returns the number of elements in this array which match the specified regex.
     *
     * @param {string[]} arr  the array to process
     * @param {RegExp} reg  the regex to match the elements in this array to
     * @return {number}
     */
    countOccurrences: (arr, reg) => {
        let occurrences = 0;
        arr.forEach(e => {
            if (e.match(reg)) {
                occurrences++;
            }
        });
        return occurrences;
    }

}
