/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const MatrixUtils = {

    /**
     * Returns the dimension of this 2d array as a {@link Vector}.
     *
     * @param {[][]} data  the 2d array to measure
     * @return {Vector}
     */
    getDimension: (data) => {
        const h = data.length;
        const w = (h === 0) ? 0 : data[0].length;
        return new Vector(w, h);
    },

    /**
     * Sorts the specified array by descending order.
     * This method accepts two-dimensional arrays, arrays of strings included.
     *
     * @param {string[]} arr  an array of strings to sort by the length of its elements
     * @return {string[]} an array containing the elements of the original array, sorted
     * by ascending order
     */
    sortByLengthDescending: (arr) => {
        return [... arr].sort((a, b) => {
            return b.length - a.length;
        });
    },

    /**
     * Gets the longest row of this 2d array.
     *
     * @param data  the 2d array to process
     * @return the longest row of the matrix
     */
    getLargestRow: (data) => {
      return MatrixUtils.sortByLengthDescending(data)[0];
    },

    /**
     * Gets the longest {@code String} in the specified {@code List}.
     *
     * @param list  the list to search in
     * @param except  if defined, ensures this element is excluded from the possible
     * results
     * @return the longest element in this list
     */
    getLongestElement: (list, except) => {
        if (except != null) {
            const length = except.length;
            const filtered = list.filter(e => e !== except);
            return StringUtils.max(length, MatrixUtils.sortByLengthDescending(filtered)[0]);
        }
        return MatrixUtils.sortByLengthDescending(list)[0];
    },

    /**
     * Gets the smallest {@code String} in the specified {@code List}.
     *
     * @param list  the list to search in
     * @return the smallest element in this list
     */
    getSmallestElement: (list) => {
        return MatrixUtils.sortByLengthDescending(list)[list.length - 1];
    },

    /**
     * Finds the longest element of each column.
     * Note that all rows of the matrix must have the same length.
     *
     * @param {string[][]} data  the 2d array to query
     * @return an array containing the longest element of each column
     */
    getLongestElementPerColumn: function(data) {
        const longestElementPerColumn = [];
        const numberOfColumns = data[0].length;
        for (let i = 0; i < numberOfColumns; i++) {
            const columnData = this.getColumn(data, i);
            const longestElement = this.getLongestElement(columnData);
            longestElementPerColumn.push(longestElement);
        }
        return longestElementPerColumn;
    },

    getColumnWidths: function(matrix) {
        return this.getLongestElementPerColumn(matrix)
            .map(e => e.length);
    },

    /**
     * Replaces every {@code null} element in the specified {@code matrix} with an
     * empty {@code String}.
     * This method also ensures that all columns of the resulting matrix have the
     * same length.
     *
     * @param {string[][]} data  the 2d array to process
     * @param {string?} placeholder  (optional) the string to replace the null cells with
     * @return a null-safe two-dimensional array
     */
    nullSafe: function(data, placeholder = '') {
        const nullSafeData = [];
        const largestRow = this.getLargestRow(data);
        for (let row of data) {
            const nullSafeRow = [];
            for (let i = 0; i < largestRow.length; i++) {
                let content = placeholder;
                if (i < row.length) {
                    const cell = row[i];
                    if (cell != null) {
                        content = cell;
                    }
                }
                nullSafeRow.push(content);
            }
            nullSafeData.push(nullSafeRow);
        }
        return nullSafeData;
    },

    /**
     * Gets the list of elements in the column at specified {@code columnIndex}.
     *
     * @param data  the 2d array to query
     * @param columnIndex  the index of the column containing the elements
     * @return a {@code List}
     */
    getColumn: (data, columnIndex) => {
        const column = [];
        for (let datum of data) {
            const cell = datum[columnIndex];
            column.push(cell);
        }
        return column;
    },

    getRow: (data, rowIndex) => {
        return data[rowIndex];
    },

    get: (data, index, axis) => {
        return (axis === Axis.X) ? MatrixUtils.getRow(data, index) : MatrixUtils.getColumn(data, index);
    },

    /**
     * Rotates the specified matrix by 90° without mutating the original
     * array.
     *
     * @param {any[][]} data  the 2d array to rotate
     */
    rotate: (data) => {
        const rotated = [];
        const width = data[0].length;
        for (let i = 0; i < width; i++) {
            rotated.push(MatrixUtils.getColumn(data, i));
        }
        return rotated;
    },

    /**
     * Adds null elements to smaller rows to ensure all rows have
     * the same length.
     *
     * @param data  the 2d array to mutate
     */
    square: (data) => {
        const largestRow = MatrixUtils.getLargestRow(data);
        for (let i = 0; i < data.length; i++) {
            const row = data[i];
            const diff = largestRow.length - row.length;
            for (let j = 0; j < diff; j++) {
                row.push(null);
            }
        }
        return data;
    },

    /**
     * Returns true if this two-dimensional array is empty or only contains blank strings.
     *
     * @param {string[][]} data  the 2d array to process
     * @return {boolean}
     */
    isBlank: (data) => {
        for (let y = 0; y < data.length; y++) {
            const row = data[y];
            for (let x = 0; x < row.length; x++) {
                if (!StringUtils.isBlank(row[x])) {
                    return false;
                }
            }
        }
        return true;
    },

    /**
     * Returns a shallow copy of this two-dimensional array.
     *
     * @param {string[][]} data  the 2d array to clone
     */
    clone: (data) => {
        return data.map(row => [...row]);
    }

}
