/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A utility class providing convenient methods for generating or mutating
 * {@link Component} objects or nodes.
 *
 * Unless specified otherwise, all methods return a {@link Component} object
 */
const GUI = {

    dockable: (draggable, dockable, onLocationChange = (a, b) => {}, onDock = (a, b) => {}) => {
        const DEFAULT_SIDE = Side.BOTTOM;
        const getLastDropSide = (component) => {
            const sides = Object.values(Side);
            for (let side of sides) {
                if (component.hasClass(side)) {
                    return side;
                }
            }
            return DEFAULT_SIDE;
        }
        const preview = new Component()
            .setUI(document.getElementById('dock-area'))
            .setVisible(false);
        dockable.addClass('dockable');
        // cache
        const zIndex = draggable.style.zIndex;
        const screenDimension = GUI.getVisibleScreenDimension();
        const screenCenter = screenDimension.div(2);
        // state
        let side = DEFAULT_SIDE;
        let lastDropSide = getLastDropSide(dockable);
        // functions
        const getSide = (x, y) => {
            const delta = new Vector(x, y).sub(screenCenter);
            const deltaAbs = delta.abs();
            const ratioX = deltaAbs.getX() / screenCenter.getX();
            const ratioY = deltaAbs.getY() / screenCenter.getY();
            const max = Math.max(ratioX, ratioY);
            if (max === ratioX) {
                return (delta.getX() < 0) ? Side.LEFT : Side.RIGHT;
            } else return (delta.getY() < 0) ? Side.TOP : Side.BOTTOM;
        }
        const dock = (side, element) => { element.addClass(side); }
        const locationChange = function(compLocation, cursorLocation) {
            draggable.style.zIndex = '9999';
            preview.setVisible(true);
            preview.putStyle('zIndex', '1');
            preview.removeClass(side);
            side = getSide(cursorLocation.getX(), cursorLocation.getY());
            dock(side, preview);
            onLocationChange(compLocation, cursorLocation);
        }
        const drop = function(compLocation, cursorLocation) {
            preview.setVisible(false);
            draggable.style.zIndex = zIndex;
            draggable.style.removeProperty("left");
            draggable.style.removeProperty("top");
            dockable.removeClass(lastDropSide);
            dock(side, dockable);
            lastDropSide = side;
            onDock(compLocation, cursorLocation);
        };
        GUI.draggable(draggable, false, locationChange, drop, () => {});
    },

    /**
     * Makes the specified element draggable so the user can click
     * it and move it around.
     *
     * @param {HTMLElement} node  a document node
     * @param {boolean} absolute  true if the node must be dragged using absolute coordinates,
     *                            false if it should be dragged relatively to its parent node
     * @param {function(Vector, Vector): void} onLocationChange  an optional callback
     *                                         to execute each time the location of the
     *                                         draggable node changes
     * @param {function(Vector, Vector): void} onDrop  an optional callback to execute
     *                                         when the draggable item is dropped
     * @param {function(): void} onGrab  an optional callback to execute when the draggable
     *                                   item is grabbed
     * @param {Axis=} [axis]  an optional {@link Axis} constant used to restrict the drag
     *                                    direction. If no axis is specified, the element
     *                                    can be dragged in any direction
     * @param {Vector=} [limitMin]  the optional lower limit
     * @param {Vector=} [limitMax]  the optional upper limit
     */
    draggable: (
        node,
        absolute = false,
        onLocationChange = (x, y) => {},
        onDrop = (x, y) => {},
        onGrab = () => {},
        axis = null,
        limitMin = null,
        limitMax = null
    ) => {
        // Cache node properties
        const transition = node.style.transition;
        const position = node.style.position;
        // Cache
        const horizontal = (axis === null) || (axis === Axis.X);
        const vertical = (axis === null) || (axis === Axis.Y);
        let delta = new Vector(0, 0);
        let lastCursorCoordinate = new Vector(0, 0);
        function exceedsLimitMin(limitMin, ratioX, ratioY) {
            return (limitMin !== null) && ((limitMin.getX() > ratioX && horizontal) || (limitMin.getY() > ratioY && vertical));
        }
        function exceedsLimitMax(limitMax, ratioX, ratioY) {
            return (limitMax !== null) && ((limitMax.getX() < ratioX && horizontal) || (limitMax.getY() < ratioY && vertical));
        }
        // Callbacks
        const onMousemove = (e) => {
            const maxWidth = absolute ? window.innerWidth : node.parentNode.offsetWidth;
            const maxHeight = absolute ? window.innerHeight : node.parentNode.offsetHeight;
            const currentCursorCoordinate = KeyEventProcessor.toPhysicalCoordinate(e);
            delta = lastCursorCoordinate.sub(currentCursorCoordinate)
            const x = horizontal ? node.offsetLeft - delta.getX() : 0;
            const y = vertical ? node.offsetTop - delta.getY() : 0;
            const ratioX = x / maxWidth * 100;
            const ratioY = y / maxHeight * 100;
            if (exceedsLimitMin(limitMin, ratioX, ratioY) || exceedsLimitMax(limitMax, ratioX, ratioY)) {
                return;
            }
            lastCursorCoordinate = currentCursorCoordinate;
            node.style.left = ratioX + '%';
            node.style.top = ratioY + '%';
            onLocationChange(new Vector(x, y), currentCursorCoordinate);
        }
        const onMousedown = (e) => {
            if (e.target.classList.contains('clickable')) {
                return;
            }
            node.style.position = 'absolute';
            node.style.transition = '0s';
            node.classList.add('dragged');
            lastCursorCoordinate = new Vector(e.clientX, e.clientY);
            document.onmouseup = onMouseup;
            document.onmousemove = onMousemove;
            // Prevent text selection while dragging
            e.preventDefault();
            onGrab();
        }
        const onMouseup = (e) => {
            node.style.transition = transition;
            node.style.position = position;
            node.classList.remove('dragged');
            document.onmouseup = null;
            document.onmousemove = null;
            onDrop(new Vector(e.clientX, e.clientY), lastCursorCoordinate);
        }
        node.onmousedown = onMousedown;
    },

    /**
     * Returns the portion of the screen which is currently occupied by the document.
     *
     * @return a {@link Vector} object
     */
    getVisibleScreenDimension: () => {
        return new Vector(GUI.getVisibleScreenWidth(), GUI.getVisibleScreenHeight());
    },

    // Uses the following StackOverflow answer:
    // Thanks to JCOC611:
    // https://stackoverflow.com/questions/4987309/screen-width-vs-visible-portion
    getVisibleScreenWidth: () => {
        return window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth
            || 0;
    },

    // Uses the following StackOverflow answer:
    // Thanks to JCOC611:
    // https://stackoverflow.com/questions/4987309/screen-width-vs-visible-portion
    getVisibleScreenHeight: () => {
        return window.innerHeight
            || document.documentElement.clientHeight
            || document.body.clientHeight
            || 0;
    },

    /**
     * Returns a {@link ScreenSize} constant matching the screen visible dimension.
     *
     * @return {ScreenSize}
     */
    getScreenSize: () => {
        const dimension = GUI.getVisibleScreenDimension();
        for (const name in ScreenSize) {
            if (ScreenSize.hasOwnProperty(name)) {
                const enumConstant = ScreenSize[name];
                if (enumConstant > dimension.getX()) {
                    return enumConstant;
                }
            }
        }
        return ScreenSize.XL;
    },

    ppAction: (action) => {
        return action.getName() + ' ' + GUI.ppKeyStroke(action);
    },

    ppKeyStroke: (action) => {
        return `(${action.getKeyStroke().toString()})`;
    },

    toDirection: (axis, dir) => {
        if (axis === Axis.X) {
            return (dir < 0) ? Direction.LEFT : Direction.RIGHT;
        } else {
            return (dir < 0) ? Direction.UP : Direction.DOWN;
        }
    },

    toCssClass: (axis) => {
        return GUI.isHorizontal(axis) ? 'horizontal' : 'vertical';
    },

    switchAxis: (axis) => {
        return GUI.isHorizontal(axis) ? Axis.Y : Axis.X;
    },

    isHorizontal: (axis) => {
        return (axis === Axis.X);
    },

    isDarkModePreferred: () => {
        // Uses the following StackOverflow answer:
        // Thanks to Mark Szabo:
        // https://stackoverflow.com/questions/56393880/how-do-i-detect-dark-mode-using-javascript
        return (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches);
    }
    
};
