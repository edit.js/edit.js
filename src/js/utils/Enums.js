/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const Enums = {

    /**
     * Returns the enum constant to which the specified key is mapped, or null
     * if this enum contains no mapping for the key.
     * The look is case-insensitive.
     *
     * @param {any} enumObject  the enum to lookup
     * @param {string} keyName  the key whose associated value is to be returned
     * @return {any} the enum constant matching the specified key if any is found; null otherwise
     */
    lookup: (enumObject, keyName) => {
        let enumConstant = null;
        for (const key in enumObject) {
            if (enumObject.hasOwnProperty(key)) {
                if (key.toLowerCase() === keyName.toLowerCase()) {
                    enumConstant = enumObject[key];
                }
            }
        }
        return enumConstant;
    }

};
