/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A utility class providing methods for apply lambdas to javascript objects.
 * Most of the methods provided by this class are inspired by the ECMAScript
 * Array.prototype API.
 */
const ObjectUtils = {

    /**
     * Creates a new object populated with the results of calling the specified function
     * on every element in this object.
     *
     * @param object  the object to apply the map function to
     * @param fn  a function with the following signature: (key, value) => E, where E is
     * the result of applying the map function on the value
     * @return a new object populated with the results of calling the specified function
     * on every element in this object.
     */
    map: (object, fn) => {
        const result = {};
        for (const key in object) {
            if (object.hasOwnProperty(key)) {
                result[key] = fn(key, object[key]);
            }
        }
        return result;
    },

    /**
     * Executes the specified function once for each property in this object.
     *
     * @param object  the object to apply the forEach function to
     * @param fn  a function with the following signature: (key, value) => void
     */
    forEach: (object, fn) => {
        for (const key in object) {
            if (object.hasOwnProperty(key)) {
                fn(key, object[key]);
            }
        }
    },

    /**
     * Returns a new object populated with the properties of this object that pass the test
     * implemented by the specified predicate.
     *
     * @param object  the object to apply the filter function to
     * @param predicate  a function taking a property key and value as parameters,
     *                   and returning a boolean
     * @return a new object populated with the properties of this object that pass the test
     * implemented by the specified predicate.
     */
    filter: (object, predicate) => {
        const result = {};
        for (const key in object) {
            if (object.hasOwnProperty(key)) {
                const value = object[key];
                if (predicate(key, value)) {
                    result[key] = value;
                }
            }
        }
        return result;
    },

    /**
     * Returns a unique element that pass the test implement by the specified predicate,
     * or null if the filter results are empty.
     *
     * @param object  the object to apply the findUnique function to
     * @param predicate  a function with the following signature: (key, value) => boolean
     * @return the element in this object which match the specified predicate
     * @throws an error if the provided predicate match more than one element in this object
     */
    findUnique: (object, predicate) => {
        const matches = ObjectUtils.filter(object, predicate);
        const count = ObjectUtils.length(matches);
        switch (count) {
            case 0:     return null;
            case 1:     return matches;
            default:    throw new Error('More than one element match specified predicate: ' + matches);
        }
    },

    /**
     * Returns whether at least one element in this object match the specified predicate.
     *
     * @param {object} object  the object to apply the anyMatch function to
     * @param {function(string, any): boolean} predicate  a function with the following signature: (key, value) => boolean
     * @return {boolean} true if any element in this object pass the test implemented by the
     * specified predicate; false otherwise
     */
    some: (object, predicate) => {
        for (const key in object) {
            if (object.hasOwnProperty(key)) {
                if (predicate(key, object[key])) {
                    return true;
                }
            }
        }
        return false;
    },

    /**
     * Returns whether all elements in this object match the specified predicate.
     *
     * @param object  the object to apply the allMatch function to
     * @param predicate  a function with the following signature: (key, value) => boolean
     * @return true if all elements in this object pass the test implemented by the
     * specified predicate; false otherwise
     */
    allMatch: (object, predicate) => {
        return (ObjectUtils.length(ObjectUtils.filter(object, predicate)) === ObjectUtils.length(object));
    },

    /**
     * Returns the number of properties inside this object.
     *
     * @param object  the object to count the properties from
     * @return the number of properties inside this object
     */
    length: (object) => {
        return Object.values(object).length;
    }

}
