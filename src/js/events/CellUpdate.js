/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * Represents a value change in a {@link Matrix} or {@link Table} cell.
 *
 * @param {number} x  the index of the column where the event occurred
 * @param {number} y  the index of the row where the event occurred
 * @param {*} previousValue  the cell value before the event occurred
 * @param {*} newValue  the cell value after the event occurred
 */
const CellUpdate = function(x, y, previousValue, newValue) {

    this.getY = () => {
        return y;
    };

    this.getX = () => {
        return x;
    };

    this.getCoordinate = () => {
        return new Vector(x, y);
    }

    this.getPreviousValue = () => {
        return previousValue;
    };

    this.getNewValue = () => {
        return newValue;
    };

    this.toString = () => {
        return '[CellUpdate] (' + y + ', ' + x + '): ' + previousValue + ' => ' + newValue;
    }

}
