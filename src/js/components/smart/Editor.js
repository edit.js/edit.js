/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Component} provides controls to edit, clear, and reset a {@link EditorTable}.
 * Every operation performed on the table is saved to the history so it can
 * be undone or redone later.
 *
 * @param {EditorTable} table  the {@link EditorTable} instance to control
 * @param {CommandHistory} history  the {@link CommandHistory} instance to use for storing and executing
 *                                 commands
 * @param {ExpandButton} expandBtn  the button responsible for expanding this editor
 * @param {EventProcessor} eventProcessor  the {@link EventProcessor} to use for processing internal
 *                        key events
 */
const Editor = function(table, history, eventProcessor, expandBtn) {

    Component.call(this);

    const BUTTONS_MARGIN = 4;
    const TABLE_OFFSET = 80;

    const listeners = [];
    // Cache
    let currentCoordinate;
    // UI
    let columnOptionsPopOver;
    let rowOptionsPopOver;
    // Internal events
    const UIActions = {
        MOVE_ROW_UP:        new Action('Move Row Up',           Context.EDITOR, new KeyStroke(Key.UP,     Modifier.ALT, Modifier.SHIFT),  (e, c) => moveRowRequest(e, c)),
        MOVE_ROW_DOWN:      new Action('Move Row Down',         Context.EDITOR, new KeyStroke(Key.DOWN,   Modifier.ALT, Modifier.SHIFT),  (e, c) => moveRowRequest(e, c)),
        MOVE_COLUMN_LEFT:   new Action('Move Column Left',      Context.EDITOR, new KeyStroke(Key.LEFT,   Modifier.ALT, Modifier.SHIFT),  (e, c) => moveColumnRequest(e, c)),
        MOVE_COLUMN_RIGHT:  new Action('Move Column Right',     Context.EDITOR, new KeyStroke(Key.RIGHT,  Modifier.ALT, Modifier.SHIFT),  (e, c) => moveColumnRequest(e, c)),
        INSERT_ROW:         new Action('Insert Row Before',     Context.EDITOR, new KeyStroke(Key.ENTER),                                 (e, c) => insertRowAfterRequest(e, c)),
        INSERT_COLUMN:      new Action('Insert Column Before',  Context.EDITOR, new KeyStroke(Key.ENTER,  Modifier.ALT),                  (e, c) => insertColumnAfterRequest(e, c)),
        DUPLICATE_ROW:      new Action('Duplicate Row',         Context.EDITOR, new KeyStroke(Key.DOWN,   Modifier.CTRL, Modifier.SHIFT), (e, c) => duplicateRowRequest(e, c)),
        DUPLICATE_COLUMN:   new Action('Duplicate Column',      Context.EDITOR, new KeyStroke(Key.RIGHT,  Modifier.CTRL, Modifier.SHIFT), (e, c) => duplicateColumnRequest(e, c)),
        DELETE_ROW:         new Action('Delete Row',            Context.EDITOR, new KeyStroke(Key.D,      Modifier.CTRL),                 (e, c) => removeRowRequest(e, c)),
        DELETE_COLUMN:      new Action('Delete Column',         Context.EDITOR, new KeyStroke(Key.D,      Modifier.ALT),                  (e, c) => removeColumnRequest(e, c))
    }
    const actions = [
        new Action('Move Cursor Up',    Context.EDITOR, new KeyStroke(Key.UP,     Modifier.ALT), (e, c) => moveCursorRequest(e, c)),
        new Action('Move Cursor Left',  Context.EDITOR, new KeyStroke(Key.LEFT,   Modifier.ALT), (e, c) => moveCursorRequest(e, c)),
        new Action('Move Cursor Down',  Context.EDITOR, new KeyStroke(Key.DOWN,   Modifier.ALT), (e, c) => moveCursorRequest(e, c)),
        new Action('Move Cursor Right', Context.EDITOR, new KeyStroke(Key.RIGHT,  Modifier.ALT), (e, c) => moveCursorRequest(e, c)),
        UIActions.MOVE_ROW_UP,
        UIActions.MOVE_ROW_DOWN,
        UIActions.MOVE_COLUMN_LEFT,
        UIActions.MOVE_COLUMN_RIGHT,
        UIActions.INSERT_ROW,
        UIActions.INSERT_COLUMN,
        UIActions.DUPLICATE_ROW,
        UIActions.DUPLICATE_COLUMN,
        UIActions.DELETE_ROW,
        UIActions.DELETE_COLUMN
    ];

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.clear = () => {
        execute(new ClearCommand(table, table.getModel()));
        notifyReset();
    };

    /**
     * Resets the table.
     * If no dimension is specified, resets the table to its initial dimension.
     *
     * @param w  the new table width (optional)
     * @param h  the new table height (optional)
     */
    this.reset = (w, h) => {
        execute(new ResetCommand(table, table.getModel(), w, h));
        notifyReset();
    };

    this.insertColumn = (x) => {
        insert(x, Axis.X);
    }

    this.insertRow = (y) => {
        insert(y, Axis.Y);
    }

    this.removeColumn = (x) => {
        remove(x, Axis.X, -1);
    }

    this.removeRow = (y) => {
        remove(y, Axis.Y, -1);
    }

    this.transpose = () => {
        const model = table.getModel();
        const rotated = MatrixUtils.rotate(model.getData());
        execute(new ImportCommand(table, table.getModel(), new TableModel([], rotated)));
    };

    this.addTableEditListener = (listener) => {
        listeners.push(listener);
    };

    this.onTableStructureChange = (model) => {
        notifyListeners(model);
        updateButtonFromModel(model);
    }

    this.onCellValueChange = (cellUpdate) => {
        history.save(new InputCommand(table, cellUpdate));
        notifyListeners(table.getModel());
    }

    this.onCellActive = (activeCellEvent) => {
        currentCoordinate = activeCellEvent.getLogicalCoordinate();
        const physicalCoordinate = activeCellEvent.getPhysicalCoordinate();
        const columnOptionsCoordinate = new Vector(
            physicalCoordinate.getX() - TABLE_OFFSET,
            table.getUI().offsetTop
        );
        const rowOptionsCoordinate = new Vector(
            table.getUI().offsetLeft,
            physicalCoordinate.getY() - TABLE_OFFSET * 2 + 25
        );
        columnOptionsPopOver.popOver(columnOptionsCoordinate);
        rowOptionsPopOver.popOver(rowOptionsCoordinate);
    }

    this.onModelImport = (tableModel) => {
        execute(new ImportCommand(table, table.getModel(), tableModel));
    }

    const updateButtonFromModel = (model) => {
        const d = model.getDimension();
        updateButtonFromDimension(d.getX(), d.getY());
    }

    const updateButtonFromDimension = (x, y) => {
        const str = x + ' x ' + y;
        expandBtn.setTitle(`Editor (${str})`);
    }

    this.onTableCreate = (coordinate) => {
        this.reset(coordinate.getX(), coordinate.getY());
    };

    this.onActionRequest = (actionRequest) => {
        switch (actionRequest) {
            case ActionRequest.TRANSPOSE:
                this.transpose();
                break;
            case ActionRequest.REDO:
                history.redo();
                break;
            case ActionRequest.UNDO:
                history.undo();
                break;
            case ActionRequest.CLEAR:
                this.clear();
                break;
            case ActionRequest.RESET:
                this.reset();
                break;
            default:
                throw new Error('Unsupported operation: ' + actionRequest);
        }
    }

    // Notified by Toolbar
    this.onRenderOptionsChange = (metadata) => {
        applyHeaderAlignment(metadata.getHeaderAlignment(), metadata.getHeaderOrientation());
    }

    this.processKeyEvent = (evt, coordinate) => {
        const action = eventProcessor.getAction(evt);
        if (action != null) {
            evt.preventDefault();
            action.process(evt, coordinate);
        }
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const execute = (command) => {
        command.execute();
        history.save(command);
    };

    const moveUp = () => {
        moveTo(Axis.Y, -1);
    };

    const moveLeft = () => {
        moveTo(Axis.X, -1);
    };

    const moveDown = () => {
        moveTo(Axis.Y, 1);
    };

    const moveRight = () => {
        moveTo(Axis.X, 1);
    };

    const moveTo = (axis, direction) => {
        const horizontal = (axis === Axis.X);
        const current = horizontal ? currentCoordinate.getX() : currentCoordinate.getY();
        const size = horizontal ? table.getWidth() : table.getHeight();
        if (current === size - direction || current + direction === -1) {
            table.select(currentCoordinate);
            return;
        }
        const newCoordinate = computeNewCoordinate(axis, direction);
        table.select(newCoordinate);
        execute(new SwapCommand(table, current + direction, current, axis));
    };

    const duplicateDown = () => {
        duplicate(Axis.Y, currentCoordinate.getY());
    }

    const duplicateRight = () => {
        duplicate(Axis.X, currentCoordinate.getX());
    }

    const duplicate = (axis, index) => {
        if (axis === Axis.Y) {
            execute(new DuplicateRowCommand(table, index));
        } else {
            execute(new DuplicateColumnCommand(table, index));
        }
    }

    const computeNewCoordinate = (axis, direction) => {
        const dir = GUI.toDirection(axis, direction);
        return currentCoordinate.add(dir);
    };

    const updateActiveCell = (axis, direction = 1) => {
        const newCoordinate = computeNewCoordinate(axis, direction);
        if (!table.contains(newCoordinate)) {
            hideSpreadSheetOptions();
        } else {
            table.select(newCoordinate);
        }
    };

    const insert = (index, axis) => {
        const horizontal = (axis === Axis.X);
        const current = horizontal ? currentCoordinate.getX() : currentCoordinate.getY();
        const command = horizontal ? new InsertColumnCommand(table, index) : new InsertRowCommand(table, index);
        execute(command);
        if (index === current) {
            updateActiveCell(axis);
        } else {
            table.select(currentCoordinate);
        }
    };

    const remove = (index, axis, direction) => {
        const horizontal = (axis === Axis.X);
        const size = horizontal ? table.getWidth() : table.getHeight();
        if (size > 1) {
            const command = horizontal ? new RemoveColumnCommand(table, index) : new RemoveRowCommand(table, index);
            execute(command);
            const current = horizontal ? currentCoordinate.getX() : currentCoordinate.getY();
            if (index === current) {
                updateActiveCell(axis, direction);
            } else if (table.contains(currentCoordinate)) {
                table.select(currentCoordinate);
            }
        }
    };

    const align = (alignment) => {
        const index = currentCoordinate.getX();
        const callback = () => notifyListeners(table.getModel());
        execute(new AlignCommand(table, index, alignment, callback));
    };

    const notifyListeners = (tableModel) => {
        if (table.isBlank()) {
            notifyReset();
            return;
        }
        listeners.forEach(l => l.onGenerate(tableModel));
    };

    const notifyReset = () => {
        listeners.forEach(l => l.onReset());
    };

    const onMouseLeave = () => {
        // hideSpreadSheetOptions();
    };

    const applyHeaderAlignment = (alignment, orientation) => {
        switch (orientation) {
            case HeaderOrientation.FIRST_ROW:
                table.alignHeader(Axis.X, alignment);
                break;
            case HeaderOrientation.FIRST_COLUMN:
                table.alignHeader(Axis.Y, alignment);
                break;
            case HeaderOrientation.NONE:
                // Fall through
            default:
                table.resetHorizontalHeader();
                table.resetVerticalHeader();
        }
    }

    const hideSpreadSheetOptions = () => {
        columnOptionsPopOver.setVisible(false);
        rowOptionsPopOver.setVisible(false);
    };

    const createTableContainer = () => {
        const rowButtons = createRowButtons('row-controls')
        const colButtons = createColButtons('col-controls')
        const wrapper = new Component()
            .add(table)
            .addClass('fill-parent');
        return new Component()
            .add(rowButtons)
            .add(colButtons)
            .add(wrapper)
            .addNode(createColumnOptions())
            .addNode(createRowOptions())
            .addClass('table-container')
            .putStyle('marginLeft', TABLE_OFFSET + 'px')
            .putStyle('marginTop', TABLE_OFFSET + 'px');
    };

    const createPopOver = (config) => {
        const popOver = new PopOver(config.orientation)
            .addListener('mouseover', config.mouseEnter, false)
            .addListener('mouseleave', config.mouseLeave, false);
        for (let i = 0; i < config.buttons.length; i++) {
            const buttonGroup = config.buttons[i];
            const container = new Component('span');
            for (let j = 0; j < buttonGroup.length; j++) {
                const buttonSpec = buttonGroup[j];
                const button = createTableButton(buttonSpec.text, buttonSpec.callback, buttonSpec.tip, buttonSpec.icon, buttonSpec.rotate);
                container.add(button);
                if (j === buttonGroup.length - 1) {
                    break;
                }
                button.putStyle(config.marginDirection, BUTTONS_MARGIN + 'px');
            }
            if (config.buttons.length > 1 && i < config.buttons.length - 1) {
                container.putStyle('paddingRight', '8px');
            }
            if (i > 0) {
                container.putStyle('paddingLeft', '8px');
                container.putStyle('borderLeft', '1px solid #ddd');
            }
            popOver.add(container);
        }
        return popOver;
    };

    const createRowOptions = () => {
        rowOptionsPopOver = createPopOver({
            buttons: [
                [
                    {text: '+',icon: null, callback: () => this.insertRow(currentCoordinate.getY()), tip: GUI.ppAction(UIActions.INSERT_ROW)},
                    {text: '', icon: Icons.BIN, callback: () => this.removeRow(currentCoordinate.getY()), tip: GUI.ppAction(UIActions.DELETE_ROW)},
                    {text: '', icon: Icons.MOVE_UP, callback: () => moveUp(), tip: GUI.ppAction(UIActions.MOVE_ROW_UP)},
                    {text: '', icon: Icons.MOVE_DOWN, callback: () => moveDown(), tip: GUI.ppAction(UIActions.MOVE_ROW_DOWN)},
                    {text: '', icon: Icons.DUPLICATE, callback: () => duplicateDown(), tip: GUI.ppAction(UIActions.DUPLICATE_ROW)},
                ]
            ],
            orientation: Axis.Y,
            marginDirection: 'marginBottom',
            mouseEnter: () => table.highlightRow(currentCoordinate.getY(), true),
            mouseLeave: () => table.highlightRow(currentCoordinate.getY(), false)
        });
        return rowOptionsPopOver.getUI();
    };

    const createColumnOptions = () => {
        columnOptionsPopOver = createPopOver({
            buttons: [
                [
                    {text: '+',icon: null, callback: () => this.insertColumn(currentCoordinate.getX()), tip: GUI.ppAction(UIActions.INSERT_COLUMN)},
                    {text: '', icon: Icons.BIN, callback: () => this.removeColumn(currentCoordinate.getX()), tip: GUI.ppAction(UIActions.DELETE_COLUMN)},
                    {text: '', icon: Icons.MOVE_DOWN, callback: () => moveLeft(), rotate: 90, tip: GUI.ppAction(UIActions.MOVE_COLUMN_LEFT)},
                    {text: '', icon: Icons.MOVE_UP, callback: () => moveRight(), rotate: 90, tip: GUI.ppAction(UIActions.MOVE_COLUMN_RIGHT)},
                    {text: '', icon: Icons.DUPLICATE, callback: () => duplicateRight(), tip: GUI.ppAction(UIActions.DUPLICATE_COLUMN)},
                ],
                [
                    {text: '', icon: Icons.ALIGN_LEFT, callback: () => align(Alignment.LEFT), tip: 'Align left'},
                    {text: '', icon: Icons.ALIGN_CENTER, callback: () => align(Alignment.CENTER), tip: 'Center text'},
                    {text: '', icon: Icons.ALIGN_RIGHT, callback: () => align(Alignment.RIGHT), tip: 'Align right'}
                ]
            ],
            orientation: Axis.X,
            marginDirection: 'marginRight',
            mouseEnter: () => table.highlightColumn(currentCoordinate.getX(), true),
            mouseLeave: () => table.highlightColumn(currentCoordinate.getX(), false)
        });
        return columnOptionsPopOver.getUI();
    };

    const createRowButtons = (cssClass) => {
        const removeRowBtn = createTableButton('-', () => this.removeRow(-1), 'Remove last row');
        const addRowBtn = createTableButton('+', () => this.insertRow(-1), 'Append new row')
            .putStyle('display', 'block')
            .putStyle('marginBottom', '2px');
        return new Component()
            .addClass(cssClass)
            .add(addRowBtn)
            .add(removeRowBtn);
    };

    const createColButtons = (cssClass) => {
        const addColBtn = createTableButton('+', () => this.insertColumn(-1), 'Append new column')
            .putStyle('marginRight', '2px');
        const removeColBtn = createTableButton('-', () => this.removeColumn(-1), 'Remove last column');
        return new Component()
            .addClass(cssClass)
            .add(addColBtn)
            .add(removeColBtn);
    };

    const createTableButton = (text, callback, tip, iconName, rotation) => {
        return new Button(text, callback, tip, ButtonStyle.FILLED)
            .withIcon(iconName, rotation)
            .addClass('style-square');
    };

    const initHtml = () => {
        this.add(createTableContainer());
        this.add(expandBtn);
        this.addListener('mouseleave', onMouseLeave, false);
        this.addClass('editor');
        this.putStyle('position', 'relative');
    };

    const init = () => {
        initHtml();
        updateButtonFromDimension(Settings.INITIAL_COLUMNS, Settings.INITIAL_ROWS);
        expandBtn.addTarget(this.getUI());
        eventProcessor.register(...actions);
        // All structure change notifications are delegated to editor listeners
        table.addStructureChangeListener(this);
        table.addCellEventListener(this);
        table.addKeyEventListener(this);
    };

    init();

    //////////////////////////////////////////////////////////////////
    // Event processing
    //////////////////////////////////////////////////////////////////

    const removeRowRequest = (e, coordinate) => {
        this.removeRow(coordinate.getY());
    }

    const removeColumnRequest = (e, coordinate) => {
        this.removeColumn(coordinate.getX());
    }

    const insertRowAfterRequest = (e, coordinate) => {
        this.insertRow(coordinate.getY() + 1);
    }

    const insertColumnAfterRequest = (e, coordinate) => {
        this.insertColumn(coordinate.getX() + 1);
    }

    const duplicateRowRequest = (e, coordinate) => {
        duplicate(Axis.Y, coordinate.getY());
    }

    const duplicateColumnRequest = (e, coordinate) => {
        duplicate(Axis.X, coordinate.getX());
    }

    const moveRowRequest = (e, coordinate) => {
        moveRequest(e, coordinate, (coordinate, target) => {
            execute(new SwapCommand(table, coordinate.getY(), target.getY(), Axis.Y));
            table.focus(target.getX(), target.getY());
        });
    }

    const moveColumnRequest = (e, coordinate) => {
        moveRequest(e, coordinate, (coordinate, target) => {
            execute(new SwapCommand(table, coordinate.getX(), target.getX(), Axis.X));
            table.focus(target.getX(), target.getY());
        });
    }

    const moveCursorRequest = (e, coordinate) => {
        moveRequest(e, coordinate, (coordinate, target) => table.focus(target.getX(), target.getY()));
    }

    const moveRequest = (e, coordinate, callback) => {
        let direction = KeyEventProcessor.toDirection(e.key);
        if (direction != null) {
            const target = coordinate.add(direction);
            if (table.contains(target)) {
                callback(coordinate, target);
            }
        }
    }

};
