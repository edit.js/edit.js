/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Component} is synchronized with the editor to display its state
 * in real time.
 *
 * @param {ExpandButton} expandBtn  an {@link ExpandButton} instance
 * @param {MessagePool} messagePool  a {@link MessagePool} instance
 * @param {function(TableModel): void} onPrefillRequest  the callback to execute on
 *                                                 prefill request
 * @param {Axis} orientation  the initial renderer orientation
 */
const TableRenderer = function(expandBtn, messagePool, onPrefillRequest, orientation) {

    Component.call(this);

    const MIN_CELLS = new Vector(2, 4);
    const MAX_CELLS = new Vector(4, 9);

    // Cache
    let model;
    let metadata;
    // UI
    let content;
    let placeholder;
    let htmlPreview;
    let htmlPreviewContainer;
    let numbers;
    // Config
    let syntaxHighlightingEnabled;
    // State
    let placeholderShowing;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.copy = () => {
        const text = this.getValue();
        if (!navigator.clipboard) {
            fallbackCopyTextToClipboard(text);
        } else {
            navigator.clipboard.writeText(text).then(() => {
            }, (err) => {
                messagePool.error('Could not copy to clipboard: ' + err);
            });
        }
        messagePool.success('Copied to clipboard');
    }

    this.setOrientation = (orientation) => {
        this.removeClass(GUI.toCssClass(orientation));
        this.addClass(GUI.toCssClass(GUI.switchAxis(orientation)));
    }

    this.toggleSyntaxHighlighting = () => {
        syntaxHighlightingEnabled = !syntaxHighlightingEnabled;
        this.generate(model, metadata);
    }

    this.isSyntaxHighlightingEnabled = () => {
        return syntaxHighlightingEnabled;
    }

    this.onRenderOptionsChange = (m) => {
        Logger.debug('Renderer', m.toString());
        metadata = m;
        updateButtonFromFormat(metadata.getFormat());
        this.generate(model, metadata);
    }

    this.onCopyAction = () => {
        this.copy();
    };

    /**
     * Serializes the specified data using specified {@link BorderPolicy}.
     *
     * @param {TableModel} tableModel  the table model to serialize
     * @param {TableMetadata} metadata  a {@link TableMetadata} object
     */
    this.generate = (tableModel, metadata) => {
        this.clear();
        if (tableModel.isEmpty() || metadata == null) {
            showPlaceholder(true);
            return;
        }
        showPlaceholder(false);
        const encoder = getEncoder(metadata);
        const serialized = encoder.encode(tableModel);
        const escaped = StringUtils.escapeHTMLChars(serialized);
        const highlighted = highlightSyntax(escaped, metadata);
        this.display(highlighted);
    }

    this.onGenerate = (tableModel) => {
        model = tableModel;
        this.generate(model, metadata);
    }

    this.onReset = () => {
        this.clear();
        model = new TableModel([], []);
        showPlaceholder(true);
    }

    this.clear = () => {
        htmlPreview.innerHTML = "";
    }

    this.getValue = () => {
        return htmlPreview.innerText;
    }

    /**
     * Displays the specified ascii table.
     *
     * @param asciiTable  the table to display, formatted as a list of strings
     */
    this.display = (asciiTable) => {
        const value = metadata.getCommentType();
        const lineStart = StringUtils.escapeHTMLChars(value.lineStart);
        const prefix = StringUtils.escapeHTMLChars(value.prefix);
        const suffix = StringUtils.escapeHTMLChars(value.suffix);
        const space = (StringUtils.isBlank(lineStart)) ? '' : ' ';
        let printable = (prefix === '') ? '' : prefix + '<br/>';
        printable += printable;
        for (let row of asciiTable) {
            printable += lineStart + space + row + '<br/>';
        }
        printable += suffix;
        htmlPreview.innerHTML = printable;
        if (Settings.SHOW_LINE_NUMBERS) {
            numbers.printLineNumbers(htmlPreview.innerText);
            numbers.setVisible(true);
        }
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const showPlaceholder = (show) => {
        if (placeholderShowing === show) {
            return;
        }
        const component = show ? placeholder.getUI() : htmlPreviewContainer.getUI();
        content
            .clearNodes()
            .addNode(component);
        placeholderShowing = show;
    }

    const updateButtonFromFormat = (format) => {
        expandBtn.setTitle(`Result (${format.displayName})`);
    }

    /**
     * Fallback using the {@code document.execCommand('copy')} api.
     *
     * This implementation uses the following answer from StackOverflow.
     * Thanks to Dean Taylor:
     * https://stackoverflow.com/questions/400212/how-do-i-copy-to-the-clipboard-in-javascript
     */
    const fallbackCopyTextToClipboard = (text) => {
        const textArea = document.createElement("textarea");
        textArea.value = text;
        textArea.style.padding = '0';
        textArea.style.border = 'none';
        textArea.style.outline = 'none';
        textArea.style.boxShadow = 'none';
        textArea.style.background = 'transparent';
        // Avoid scrolling to bottom
        textArea.style.top = "0";
        textArea.style.left = "0";
        textArea.style.position = "fixed";
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();
        try {
            document.execCommand('copy');
        } catch (err) {
            messagePool.error('Your browser doesn\'t support copying to clipboard: ' + err);
        }
        document.body.removeChild(textArea);
    };

    const getEncoder = (metadata) => {
        switch (metadata.getFormat()) {
            case Format.UNICODE:
            case Format.UNICODE_BOLD:
                // Fall through
            case Format.PLAIN_ASCII:
                return new PlainTextEncoder(metadata);
            case Format.GITHUB_MARKDOWN:
                return new GitHubMarkdownEncoder(metadata);
            case Format.CUCUMBER_MARKDOWN:
                // Fall through
            case Format.CONFLUENCE_MARKDOWN:
                return new ConfluenceEncoder(metadata);
            case Format.LATEX:
                return new LatexEncoder(metadata);
            case Format.ASCII_DOC:
                return new AsciiDocEncoder(metadata);
            case Format.MEDIAWIKI:
                return new MediaWikiEncoder(metadata);
            case Format.HTML:
                return new HtmlEncoder(metadata);
            case Format.BBCODE:
                return new BBCodeEncoder(metadata);
            case Format.CSV:
                return new CsvEncoder(metadata);
            case Format.SQL:
                return new SqlEncoder(metadata);
            case Format.JSON:
            case Format.PYTHON:
            case Format.JAVASCRIPT:
            case Format.PHP:
            case Format.JAVA:
                // Fall through
            case Format.C_SHARP:
                return new CTypeEncoder(metadata);
            default:
                throw new Error('Unsupported format. No encoder found for: ' + metadata.getFormat().displayName);
        }
    };

    const getHighlighter = (metadata) => {
        const format = metadata.getFormat();
        switch (format) {
            case Format.SQL:
                return new SqlHighlighter();
            case Format.CSV:
                return new CsvHighlighter(metadata.getSeparator());
            case Format.LATEX:
                return new LatexHighlighter();
            case Format.MEDIAWIKI:
                // Fall through
            case Format.ASCII_DOC:
                return new MediaWikiHighlighter(format);
            case Format.GITHUB_MARKDOWN:
            case Format.CUCUMBER_MARKDOWN:
                // Fall through
            case Format.CONFLUENCE_MARKDOWN:
                return new MarkdownHighlighter(format);
            case Format.HTML:
                // Fall through
            case Format.BBCODE:
                return new TagBasedHighlighter(format);
            case Format.PLAIN_ASCII:
            case Format.UNICODE:
                // Fall through
            case Format.UNICODE_BOLD:
                return new PlainTextHighlighter(format);
            case Format.JSON:
            case Format.PYTHON:
            case Format.JAVASCRIPT:
            case Format.PHP:
            case Format.JAVA:
                // Fall through
            case Format.C_SHARP:
                return new CTypeHighlighter(format);
            default:
                throw new Error('Unsupported format: no syntax highlighter found for ' + format.displayName);
        }
    }

    const highlightSyntax = (input, metadata) => {
        return syntaxHighlightingEnabled ? getHighlighter(metadata).highlight(input) : input;
    }

    const createContent = () => {
        return new Component()
            .addClass('renderer-content');
    }

    const createPreview = () => {
        htmlPreview = new Component('pre')
            .addClass('result')
            .getUI();
        numbers = new LineNumbers();
        return new Component()
            .add(numbers)
            .addNode(htmlPreview)
            .addClass('fill-parent')
            .addClass('renderer-preview');
    }

    const createPlaceholder = () => {
        const button = new Button('Prefill with random data', prefill, 'Load example data', ButtonStyle.FILLED)
            .addClass('prefill-btn', 'primary')
            .withIcon(Icons.NEW);
        return new Component()
            .addClass('renderer-placeholder')
            .add(new MarkdownRenderer(getPlaceholderText()))
            .add(button);
    }

    const prefill = () => {
        const data = generateRandomMatrix(Settings.PREFILL_EMPTY_CELLS_ALLOW);
        const model = new TableModel([], data);
        onPrefillRequest(model);
        messagePool.success('Loaded random data');
    }

    const generateRandomMatrix = (allowEmptyCells) => {
        const w = Values.randInt(MIN_CELLS.getX(), MAX_CELLS.getX());
        const h = Values.randInt(MIN_CELLS.getY(), MAX_CELLS.getY());
        return ArrayUtils.range(0, h).map(y =>
            ArrayUtils.range(0, w).map(x => randomCell(allowEmptyCells))
        );
    }

    const randomCell = (allowEmptyCells) => {
        return (!allowEmptyCells || Values.randBool(Settings.PREFILL_EMPTY_CELLS_BIAS)) ?
            Values.randElement(Settings.ALPHABET) : null;
    }

    const getPlaceholderText = () => {
        return `
            # Getting Started
            1. Start creating a new table using the editor
            or 
            2. Click \`Import\` to modify or convert an existing table to any other format
            When you're happy with the results, click \`Copy Result\` to use your table wherever you like.
            Tip: You can display help at any time using \`Ctrl + Q\` or by clicking the info button on the bottom right of the screen.
            Happy editing!
        `;
    }

    const initComponents = () => {
        content = createContent();
        placeholder = createPlaceholder();
        htmlPreviewContainer = createPreview()
        expandBtn.addTarget(this.getUI());
    }

    const init = () => {
        syntaxHighlightingEnabled = Settings.SYNTAX_HIGHLIGHTING;
        initComponents();
        this.addClass('renderer')
            .add(content)
            .add(expandBtn);
        this.onReset();
        this.generate(new TableModel([], []), metadata);
        this.setOrientation(orientation);
    };

    init();

};
