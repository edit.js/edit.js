/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Component} provides the primary tools allowing the user to
 * control the behavior of the program.
 * When the user interacts with one of its controls, the toolbar
 * sends notifications to the handlers which are responsible for
 * processing the user request.
 *
 * @param {FormatPicker} formatPicker  a {@link FormatPicker} object
 * @param {ImportDialog} importDialog  a {@link ImportDialog} object
 * @param {CreateDialog} createDialog  a {@link CreateDialog} object
 * @param {{key:Action}} GlobalActions  a set of {@link Action actions}
 */
const Toolbar = function(formatPicker, importDialog, createDialog, GlobalActions) {

    Component.call(this);

    const HAMBURGER_BREAKPOINT = ScreenSize.L;

    // contextual options
    let rowBorderCombobox;
    let columnBorderCombobox;
    let outputCombobox;
    let commentCombobox;
    let jsonFormatCombobox;
    let headerOrientationCombobox;
    let headerAlignmentCombobox;
    let headerCaseCombobox;
    let javaFormatCombobox;
    let phpFormatCombobox;
    let pythonFormatCombobox;
    let cSharpFormatCombobox;
    let csvSeparatorField;
    let emptyCellPlaceholderField;
    // listeners
    const renderOptionChangeListeners = [];
    const editorActionListeners = [];
    const copyActionListeners = [];
    // UI
    let hamburgerBtn;
    let hamburgerIcon;
    let editorControls;
    let contextualMenu;
    let contextualOptionsContainer;
    // State
    let isHamburgerEnabled = false;
    let bulkEditMode = false;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.closeContextualMenu = () => {
        contextualMenu.setVisible(false);
    };

    this.getMetadata = () => {
        return new TableMetadata()
            .setFormat(formatPicker.getValue())
            .setRowBorderPolicy(rowBorderCombobox.getValue())
            .setColumnBorderPolicy(columnBorderCombobox.getValue())
            .setOutputPolicy(outputCombobox.getValue())
            .setCommentType(commentCombobox.getValue())
            .setSeparator(csvSeparatorField.getValue())
            .setJsonFormat(jsonFormatCombobox.getValue())
            .setJavaFormat(javaFormatCombobox.getValue())
            .setCSharpFormat(cSharpFormatCombobox.getValue())
            .setPhpFormat(phpFormatCombobox.getValue())
            .setPythonFormat(pythonFormatCombobox.getValue())
            .setEmptyCellPlaceholder(emptyCellPlaceholderField.getValue())
            .setHeaderOrientation(headerOrientationCombobox.getValue())
            .setHeaderCase(headerCaseCombobox.getValue())
            .setHeaderAlignment(headerAlignmentCombobox.getValue());
    }

    this.setMetadata = (metadata) => {
        formatPicker.setValue(metadata.getFormat());
        setIfNotNull(outputCombobox, metadata.getOutputPolicy());
        setIfNotNull(rowBorderCombobox, metadata.getRowBorderPolicy());
        setIfNotNull(columnBorderCombobox, metadata.getColumnBorderPolicy());
        setIfNotNull(headerOrientationCombobox, metadata.getHeaderOrientation());
        setIfNotNull(headerCaseCombobox, metadata.getHeaderCase());
        setIfNotNull(headerAlignmentCombobox, metadata.getHeaderAlignment());
        setIfNotNull(csvSeparatorField, metadata.getSeparator());
        setIfNotNull(jsonFormatCombobox, metadata.getJsonFormat());
        setIfNotNull(cSharpFormatCombobox, metadata.getCSharpFormat());
        setIfNotNull(javaFormatCombobox, metadata.getJavaFormat());
        setIfNotNull(phpFormatCombobox, metadata.getPhpFormat());
        setIfNotNull(pythonFormatCombobox, metadata.getPythonFormat());
        setIfNotNull(emptyCellPlaceholderField, metadata.getEmptyCellPlaceholder());
    };

    this.addCopyActionListener = (listener) => {
        copyActionListeners.push(listener);
    };

    this.addEditorActionListener = (listener) => {
        editorActionListeners.push(listener);
    };

    this.addRenderOptionChangeListener = (listener) => {
        renderOptionChangeListeners.push(listener);
        listener.onRenderOptionsChange(this.getMetadata());
    };

    this.onMetadataImport = (metadata) => {
        bulkEditMode = true;
        this.setMetadata(metadata);
        updateContextualOptions();
        notifyRenderOptionChangeListeners(this.getMetadata());
        bulkEditMode = false;
    };

    this.setLayout = (screenSize) => {
        isHamburgerEnabled = (screenSize <= HAMBURGER_BREAKPOINT);
        hamburgerBtn.setVisible(isHamburgerEnabled);
        if (!hamburgerBtn.hasClass('open')) {
            editorControls.setVisible(!isHamburgerEnabled);
        }
        if (!isHamburgerEnabled) {
            this.openHamburger(false);
        }
    }

    this.openHamburger = (open) => {
        if (isHamburgerEnabled) {
            editorControls.setVisible(open);
        }
        hamburgerBtn.toggleClass('open', open);
        hamburgerIcon.toggleClass('open', open);
    }

    this.toggleHamburger = () => {
        this.openHamburger(!hamburgerBtn.hasClass('open'));
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const toggleEditorOptions = () => {
        editorControls.toggleClass('hidden');
        hamburgerBtn.toggleClass('open');
        hamburgerIcon.toggleClass('open');
    }

    const toggleCreateDialog = (node) => {
        const margin = 5;
        let coordinate = new Vector(
            node.offsetLeft,
            node.offsetTop + node.offsetHeight + margin
        );
        if (isHamburgerEnabled) {
            coordinate = coordinate.add(editorControls.getOffset());
        }
        createDialog.relocate(coordinate);
        createDialog.toggle();
    };

    const openContextualMenu = (anchorElement) => {
        const offset = anchorElement.getBoundingClientRect();
        const x = offset.x - 500 + anchorElement.offsetWidth;
        const y = offset.y + 50;
        contextualMenu.setVisible(true)
            .putStyle('left', x + 'px')
            .putStyle('top', y + 'px');
    };

    const openImportDialog = () => {
        importDialog.setOpen(true);
        closeHamburgerIfOpen();
    }

    const closeHamburgerIfOpen = () => {
        if (isHamburgerEnabled) {
            this.openHamburger(false);
        }
    }

    const notifyCopyActionListeners = () => {
        copyActionListeners.forEach(listener => listener.onCopyAction());
    };

    const notifyEditorActionListeners = (actionRequest) => {
        editorActionListeners.forEach(listener => listener.onActionRequest(actionRequest));
        closeHamburgerIfOpen();
    };

    const notifyRenderOptionChangeListeners = (metadata) => {
        renderOptionChangeListeners.forEach(listener => listener.onRenderOptionsChange(metadata));
    };

    const onFormElementValueChange = () => {
        if (bulkEditMode) {
            return;
        }
        updateContextualOptions();
        notifyRenderOptionChangeListeners(this.getMetadata());
    };

    const onTextFieldValueChange = () => {
        notifyRenderOptionChangeListeners(this.getMetadata());
    };

    const updateContextualOptions = () => {
        displayContextualOptions(getOptionsToDisplay());
    };

    const displayContextualOptions = (options) => {
        contextualOptionsContainer.clearNodes();
        options.forEach(option => {
            contextualOptionsContainer.add(option)
        });
    };

    const getOptionsToDisplay = () => {
        switch (formatPicker.getValue()) {
            case Format.UNICODE:
            case Format.UNICODE_BOLD:
            // Fall through
            case Format.PLAIN_ASCII:
                return [rowBorderCombobox, columnBorderCombobox, headerOrientationCombobox, headerCaseCombobox, headerAlignmentCombobox];
            case Format.GITHUB_MARKDOWN:
                return [headerCaseCombobox, outputCombobox];
            case Format.CUCUMBER_MARKDOWN:
            // Fall through
            case Format.CONFLUENCE_MARKDOWN:
                return [headerCaseCombobox, outputCombobox];
            case Format.LATEX:
                return [rowBorderCombobox, columnBorderCombobox, emptyCellPlaceholderField, outputCombobox];
            case Format.MEDIAWIKI:
                return [headerOrientationCombobox, headerCaseCombobox, outputCombobox];
            case Format.JSON:
                return [jsonFormatCombobox, outputCombobox];
            case Format.HTML:
                return [headerOrientationCombobox, headerCaseCombobox, headerAlignmentCombobox, outputCombobox];
            case Format.BBCODE:
                return [headerOrientationCombobox, headerCaseCombobox, outputCombobox];
            case Format.ASCII_DOC:
                return [headerOrientationCombobox, headerCaseCombobox, outputCombobox];
            case Format.CSV:
                return [csvSeparatorField];
            case Format.JAVA:
                return [javaFormatCombobox, outputCombobox];
            case Format.C_SHARP:
                return [cSharpFormatCombobox, outputCombobox];
            case Format.JAVASCRIPT:
                return [jsonFormatCombobox, outputCombobox];
            case Format.PHP:
                return [phpFormatCombobox, outputCombobox];
            case Format.PYTHON:
                return [pythonFormatCombobox, outputCombobox];
            case Format.SQL:
            // Fall through
            default:
                return [];
        }
    };

    const setIfNotNull = (formElement, value) => {
        if (value != null) {
            formElement.setValue(value);
        }
    };

    const createBranding = () => {
        return new Component()
            .addClass('branding')
            .add(new Logo())
            .add(new AppName('label-center', 'branding-label'));
    };


    const createEditorControls = () => {
        const buttonsContainer = new Component()
            .addClass('editor-controls');
        [
            createTextButton('Import',        () => openImportDialog(), 'Import an existing table ' + GUI.ppKeyStroke(GlobalActions.TOGGLE_IMPORT), Icons.PASTE),
            createTextButton('New',   function() { toggleCreateDialog(this); }, 'Create a new table of specified dimension', Icons.NEW),
            createTextButton('Undo',          () => notifyEditorActionListeners(ActionRequest.UNDO), GUI.ppAction(GlobalActions.UNDO), Icons.UNDO),
            createTextButton('Redo',          () => notifyEditorActionListeners(ActionRequest.REDO), GUI.ppAction(GlobalActions.REDO), Icons.REDO),
            createTextButton("Reset",         () => notifyEditorActionListeners(ActionRequest.RESET), 'Restore initial state', Icons.RESET),
            createTextButton("Clear",         () => notifyEditorActionListeners(ActionRequest.CLEAR), 'Clear all cells content', Icons.CLEAR),
            createTextButton("Transpose",     () => notifyEditorActionListeners(ActionRequest.TRANSPOSE), 'Transpose the table', Icons.TRANSPOSE)
        ].forEach(button => buttonsContainer.add(button));
        return buttonsContainer;
    };

    const createTextButton = (text, callback, tip, iconName, rotation) => {
        return new Button(text, callback, tip, ButtonStyle.TEXT)
            .withIcon(iconName, rotation);
    };

    const createOutlinedButton = (text, callback, tip, iconName, rotation) => {
        return new Button(text, callback, tip, ButtonStyle.OUTLINED)
            .withIcon(iconName, rotation)
            .addClass('style-rectangle');
    };

    const createRendererControls = () => {
        const optionsBtn = createOutlinedButton('Options', function() {openContextualMenu(this)}, 'Displays the contextual options', Icons.OPTIONS)
            .putStyle('marginRight', '5px');
        const optionsStep = new Component('span')
            .add(optionsBtn)
            .addClass('step');
        const btn = new Button('Copy Result', notifyCopyActionListeners, 'Copy result to clipboard', ButtonStyle.FILLED)
            .withIcon(Icons.COPY)
            .addClass('primary', 'style-rectangle')
            .putStyle('marginRight', '5px');
        return new Component()
            .addClass('renderer-controls')
            .add(createFormatCombobox())
            .add(optionsStep)
            .add(btn);
    };

    const createFormatCombobox = () => {
        formatPicker.registerCallback(ignored => onFormElementValueChange());
        return new Component('span')
            .addClass('step')
            .add(formatPicker);
    };

    const createTopDiv = () => {
        const title = new Component()
            .addClass('dialog-title')
            .setText('Contextual options');
        const permanentOptions = new Component()
            .add(commentCombobox);
        contextualOptionsContainer = new Component();
        return new Component()
            .addClass('toolbar-options')
            .add(title)
            .add(contextualOptionsContainer)
            .add(permanentOptions);
    };

    const createContextualMenu = () => {
        const closeButton = new CloseButton('Close dialog', () => this.closeContextualMenu());
        const text = '' +
            'These options vary depending on the table format.<br/>' +
            'You can <b>drag</b> this dialog with your mouse.';
        const bottomTip = new Component()
            .addClass('toolbar-options-tip')
            .setHtml(text);
        contextualMenu = new Component()
            .add(createTopDiv())
            .add(bottomTip)
            .add(closeButton)
            .addClass('dialog', 'toolbar-options-dialog')
            .setVisible(false);
        GUI.draggable(contextualMenu.getUI(), true);
        return contextualMenu;
    };

    const initFormComponents = () => {
        rowBorderCombobox = Forms.createRowBorderCombobox(onFormElementValueChange);
        columnBorderCombobox = Forms.createColumnBorderCombobox(onFormElementValueChange);
        outputCombobox = Forms.createOutputCombobox(onFormElementValueChange);
        commentCombobox = Forms.createCommentCombobox(onFormElementValueChange);
        jsonFormatCombobox = Forms.createJsonOptionCombobox(onFormElementValueChange);
        headerOrientationCombobox = Forms.createHeaderCombobox(onFormElementValueChange);
        headerAlignmentCombobox = Forms.createHeaderAlignmentCombobox(onFormElementValueChange);
        headerCaseCombobox = Forms.createHeaderCaseCombobox(onFormElementValueChange);
        javaFormatCombobox = Forms.createJavaDataStructureCombobox(onFormElementValueChange);
        phpFormatCombobox = Forms.createPhpFormatCombobox(onFormElementValueChange);
        pythonFormatCombobox = Forms.createPythonFormatCombobox(onFormElementValueChange);
        cSharpFormatCombobox = Forms.createCSharpDataStructureCombobox(onFormElementValueChange);
        csvSeparatorField = Forms.createCsvSeparatorField(onTextFieldValueChange);
        emptyCellPlaceholderField = Forms.createLatexPlaceholderField(onTextFieldValueChange);
    }

    const createHamburgerButton = () => {
        hamburgerIcon = new Component()
            .add(new Component())
            .add(new Component())
            .add(new Component())
            .add(new Component())
            .addClass('hamburger')
        hamburgerBtn = new Component()
            .addClass('hamburger-btn', 'center', 'clickable')
            .add(hamburgerIcon)
            .setTip(GUI.ppAction(GlobalActions.TOGGLE_EDITOR_ACTIONS))
            .addListener('click', toggleEditorOptions, false);
        return new Component()
            .addClass('hamburger-container')
            .add(hamburgerBtn);
    }

    const createComponentListener = () => {
        return {
            onVisibilityChanged: (component, visible) => {
                if (!visible) {
                    closeHamburgerIfOpen();
                }
            }
        }
    }

    const init = () => {
        editorControls = createEditorControls();
        initFormComponents();
        this.addClass('toolbar')
            .add(createBranding())
            .add(createHamburgerButton())
            .add(editorControls)
            .add(createRendererControls())
            .add(createContextualMenu());
        updateContextualOptions();
        this.setLayout(GUI.getScreenSize());
        createDialog.addComponentListener(createComponentListener());
    };

    init();

};
