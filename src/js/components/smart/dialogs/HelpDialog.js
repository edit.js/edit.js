/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const HelpDialog = function(actions) {

    Dialog.call(this);

    let content;

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const checkUpdates = () => {
        window.open(`${editjs.REPOSITORY}/-/releases`, '_blank');
    }

    /**
     * Converts the specified list of {@link Action actions} as a 2d array
     * of strings.
     *
     * @param {Action[]} actions  the list of actions to convert
     * @return {string[][]}
     */
    const toMatrix = (actions) => {
        const matrix = [];
        actions.forEach(action => {
           const row = [];
           row.push(action.getName());
           row.push(action.getKeyStroke().toString());
           row.push(action.getContext());
           matrix.push(row);
        });
        return matrix;
    }

    const displayShortcuts = () => {
        const table = new Table(toMatrix(actions), ['Action', 'Shortcut', 'Context'], createCellFormatter());
        content
            .clearNodes()
            .add(table);
    }

    const displayAbout = () => {
        const btn = new Button('Check Latest', checkUpdates, `Get latest version on ${editjs.HOST}`, ButtonStyle.FILLED)
            .addClass('style-rectangle');
        const btnContainer = new Component()
            .addClass('about-top-bar-button-container')
            .add(btn);
        const logoContainer = new Component()
            .addClass('app-logo')
            .add(new Logo(60, 60));
        const versionContainer = new Component()
            .addClass('app-summary')
            // .add(text('edit.js', 'app-name'))
            .add(new AppName('test'))
            .add(text(`Version ${editjs.VERSION}`, 'app-version'))
        const appInfo = new Component()
            .addClass('app-info')
            .add(logoContainer)
            .add(versionContainer);
        const bar = new Component()
            .addClass('about-top-bar')
            .add(appInfo)
            .add(btnContainer);
        content
            .clearNodes()
            .add(bar)
            .add(new MarkdownRenderer(getAboutText()));
        function text(txt, className) {
            return new Component().addClass(className).setText(txt);
        }
    }

    const getAboutText = () => {
        return `
            ## Contributing
            edit.js is an open source project; contributions are welcome.
            To report a bug or request a new feature, feel free to open an issue or submit a pull request on ${editjs.HOST}.
            ${editjs.REPOSITORY}
            ## License
            Licensed under Apache version 2.0.
        `;
    }

    const createMenu = () => {
        const menu = new Component()
            .addClass('help-menu');
        const group = new ButtonGroup();
        const items = [
            {name: 'Shortcuts', callback: displayShortcuts},
            {name: 'About', callback: displayAbout}
        ];
        items.forEach((item, i) => {
            const button = new ToggleButton(item.name, item.callback, true);
            const active = (i === 0);
            menu.add(button);
            group.add(button, active);
            if (active) {
                button.getUI().click();
            }
        });
        return menu;
    }

    const createContent = () => {
        return new Component()
            .addClass('help-content');
    }

    const createTitle = () => {
        return new Component()
            .addClass('dialog-title')
            .setText('Help');
    }

    const createCenterPanel = () => {
        return new Component()
            .addClass('help-wrapper')
            .add(createMenu())
            .add(content);
    }

    const createCellFormatter = () => {
        return (cell, x, y, header) => (header === 'Context') ? createTag(cell) : document.createTextNode(cell);
        function createTag(cell) {
            let icon = getIcon(cell);
            return new Tag(cell, TagType.ICONIFIED, 'context-' + cell, icon).getUI();
        }
        function getIcon(cell) {
            switch (cell) {
                case Context.GLOBAL:
                    return Icons.GLOBAL;
                case Context.EDITOR:
                    return Icons.TABLE2;
                case Context.IMPORT:
                    return Icons.UPLOAD;
                default:
                    return Icons.HELP;
            }
        }
    }

    const init = () => {
        const closeButton = new CloseButton('Close dialog', () => this.setOpen(false));
        content = createContent();
        this
            .add(closeButton)
            .add(createTitle())
            .add(createCenterPanel())
            .addClass('help-dialog', 'center');
    }

    init();

}
