/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Dialog} provides controls to import and process an existing table for further
 * editing.
 *
 * @param {TableReader} reader  the reader to use for parsing the input string into
 * a table model
 * @param {MessagePool} messagePool  the message pool to use for providing feedback
 * to the user
 * @param {EventProcessor} eventProcessor  the {@link EventProcessor} to use for processing
 * internal key events
 */
const ImportDialog = function(reader, messagePool, eventProcessor) {

    Dialog.call(this);

    // State
    let status;
    // UI
    let importBtn;
    let messageContainer;
    let tooltip;
    let tooltipWrapper;
    let contextualOptions;
    // form elements
    let textArea;
    let formatPicker;
    let separatorTextField;
    let emptyCellPlaceholderField;

    const actions = [
        new Action('Paste and detect', Context.IMPORT, new KeyStroke(Key.V, Modifier.CTRL), () => autodetect()),
        new Action('Confirm import',   Context.IMPORT, new KeyStroke(Key.ENTER, Modifier.CTRL), () => importTable())
    ];

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.open = () => {
        setTooltipVisible(false);
        textArea.getUI().focus();
    }

    this.close = () => {
        setTooltipVisible(false);
        textArea.getUI().value = '';
        formatPicker.reset();
        hideMessage();
        resetContextualOptions();
    };

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const showMessage = (msg, messageType) => {
        const icon = new Icon(messageType.icon)
            .putStyle('marginRight', '10px');
        const textContainer = new Component('span')
            .setText(msg);
        if (status != null) {
            messageContainer.removeClass(status.displayName)
        }
        messageContainer
            .addClass(messageType.displayName)
            .clearNodes()
            .add(icon)
            .add(textContainer)
            .setVisible(true);
        status = messageType;
    };

    const setTooltipVisible = (visible) => {
        tooltipWrapper.toggleClass('highlighted', visible);
        tooltip.setVisible(visible);
    }

    const hideMessage = () => {
        messageContainer.setVisible(false);
    };

    const onFormatSelectionChange = () => {
        setTooltipVisible(false);
        hideMessage();
        updateContextualOptions();
    };

    const importTable = () => {
        const metadata = getMetadata();
        if (metadata.getFormat() === null) {
            showMessage('Please specify the format of the table to import', MessageType.ERROR);
            return;
        }
        const input = getUserInput();
        if (!StringUtils.isBlank(input)) {
            try {
                reader.import(input, metadata);
                messagePool.success('Data successfully imported');
            } catch(error) {
                const errorMsg = 'Unable to parse input';
                Logger.error('ImportDialog', errorMsg + ':', error);
                showMessage(errorMsg, MessageType.ERROR);
                return;
            }
        }
        this.setOpen(false);
    }

    const getUserInput = () => {
        return textArea.getUI().value;
    }

    const resetContextualOptions = () => {
        contextualOptions.clearNodes();
    };

    const updateContextualOptions = () => {
        resetContextualOptions();
        switch (formatPicker.getValue()) {
            case Format.CSV:
                contextualOptions.add(separatorTextField);
                break;
            case Format.LATEX:
                contextualOptions.add(emptyCellPlaceholderField);
                break;
        }
    };

    const getMetadata = () => {
        return new TableMetadata()
            .setFormat(formatPicker.getValue())
            .setSeparator(separatorTextField.getValue())
            .setEmptyCellPlaceholder(emptyCellPlaceholderField.getValue());
    }

    const autodetect = () => {
        const detectedFormat = reader.detectFormat(getUserInput());
        if (detectedFormat != null) {
            formatPicker.setValue(detectedFormat);
            setTooltipVisible(true);
            hideMessage();
            return true;
        }
        return false;
    }

    const onKeyup = (evt) => {
        const action = eventProcessor.getAction(evt);
        if (action != null) {
            evt.preventDefault();
            action.process(evt);
        }
    };

    const onAutodetectClick = () => {
        if (!autodetect() && !StringUtils.isBlank(getUserInput())) {
            const msg = 'Could not automatically infer format';
            showMessage(msg, MessageType.WARN);
        }
    }

    const createToolbar = () => {
        const closeButton = new CloseButton('Close dialog', () => this.setOpen(false));
        const title = new Component()
            .addClass('dialog-title')
            .setText('Import');
        return new Component()
            .addClass('import-dialog-toolbar')
            .add(title)
            .add(closeButton);
    };

    const createTooltip = () => {
        const text = new Component()
            .addClass('tooltip-text')
            .setText('Format detected');
        const tooltipText = new Component()
            .addClass('tooltip-text-wrapper')
            .add(new Icon(Icons.OK))
            .add(text);
        tooltip = new Component()
            .addClass('tooltip')
            .setVisible(false)
            .add(tooltipText);
        tooltipWrapper = new Component()
            .addClass('tooltip-wrapper')
            .add(formatPicker)
            .add(tooltip);
        return tooltipWrapper;
    };

    const createPickerWrapper = () => {
        const btn = new Button('', onAutodetectClick, 'Auto-detect format', ButtonStyle.FILLED)
            .withIcon(Icons.MAGIC_WAND)
            .addClass('style-rectangle', 'detect-format-btn');
        return new Component()
            .addClass('picker-wrapper')
            .add(createTooltip())
            .add(btn);
    }

    const init = () => {
        separatorTextField = Forms.createCsvSeparatorField();
        emptyCellPlaceholderField = Forms.createLatexPlaceholderField();
        formatPicker = Forms.createFormatPicker('Specify the table format to import', 'fp-import')
            .registerCallback(ignored => onFormatSelectionChange())
            .putStyle('width', '100%');
        messageContainer = new Component()
            .addClass('message')
            .setVisible(false);
        textArea = new Component('textarea')
            .addListener('keyup', e => onKeyup(e), false);
        textArea.getUI().placeholder = 'Paste table to import...';
        importBtn = new Button('Import', importTable, 'Import table', ButtonStyle.FILLED)
            .withIcon(Icons.UPLOAD)
            .addClass('import-button', 'style-rectangle');
        contextualOptions = new Component();
        eventProcessor.register(...actions);
        this.add(createToolbar())
            .add(textArea)
            .add(createPickerWrapper())
            .add(contextualOptions)
            .add(messageContainer)
            .add(importBtn)
            .addClass('import-dialog', 'center');
    };

    init();

};
