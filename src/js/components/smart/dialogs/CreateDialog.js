/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Dialog} provides controls to generate a new table of arbitrary
 * dimension.
 *
 * @param {number} c  the initial number of columns
 * @param {number} r  the initial number of rows
 */
const CreateDialog = function(c, r) {

    Dialog.call(this);

    const SCALE = 40;
    const COUNT_HEIGHT = 30;
    const LOGICAL_OFFSET = new Vector(1, 1);

    const listeners = [];
    const cols = c + 1;
    const rows = r + 1;
    let lastCoordinate = new Vector(1, 1);
    // ui
    let area;
    let grid;
    let countLabel;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.reset = (width = cols, height = rows) => {
        grid.clearNodes();
        resize(width, height);
        countLabel.setText((width - 1) + ' x ' + (height - 1));
        printLines(height, Axis.X);
        printLines(width, Axis.Y);
    };

    this.relocate = (coordinate) => {
        this.moveTo(coordinate);
    };

    this.close = () => {
        this.reset();
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const computeLogicalCoordinate = (event) => {
        const cursorCoordinate = KeyEventProcessor.toPhysicalCoordinate(event);
        const relativeCoordinate = cursorCoordinate.sub(this.getOffset());
        return relativeCoordinate
            .div(SCALE)
            .map(c => Math.floor(c)).add(LOGICAL_OFFSET);
    };

    const onMouseMove = (event) => {
        const logicalCoordinate = computeLogicalCoordinate(event);
        if (!logicalCoordinate.equals(lastCoordinate)) {
            this.reset(logicalCoordinate.getX() + 1, logicalCoordinate.getY() + 1);
        }
        lastCoordinate = logicalCoordinate;
    };

    const resize = (width, height) => {
        area.putStyle('width', (width - 1) * SCALE + 1 + 'px');
        area.putStyle('height', (height - 1) * SCALE + 1 + 'px');
        this.putStyle('width', width * SCALE + 'px');
        this.putStyle('height', height * SCALE + COUNT_HEIGHT + 'px');
    };

    const printLines = (n, axis) => {
        const cssClass = GUI.toCssClass(axis);
        const property = (axis === Axis.X) ? 'top' : 'left';
        for (let i = 0; i < n - 1; i++) {
            const line = new Component()
                .addClass('line', cssClass)
                .putStyle(property, (i + 1) * SCALE + 'px');
            grid.add(line);
        }
    }

    const onMousedown = (event) => {
        switch (event.button) {
            // Left click
            case 0:
                const coordinate = computeLogicalCoordinate(event);
                notifyListeners(coordinate);
                this.setOpen(false);
                break;
            // Right click
            case 2:
                this.setOpen(false);
                break;
        }
    };

    this.addTableCreateListener = (listener) => {
        listeners.push(listener);
    };

    const notifyListeners = (coordinate) => {
        listeners.forEach(listener => listener.onTableCreate(coordinate));
    };

    const init = (cols, rows) => {
        area = new Component()
            .addClass('area');
        grid = new Component()
            .addClass('grid');
        countLabel = new Component()
            .addClass('count-label', 'label-center');
        const count = new Component()
            .addClass('grid-count')
            .add(countLabel);
        this
            .add(area)
            .add(grid)
            .add(count)
            .addListener('mousemove', e => onMouseMove(e), false)
            .addListener('mousedown', e => onMousedown(e), false)
            .addClass('create-dialog');
        this.reset(cols, rows);
    };

    init(cols, rows);

};
