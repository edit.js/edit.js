/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A service which displays one or several pooled messages using {@link Snackbar} instances.
 *
 * @param {Number} capacity  the maximum number of messages which can be displayed at a time
 * @param {Number} duration  the duration of the messages to display
 * @param {Component} target  the component to append the messages to
 *
 * @see MessageType
 * @see Snackbar
 */
const MessagePool = function(capacity, duration, target) {

    /** @type {Snackbar[]} */
    let pool = [];
    // cache
    let createdCount = 0;
    let displayedCount = 0;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * Requests the specified message to be displayed in a {@link Snackbar}.
     *
     * @param {string} message  the message to display
     * @param {MessageType} type  the type of the message to display
     */
    this.request = (message, type) => {
        let snackbar;
        if (hasAvailableMessages()) {
            snackbar = pool.shift();
            pool.push(snackbar);
        } else {
            if (!isPoolAtMaximumCapacity()) {
                snackbar = new Snackbar(duration, release);
                target.add(snackbar);
                pool.push(snackbar);
                createdCount++;
            } else if (areAllMessagesDisplayed()) {
                snackbar = pool.shift();
                snackbar.close();
                pool.push(snackbar);
            }
        }
        if (displayedCount === capacity) {
            displayedCount = 0;
        }
        displayedCount++;
        setTimeout(() => {
            snackbar.open(message, type, displayedCount * 58 - 15);
            Logger.debug('MessagePool', this.toString());
        }, 5)
    }

    this.success = (message) => { this.request(message, MessageType.SUCCESS); }

    this.error = (message) => { this.request(message, MessageType.ERROR); }

    this.toString = () => {
        return `[MessagePool] Capacity: ${capacity}, Messages: ${getMessageCount()}, Available: ${getAvailableCount()}`;
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const release = () => {
        if (getDisplayedCount() === 0) {
            displayedCount = 0;
        }
    }

    const getDisplayedCount = () => {
        return pool.filter(e => e.isOpen()).length;
    }

    const getAvailableCount = () => {
        return pool.filter(e => !e.isOpen()).length;
    }

    const hasAvailableMessages = () => {
        return (getAvailableCount() > 0);
    }

    const areAllMessagesDisplayed = () => {
        return (getDisplayedCount() === capacity);
    }

    const isPoolAtMaximumCapacity = () => {
        return (getMessageCount() === capacity);
    }

    const getMessageCount = () => {
        return createdCount;
    }

};
