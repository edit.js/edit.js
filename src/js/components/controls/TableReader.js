/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This class is responsible for deserializing an input string using one
 * of its known parsers, and notifying listeners.
 */
const TableReader = function() {

    const SUPPORTED_FORMATS = Object.values(Format);

    const importListeners = [];
    const formatChangeListeners = [];

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.import = (input, metadata) => {
        const result = Decoders.of(metadata).decode(input);
        result.getMetadata().setFormat(metadata.getFormat());
        result.getMetadata().setCommentType(detectCommentType(input));
        notifyFormatChangeListeners(result.getMetadata());
        notifyImportListeners(result.getTableModel());
    }

    /**
     * Detects the format of the specified input.
     *
     * @param {string} input  the input string to process
     * @return {Format} a {@link Format} constant if the input format could be detected; null otherwise
     */
    this.detectFormat = (input) => {
        const rows = Decoders.strip(input);
        if (rows.length === 0) {
            return null;
        }
        for (let format of SUPPORTED_FORMATS) {
            const metadata = new TableMetadata()
                .setFormat(format);
            const decoder = Decoders.of(metadata);
            const detected = decoder.supports(input);
            if (detected !== null) {
                Logger.debug('TableReader', 'Detected format:', detected);
                return detected;
            }
        }
        Logger.debug('TableReader', 'No format found for input:', input);
        return null;
    };

    this.addImportListener = (listener) => {
        importListeners.push(listener);
    };

    this.addFormatChangeListener = (listener) => {
        formatChangeListeners.push(listener);
    };

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const detectCommentType = (input) => {
        // future releases might support automatic comment type detection
        return CommentType.NONE;
    }

    const notifyImportListeners = (tableModel) => {
        importListeners.forEach(listener => listener.onModelImport(tableModel));
    };

    const notifyFormatChangeListeners = (metadata) => {
        formatChangeListeners.forEach(listener => listener.onMetadataImport(metadata));
    };

}
