/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * Instances of this class encapsulate functionality which can be triggered by a
 * {@link KeyStroke}.
 *
 * @param {string} name  the display name of this action. Useful for debugging and runtime
 * auto-generated documentation
 * @param {string} context  the context required for this action to be triggered
 * @param {KeyStroke} keyStroke  the {@link KeyStroke} which must trigger this action
 * @param {function(Event, ...*): void} callback  the callback to execute when this action is triggered
 */
const Action = function(name, context, keyStroke, callback) {

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.getName = () => {
        return name;
    }

    this.getContext = () => {
        return context;
    }

    /**
     * Returns the {@link KeyStroke} associated to this action.
     *
     * @return a {@link KeyStroke} instance
     */
    this.getKeyStroke = () => {
        return keyStroke;
    }

    /**
     * Triggers the callback associated to this action.
     *
     * @param evt  the event to process
     * @param args  the callback arguments
     */
    this.process = (evt, ...args) => {
        callback(evt, ...args);
    }

    /**
     * Determines whether this action must be triggered by this event.
     *
     * @param evt  the event to process
     * @return true if the keys associated to this event match this action {@link KeyStroke};
     * false otherwise
     */
    this.supports = (evt) => {
        return (
            keyStroke.getKey() === evt.key &&
            ArrayUtils.equalsIgnoreOrder(keyStroke.getModifiers(), KeyEventProcessor.getModifiers(evt))
        )
    }

    this.toString = () => {
        return quote(context) + ' ' + quote(name) + ' ' + keyStroke.toString();
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const quote = (subject) => {
        return '[' + subject + ']';
    }

}
