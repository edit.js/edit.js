/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A {@link Component} that is used to divide two components, each separated by a divider,
 * which can be dragged by the user to redistribute the available space between the two
 * components.
 *
 * @param {Component} a  the first component to divide
 * @param {Component} b  the second component to divide
 * @param {Axis} axis  the split pane orientation
 */
const SplitPane = function(a, b, axis = Axis.X) {

    Component.call(this);

    // UI
    let gutter;
    // Cache
    let orientation;
    let horizontal;
    let dynamicSizeProperty;
    let fixedSizeProperty;
    let dynamicOffsetProperty;
    let fixedOffsetProperty;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.getOrientation = () => {
        return orientation;
    }

    /**
     * Sets the split pane orientation to the specified {@link Axis}.
     *
     * @param {Axis} axis  the new orientation to apply
     */
    this.setOrientation = (axis) => {
        computeValues(axis);
        updateOrientation();
    }

    /**
     * Expands the component in this split pane identified by this parameter.
     *
     * @param {boolean | null} first  if true, expands the first component,
     *                                if false, expands the second component;
     *                                otherwise, equally redistributes available
     *                                space between the two components
     */
    this.expand = (first) => {
        switch (first) {
            case true:
                expandA();
                return;
            case false:
                expandB();
                return;
            default:
                restoreComponents();
        }
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const computeValues = (axis) => {
        orientation = axis;
        horizontal = (orientation === Axis.X);
        dynamicSizeProperty = getSizeProperty(horizontal);
        fixedSizeProperty = getSizeProperty(!horizontal);
        dynamicOffsetProperty = getOffsetProperty(horizontal);
        fixedOffsetProperty = getOffsetProperty(!horizontal);
    }

    const getOffsetProperty = (horizontal) => {
        return horizontal ? 'left' : 'top';
    }

    const getSizeProperty = (horizontal) => {
        return horizontal ? 'maxWidth' : 'maxHeight';
    }

    const expandA = () => {
        a.putStyle(dynamicSizeProperty, '90%');
        b.putStyle(dynamicSizeProperty, '10%');
        gutter.putStyle(dynamicOffsetProperty, '90%');
    }

    const expandB = () => {
        a.putStyle(dynamicSizeProperty, '10%')
        b.putStyle(dynamicSizeProperty, '90%');
        gutter.putStyle(dynamicOffsetProperty, '10%');
    }

    const restoreComponents = () => {
        a.putStyle('marginLeft', '0%');
        a.putStyle(dynamicSizeProperty, '50%');
        b.putStyle(dynamicSizeProperty, '50%');
        gutter.putStyle(dynamicOffsetProperty, '50%');
    }

    const computeRatio = (coordinate) => {
        return horizontal ?
            (coordinate.getX() / this.getWidth()  * 100) :
            (coordinate.getY() / this.getHeight() * 100);
    }

    const updateOrientation = () => {
        const newOrientation = GUI.toCssClass(orientation);
        const previousOrientation = GUI.toCssClass(GUI.switchAxis(orientation));
        gutter.removeClass(previousOrientation);
        gutter.addClass('gutter', newOrientation);
        initDrag();
        this.putStyle('flexDirection', horizontal ? 'row' : 'column');
        const dynamic = gutter.getStyle(dynamicOffsetProperty);
        const fixed = gutter.getStyle(fixedOffsetProperty);
        a.putStyle(fixedSizeProperty, '100%')
        b.putStyle(fixedSizeProperty, '100%');
        const ratio = parseFloat(fixed.replace('%', '')) || 50;
        a.putStyle(dynamicSizeProperty, ratio + '%');
        b.putStyle(dynamicSizeProperty, 100 - ratio + '%');
        gutter.putStyle(dynamicOffsetProperty, fixed);
        gutter.putStyle(fixedOffsetProperty, dynamic);
    }

    const initDrag = () => {
        const LIMIT_MIN = new Vector(10, 10);
        const LIMIT_MAX = new Vector(90, 90);
        GUI.draggable(gutter.getUI(), false, onLocationChange, onDrop, onGrab, orientation, LIMIT_MIN, LIMIT_MAX);
        function onLocationChange(current) {
            const aRatio = computeRatio(current);
            const bRatio = 100 - aRatio;
            a.putStyle(dynamicSizeProperty, aRatio + '%');
            b.putStyle(dynamicSizeProperty, bRatio + '%');
        }
        function onDrop() {
            a.removeStyle('transition');
            b.removeStyle('transition');
        }
        function onGrab() {
            a.putStyle('transition', 'none');
            b.putStyle('transition', 'none');
        }
    }

    const init = () => {
        computeValues(axis);
        gutter = new Component();
        updateOrientation();
        this.addClass('split-pane')
            .add(a)
            .add(b)
            .add(gutter);
    }

    init();

}
