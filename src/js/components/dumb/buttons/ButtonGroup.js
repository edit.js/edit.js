/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This control guarantees only one button from this group can be selected at a time.
 * Selecting of those buttons turns off all other buttons in this group.
 *
 * @param {boolean=} [allowNoSelection]  true if this group allows no selection; false
 *                                       otherwise
 * @param {ToggleButton=} [buttons]  an optional list of toggle buttons
 */
const ButtonGroup = function(allowNoSelection = false, ...buttons) {

    // Config
    const buttonList = [];
    let selectionChangeCallback = () => {};
    // State
    let cursor = -1;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.add = (button, active) => {
        buttonList.push(button);
        button.addListener('click', onActiveButtonChange, false);
        if (active) {
            button.setActive(true);
        }
        return this;
    }

    this.clearSelection = () => {
        if (cursor > -1) {
            setActive(cursor, false);
            cursor = -1;
        }
        if (!allowNoSelection) {
            setActive(0, true);
        }
    }

    /**
     * Subscribes to selection change events.
     *
     * @param {function(value: any, active: boolean): void} callback  the
     * listener to notify on selection change
     */
    this.onSelectionChange = (callback) => {
        selectionChangeCallback = callback;
        return this;
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const onActiveButtonChange = (evt) => {
        const previous = buttonList[cursor];
        const current = find(evt.target);
        cursor = buttonList.indexOf(current);
        if (previous != null) {
            previous.setActive(false);
        }
        const active = !(current === previous && allowNoSelection);
        current.setActive(active);
        selectionChangeCallback(current.getValue(), active)
        if (!active) {
            cursor = -1;
        }
    }

    const find = (btnNode) => {
        return buttonList.filter(b => b.getUI() === btnNode)[0];
    }

    const setActive = (index, active) => {
        cursor = index;
        buttonList[index].setActive(active);
    }

    const init = () => {
        buttons.forEach(btn => this.add(btn));
        if (buttonList.length > 0 && !allowNoSelection) {
            setActive(0, true);
        }
    }

    init();

}
