/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A button with no text and an icon at its center.
 *
 * @param {Icons} iconName  the icon to display in this button
 * @param {function(Event): void} callback  the callback to execute on button click
 * @param {string=} [tip]  an optional tip to display on mouse over
 * @param {number=} [rotation]  the initial rotation to apply to this buttons icon, in degrees
 * @param {ButtonStyle=} [style]  the {@link ButtonStyle} to apply
 */
const IconButton = function(iconName, callback, tip = '', rotation = 0, style = ButtonStyle.FILLED) {

    Button.call(this, '', callback, tip, style);

    let icon;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * Rotates the button icon around its center.
     *
     * @param {number} angle  the angle of rotation in degrees as an integer
     *                        where -360 < angle < +360
     */
    this.rotateIcon = (angle) => {
        icon.putStyle('transform', `rotate(${angle}deg)`);
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const init = () => {
        icon = new Icon(iconName, rotation);
        this
            .add(icon)
            .addClass('style-icon')
    }

    init();

}
