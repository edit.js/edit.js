/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Component} acts as a push button which can be toggled between two states.
 * The action to execute depends on the button state.
 *
 * @param {Side} side  the side to dock this component to
 * @param {function(): void} activeAction  the callback to execute when this button is selected
 * @param {function(): void} inactiveAction  the callback to execute when this button is deselected
 * @param {string} tip  the tooltip text
 * @param {string|null} title  an optional display name to be printed next to this button
 */
const ExpandButton = function(side, activeAction, inactiveAction, tip, title) {

    Component.call(this);

    // State
    let active = true;
    // UI
    let button;
    let label;
    // Cache
    let target;
    let activeIcon;
    let inactiveIcon;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.addTarget = (htmlElem) => {
        target = htmlElem;
    }

    this.setTitle = (text) => {
        label.setText(text);
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const toggle = () => {
        if (active) {
            activeAction();
        } else {
            inactiveAction();
        }
        updateIcon();
        active = !active;
    }

    const updateIcon = () => {
        const icon = active ? activeIcon : inactiveIcon;
        button
            .clearNodes()
            .add(icon);
    }

    const createTitle = () => {
        label = new Component()
            .addClass('label-center')
            .setText(title);
        return new Component()
            .addClass('expand-button-title')
            .add(label);
    }

    const init = () => {
        activeIcon = new Icon(Icons.MINIMIZE);
        inactiveIcon = new Icon(Icons.MAXIMIZE);
        button = new Component('button')
            .addClass('expand-button')
            .add(inactiveIcon)
        this.addClass('expand-button-wrapper')
            .putStyle(side, 0)
            .addClass('clickable')
            .addListener('click', toggle, false)
            .addListener('mouseenter', () => target.classList.add('highlighted'), false)
            .addListener('mouseleave', () => target.classList.remove('highlighted'), false)
            .setTip(tip);
        if (title != null) {
            this.add(createTitle());
        }
        this.add(button);
    }

    init();

}
