/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const TagType = Object.freeze({

    PLAIN:      'plain',
    ICONIFIED:  'iconified'

});

/**
 * Tags are compact elements that represent metadata about an element on the screen.
 * In the future, tags might support interaction with mouse and keyboard so that users
 * can make selections, filter content, or trigger actions.
 *
 * @param {string} text  the text to be displayed in this tag
 * @param {TagType=} [type]  an optional {@link TagType}
 * @param {string=} [cssClass]  an optional custom css class
 * @param {Icons=} [iconName]  an optional icon to display in this tag
 */
const Tag = function(text, type = TagType.PLAIN, cssClass, iconName) {

    Component.call(this, 'span');

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const init = () => {
        if (iconName != null) {
            const icon = new Component()
                .addClass('tag-icon')
                .add(new Icon(iconName));
            this.add(icon);
        }
        const tagName = new Component()
            .addClass('tag-name')
            .setText(text);
        this
            .add(tagName)
            .addClass('tag', type);
        if (!StringUtils.isBlank(cssClass)) {
            this.addClass(cssClass);
        }
    }

    init();

}

