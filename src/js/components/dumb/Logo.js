/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const Logo = function(width = 30, height = width, ...cssClasses) {

    Component.call(this);

    const getSvgCode = () => {
        return `
 <svg width="${width}" height="${height}" viewBox="0 0 7.9375 7.9375" xmlns="http://www.w3.org/2000/svg">
  <g transform="translate(-3.1119 -5.2917)"><path d="m6.8224 5.3596-2.9884 1.7254a0.51962 0.51962 120 0 0-0.25981 0.45v3.4508a0.51962 0.51962 60 0 0 0.25981 0.45l2.9884 1.7254a0.51961 0.51961-180 0 0 0.51961 0l2.9884-1.7254c0.14349-0.08284 0.1425-0.21544-8.57e-4 -0.2985-0.54273-0.31444-1.1041-0.66812-1.6329-0.9572-0.14494-0.07925-0.38057-0.07298-0.52406 0.0099l-0.83065 0.47957a0.51962 0.51962 9.1869e-7 0 1-0.51962 0l-0.83065-0.47961a0.1732 0.1732 90 0 1 1e-7 -0.3l0.83065-0.47957a0.1732 0.1732 90 0 0 1e-7 -0.3l-0.83065-0.47957a0.1732 0.1732 90 0 1 1e-7 -0.3l0.83065-0.47957a0.51962 0.51962-180 0 1 0.51962 0l0.83065 0.47957a0.51961 0.51961 180 0 0 0.51961-4.2e-6l1.6382-0.94583a0.17321 0.17321 90 0 0-1e-6 -0.3l-2.9884-1.7254a0.51961 0.51961 3.5361e-7 0 0-0.51961 0z" fill="#4481e2" fill-rule="evenodd" stroke-width=".5286"/></g>
 </svg>
     `;
    }

    const init = () => {
        this.addClass('logo', ...cssClasses)
            .setHtml(getSvgCode());
    }

    init();

}
