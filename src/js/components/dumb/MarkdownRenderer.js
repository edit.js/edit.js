/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const MarkdownRenderer = function(markdown) {

    Component.call(this);

    const Types = {
        H1:   'h1',
        H2:   'h2',
        BODY: 'p',
        LINK: 'a'
    };

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.render = (markdown) => {
        this.clearNodes();
        format(markdown)
            .forEach(component => this.add(component));
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const format = (markdown) => {
        return Decoders.strip(markdown)
            .map(line => toComponent(line));
    }

    const toComponent = (line) => {
        const type = getType(line);
        const text = parse(line);
        const isLink = (type === Types.LINK);
        const component = new Component(type)
            .setHtml(parseInlineElements(text));
        if (isLink) {
            const ui = component.getUI();
            ui.href = text;
            ui.target = '_blank';
        }
        return component;
    }

    const parseInlineElements = (line) => {
        const CODE_PATTERN = /`([^`]+)`/;
        const formatted = line.replace(CODE_PATTERN, (match, $1) => {
            return '<code>' + $1 + '</code>';
        });
        // Note: we might want to parse the LINK elements here in the future
        //  as to allow inline URLs.
        return formatted;
    }

    const getType = (line) => {
        const link = line.match(/https?:\/\/.*/);
        if (link != null) {
            return Types.LINK;
        }
        const text = line.match(/^#+/);
        if (text == null) {
            return Types.BODY;
        }
        return (text[0].length === 1) ? Types.H1 : Types.H2;
    }

    const parse = (line) => {
        return line.replace(/^#+ /, '');
    }

    const init = () => {
        this.addClass('markdown');
        this.render(markdown);
    }

    init();

}
