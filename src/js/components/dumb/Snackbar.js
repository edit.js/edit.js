/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A component responsible for displaying brief messages about app processes
 * at the bottom of the screen.
 *
 * More info on snackbars:
 * https://material.io/components/snackbars
 *
 * @param {number} duration  the duration of this message, in milliseconds
 * @param {function(Snackbar): void} [onClose]  an optional callback to be executed
 * after this snackbar has been closed
 *
 * @see MessageType
 */
const Snackbar = function(duration, onClose) {

    Component.call(this);

    const OFFSET = 45;

    // State
    let open;
    let startTime;
    // UI
    let message;
    let iconContainer;
    // Cache
    let previousType;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * Opens this snackbar with the specified text message and {@link MessageType}.
     *
     * @param {string} msg  the message to display
     * @param {MessageType} type  the type of the message to display
     * @param {number=} [offset]  an optional vertical offset, in pixels
     */
    this.open = (msg, type, offset = OFFSET) => {
        message.setText(msg);
        this.putStyle('opacity', '1');
        this.putStyle('marginBottom', `${offset}px`);
        setType(type);
        startTime = Date.now();
        open = true;
        setTimeout(this.close, duration);
    };

    this.close = () => {
        const elapsedTime = Values.timeDiff(startTime);
        if (elapsedTime < duration) {
            // do not hide if snackbar was reset before its duration was expired
            return;
        }
        open = false;
        this.putStyle('opacity', '0');
        this.putStyle('marginBottom', '0');
        if (onClose != null) {
            onClose(this);
        }
    };
    //
    this.isOpen = () => {
        return open;
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const setType = (type) => {
        if (previousType != null) {
            this.removeClass(previousType.displayName);
        }
        this.addClass(type.displayName);
        previousType = type;
        iconContainer
            .clearNodes()
            .add(createIcon(type));
    }

    const createIcon = (type) => {
        return new Icon(type.icon)
            .putStyle('marginRight', '20px')
            .putStyle('verticalAlign', 'middle');
    }

    const createLabel = () => {
        iconContainer = new Component('span');
        message = new Component('span');
        return new Component()
            .add(iconContainer)
            .add(message)
            .addClass('snackbar-label', 'label-center');
    }

    const init = () => {
        this
            .addClass('snackbar')
            .add(createLabel());
    };

    init();

};
