/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * Provides a view that allows the user to select a target format.
 *
 * @param {Format} defaultValue  the default selected format
 * @param {string} tip  a textual description to display on mouse over
 * @param {string} cssClass  a unique css class providing custom styles for this component
 * @see Format
 */
const FormatPicker = function(defaultValue, tip, cssClass) {

    Component.call(this);

    // Config
    let callback = () => {}
    // State
    let value = defaultValue;
    let openState;
    // Cache
    /** @type {Option[]} */
    let options = [];
    let typeFilter;
    // UI
    let filterButtonGroup;
    let button;
    let dialog;
    let optionsContainer;
    let searchField;
    let eraseFiltersBtn;
    let eraseSearchStringBtn;
    let nameContainer;
    let countLabel;
    let placeholder;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.getValue = () => {
        return value;
    }

    this.setValue = (newValue) => {
        getOption(value).setSelected(false);
        value = newValue;
        getOption(newValue).setSelected(true);
        button.setText(defaultItemFormatter(newValue));
    }

    this.reset = () => {
        this.setValue(defaultValue);
    }

    this.isOpen = () => {
        return openState;
    }

    this.setOpen = (open) => {
        openState = open;
        dialog.setVisible(openState);
        if (open) {
            focusInput();
        } else {
            resetFilters();
            filterButtonGroup.clearSelection();
        }
    }

    this.toggle = () => {
        this.setOpen(!openState);
    }

    this.registerCallback = function(then) {
        callback = then;
        return this;
    };

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const focusInput = () => {
        searchField.getUI().focus();
    }

    const resetFilters = () => {
        resetTypeFilter();
        resetSearchField();
        filterResults();
    }

    const resetTypeFilter = () => {
        typeFilter = null;
        filterButtonGroup.clearSelection();
    }

    const resetSearchField = () => {
        searchField.getUI().value = '';
    }

    const filterResults = () => {
        const searchString = getSearchString();
        eraseSearchStringBtn.setVisible('' !== searchString);
        let count = 0;
        options.forEach(option => {
            const textMatch = matchesText(option, searchString);
            const typeMatch = matchesType(option, typeFilter);
            const matches = textMatch && typeMatch;
            option.setVisible(matches);
            if (matches) {
                count++;
            }
        });
        placeholder.setVisible(count === 0);
        eraseFiltersBtn.setEnabled(typeFilter != null);
        countLabel.setText(formatCount(count));
        focusInput();
    }

    const matchesType = (option, type) => {
        if (type == null) {
            return true;
        }
        return (option.getValue().type === type);
    }

    const matchesText = (option, searchString) => {
        if (StringUtils.isBlank(searchString)) {
            return true;
        }
        const lowerCased = option.getValue().displayName.toLowerCase();
        return (lowerCased.match(searchString));
    }

    const getOption = (format) => {
        return options.filter(option => option.getValue() === format)[0];
    }

    const getSearchString = () => {
        const searchString = searchField.getUI().value.toLowerCase();
        return StringUtils.escapeRegexCharacters(searchString);
    }

    const requestNewFormat = () => {
        window.open(`${editjs.REPOSITORY}/-/issues`, '_blank');
    }

    const formatCount = (count) => {
        return count + ' ' + StringUtils.pluralize('format', count);
    }

    const onTypeFilterChange = (type, active) => {
        typeFilter = active ? type : null;
        filterResults();
    }

    const onSearchFieldInput = () => {
        filterResults();
    }

    const onEraseButtonClick = () => {
        resetSearchField();
        filterResults();
    }

    const onResetButtonClick = () => {
        resetTypeFilter();
        filterResults();
    }

    const onOptionClick = (format) => {
        this.setValue(format);
        this.setOpen(false);
        callback(format);
    }

    const createOption = (format) => {
        return new Option(format)
            .addListener('click', () => onOptionClick(format), false);
    }

    const createFilterOptions = () => {
        const buttonsWrapper = new Component();
        eraseFiltersBtn = new Button('All Categories', onResetButtonClick, 'Erase filters')
            .withIcon(Icons.BIN, 0)
            .addClass('style-rectangle', 'reset-filters-btn')
            .setEnabled(false);
        const resetWrapper = new Component()
            .addClass('reset-filters-btn-wrapper')
            .add(eraseFiltersBtn);
        const buttons = Object.values(FormatType)
            .map(type => new ToggleButton(type, () => {}, false, type));
        buttons.forEach(btn => buttonsWrapper.add(btn));
        filterButtonGroup = new ButtonGroup(true, ...buttons)
            .onSelectionChange((type, active) => onTypeFilterChange(type, active));
        return new Component()
            .addClass('filter-options')
            .add(buttonsWrapper)
            .add(resetWrapper);
    }

    const createSearchField = () => {
        const input = new Component('input')
            .addListener('input', onSearchFieldInput, false)
            .addClass('search-field');
        input.getUI().type = 'text';
        input.getUI().placeholder = 'Filter by name...';
        return input;
    }

    const createSearchFieldInputWrapper = () => {
        eraseSearchStringBtn = new IconButton(Icons.PLUS, onEraseButtonClick, 'Reset search string', 45, ButtonStyle.NONE)
            .addClass('erase-btn')
            .setVisible(false);
        const searchIcon = new Icon(Icons.SEARCH)
            .addClass('search-icon');
        return new Component()
            .addClass('search-field-wrapper')
            .add(searchField)
            .add(eraseSearchStringBtn)
            .add(searchIcon);
    }

    const createPlaceholder = () => {
        const resetFiltersBtn = new Button('Clear Filters', resetFilters, '', ButtonStyle.FILLED)
            .withIcon(Icons.BIN)
            .addClass('style-rectangle');
        const requestNewFormatBtn = new Button('Request New Format', requestNewFormat, '', ButtonStyle.FILLED)
            .withIcon(Icons.ROCKET)
            .addClass('style-rectangle', 'primary');
        const buttonsWrapper = new Component()
            .addClass('buttons-wrapper')
            .add(resetFiltersBtn)
            .add(new Component('p').addClass('or').setText('or'))
            .add(requestNewFormatBtn);
        return new Component()
            .addClass('format-picker-placeholder')
            .add(new MarkdownRenderer(getPlaceholderText()))
            .add(buttonsWrapper)
            .setVisible(false);
    }

    const getPlaceholderText = () => {
        return `
            # No results
            Looks like this format is not supported yet.
        `;
    }

    const createOptions = () => {
        const container = new Component('ul')
            .addClass('options');
        for (let format of Object.values(Format)) {
            const option = createOption(format);
            options.push(option);
            container.add(option);
            if (format === value) {
                option.setSelected(true);
            }
        }
        return container;
    }

    const createDialog = () => {
        searchField = createSearchField();
        const filterOptions = createFilterOptions();
        optionsContainer = createOptions();
        placeholder = createPlaceholder();
        countLabel = new Component()
            .setText(formatCount(ObjectUtils.length(Format)))
            .addClass('count-label')
        const title = new Component()
            .addClass('dialog-title')
            .setText('Target format');
        return new Component()
            .addClass('dialog')
            .add(new CloseButton('Closes this dialog', () => this.setOpen(false)))
            .add(title)
            .add(filterOptions)
            .add(createSearchFieldInputWrapper())
            .add(optionsContainer)
            .add(placeholder)
            .add(countLabel)
            .setVisible(false);
    }

    const createCaption = (title) => {
        const captionLabel = new Component()
            .setText(title)
            .addClass('form-element-caption-label', 'label-center');
        return new Component()
            .addClass('form-element-caption')
            .add(captionLabel);
    };

    const defaultItemFormatter = (item) => {
        const prefix = StringUtils.isBlank(item.type) ? '' : '[' + item.type + '] ';
        return prefix + item.displayName;
    }

    const init = () => {
        dialog = createDialog();
        button = new Button(defaultItemFormatter(defaultValue), this.toggle, tip, ButtonStyle.TEXT)
            .withIcon(Icons.ANGLE_DOWN, 0)
            .addClass('style-rectangle', 'picker-btn');
        const wrapper = new Component()
            .addClass('form-element-wrapper')
            .add(createCaption('Format'))
            .add(button)
            .add(dialog);
        this.add(wrapper)
            .addClass('format-picker', 'form-element', 'clickable', cssClass)
            .setTip(tip);
    }

    init();

    //////////////////////////////////////////////////////////////////
    // Inner Class
    //////////////////////////////////////////////////////////////////

    function Option(format) {

        Component.call(this, 'li');

        //////////////////////////////////////////////////////////////////
        // API
        //////////////////////////////////////////////////////////////////

        this.setSelected = (selected) => {
            this.toggleClass('selected', selected);
        }

        this.getValue = () => {
            return format;
        }

        //////////////////////////////////////////////////////////////////
        // Internal
        //////////////////////////////////////////////////////////////////

        const init = () => {
            nameContainer = new Component()
                .setText(format.displayName)
                .addClass('option-text');
            const tagContainer = new Component()
                .addClass('tag-container')
                .add(new Tag(format.type));
            this.add(nameContainer)
                .add(tagContainer)
                .addClass('option', 'clickable');
        }

        init();

    }

}
