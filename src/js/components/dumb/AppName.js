/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const AppName = function(...cssClasses) {

    Component.call(this);


    const init = () => {
        const left = new Component('span')
            .addClass('branding-label', 'left')
            .setText('edit');
        const right = new Component('span')
            .addClass('branding-label', 'right')
            .setText('.js');
        const label = new Component()
            .add(left)
            .add(right)
            .addClass(...cssClasses);
        this
            .addClass('branding-label-wrapper')
            .add(label)
    }

    init();

}
