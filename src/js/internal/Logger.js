/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A singleton object responsible for logging messages based on their severity.
 *
 * By default, all logs are written to the browser console.
 * A custom target may be defined by invoking {@link Logger#setTarget}.
 * Setting a different target may be useful for writing logs to an HTML component
 * or to inject a stub in the context of unit testing.
 * Any Logger target must implement the following methods:
 * <ul>
 *     <li><code>error(...data: any[]): void</code></li>
 *     <li><code>info(...data: any[]): void</code></li>
 *     <li><code>log(...data: any[]): void</code></li>
 *     <li><code>trace(...data: any[]): void</code></li>
 * </ul>
 *
 * @see LogLevel
 */
const Logger = {

    LOG_LEVEL: LogLevel.ERROR,
    PRODUCTION: true,
    TARGET: console,

    setProduction: (production) => {
        Logger.PRODUCTION = production;
    },

    setLevel: (level) => {
        Logger.LOG_LEVEL = level;
    },

    setTarget: (target) => {
        Logger.TARGET = target;
    },

    error: (component, ...elements) => {
        Logger.log(LogLevel.ERROR, component, ...elements);
    },

    debug: (component, ...elements) => {
        Logger.log(LogLevel.DEBUG, component, ...elements);
    },

    info: (component, ...elements) => {
        Logger.log(LogLevel.INFO, component, ...elements);
    },

    verbose: (component, ...elements) => {
        Logger.log(LogLevel.VERBOSE, component, ...elements);
    },

    trace: (component, ...elements) => {
        Logger.log(LogLevel.TRACE, component, ...elements);
    },

    /**
     * Logs the specified elements using the specified {@link LogLevel}.
     *
     * @param level  a {@link LogLevel} constant
     * @param component  the name of the component which initiated the log request
     * @param elements  the elements to log
     */
    log: (level, component, ...elements) => {
        if (Logger.hasLevel(level)) {
            const method = getConsoleMethod(level);
            Logger.TARGET[method]('[' + level.name + ']', 'Emitter:', component, '\n', ...elements);
        }
        function getConsoleMethod(level) {
            switch (level) {
                case LogLevel.ERROR:   return 'error';
                case LogLevel.INFO:    return 'info';
                case LogLevel.DEBUG:   return 'info';
                case LogLevel.VERBOSE: return 'log';
                case LogLevel.TRACE:   return 'trace';
            }
        }
    },

    //

    isError: () => {
        return (Logger.LOG_LEVEL.severity <= LogLevel.ERROR.severity);
    },

    isInfo: () => {
        return (Logger.LOG_LEVEL.severity <= LogLevel.INFO.severity);
    },

    isDebug: () => {
        return (!Logger.PRODUCTION && Logger.LOG_LEVEL.severity <= LogLevel.DEBUG.severity);
    },

    isVerbose: () => {
        return (!Logger.PRODUCTION && Logger.LOG_LEVEL.severity <= LogLevel.VERBOSE.severity);
    },

    isTrace: () => {
        return (!Logger.PRODUCTION && Logger.LOG_LEVEL.severity === LogLevel.TRACE.severity);
    },

    hasLevel: (level) => {
        switch (level) {
            case LogLevel.ERROR:   return Logger.isError();
            case LogLevel.INFO:    return Logger.isInfo();
            case LogLevel.DEBUG:   return Logger.isDebug();
            case LogLevel.VERBOSE: return Logger.isVerbose();
            case LogLevel.TRACE:   return Logger.isTrace();
        }
        return false;
    }

}
